package dbunit

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 3/15/11
 * Time: 4:20 PM
 * To change this template use File | Settings | File Templates.
 */
class DBImportConnection {

private static Connection conn;
    private static Logger glogger = Logger.getLogger("DBImportConnection");

    /**
     * This mathod is used to get the sql connection
     * @return Connection
     * @throws Exception
     */
    public static Connection getConnection() throws Exception
	{
		try
        {
            String url = "jdbc:postgresql://uranus.fiehnlab.ucdavis.edu:5432/binbase-scheduler-test";
            String driverClassName = "org.postgresql.Driver";
            String username = "binbase-scheduler";
            String password = "180sub";

            Class.forName(driverClassName);
            conn = DriverManager.getConnection(url, username, password);

		}catch (Exception ce)
		{
			glogger.error("Exception in getConnection Mathod of DBConnection Class : ",ce);
			ce.printStackTrace();
			throw ce;
		 }
	   return conn;
	}

    /**
     *  Close the Connection
     */
    public static void closeConnection()
    {
        try
        {
            if(!conn.isClosed())
                conn.close();
        }
        catch(SQLException sqlex)
        {
            glogger.error("Not able to close connection  " , sqlex);
        }
    }

    /**
     * close the given connection instence
     * @param con
     */
    public static void closeConnection(Connection con)
    {
        try
        {
            if(con != null && !con.isClosed())
            {
                con.close();
                con = null;
            }
        }
        catch(SQLException sqlex)
        {
            glogger.error("Sql exception While Closing the connection  ", sqlex);
        }
        catch(Exception genExp)
        {
            glogger.error("General exception Not able to close connection  ", genExp);
        }
    }

}
