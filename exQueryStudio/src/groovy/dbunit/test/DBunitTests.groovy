package dbunit.test

import dbunit.DBunitUtil
import grails.test.GrailsUnitTestCase
import javax.sql.DataSource
import org.apache.log4j.Logger
import org.dbunit.dataset.xml.XmlDataSet

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 12/21/10
 * Time: 3:55 PM
 * test cases to test DBunitUtil class
 */
class DBunitTests extends GrailsUnitTestCase {

   final static String DEFAULT_DATASET = "test/datasets/exQueryStudio-test.xml"

   Logger logger = Logger.getLogger("test")

   DataSource dataSource

   /**
    * returns our default data set
    * @return
    */
   protected String getDataSet() {
     return DEFAULT_DATASET
   }


   protected void setUp() {
     super.setUp()

     logger.info "calling internal setup..."
     preSetup()

   }

   /**
    * setups the datalayer for us
    */
   protected synchronized void preSetup() {
     logger.info "setting up database"
     assertTrue(dataSource != null)

     File file = new File(getDataSet())

     logger.info "using: ${file}"
     assertTrue(file.exists());

     DBunitUtil.initialize(dataSource, new XmlDataSet(new FileReader(file)))

   }
}
