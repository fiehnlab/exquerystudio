package dbunit

import java.sql.Types
import org.dbunit.dataset.datatype.DataType
import org.dbunit.dataset.datatype.DataTypeException
import org.dbunit.dataset.datatype.DefaultDataTypeFactory

/**
 * User: pradeep
 * Date: Jul 12, 2010
 * Time: 10:53:27 AM
 * required because of a DBUnit bug with HSQLDB
 * Reff : http://www.dbunit.org/properties.html#typefactory
 */
public class HSQLDataTypeFactory extends DefaultDataTypeFactory {

    @Override
    public DataType createDataType(int sqlType, String sqlTypeName) throws DataTypeException {
        if(sqlType == Types.BOOLEAN){
            return DataType.BOOLEAN;
        }

        return super.createDataType(sqlType,sqlTypeName);
    }
}
