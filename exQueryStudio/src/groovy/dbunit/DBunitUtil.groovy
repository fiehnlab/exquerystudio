package dbunit

import javax.sql.DataSource
import org.apache.log4j.Logger
import org.dbunit.database.DatabaseConfig
import org.dbunit.database.DatabaseConnection
import org.dbunit.dataset.IDataSet
import org.dbunit.operation.DatabaseOperation

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 12/21/10
 * Time: 4:14 PM
 * To initialize data set for DBunit tests
 */
class DBunitUtil {

    /**
     * initialize a dbunit dataset for testing, works only with HSQLDB!
     * @param source
     * @param data
     * @return
     * @throws Exception
     */
    public static synchronized void initialize(DataSource source, IDataSet data) throws Exception{
        Logger logger = Logger.getLogger(DBunitUtil.class);
        logger.info("create connection...");
        DatabaseConnection dbunit = new DatabaseConnection(source.getConnection());

        logger.info("create configuration");
        DatabaseConfig config = dbunit.getConfig();

        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,new HSQLDataTypeFactory());

        logger.info("execute clean insert");
        DatabaseOperation.CLEAN_INSERT.execute(dbunit,data);
        logger.info("done and clean connection");
        dbunit.close();
        config = null;

    }
}
