package dbunit

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 3/15/11
 * Time: 4:18 PM
 * To change this template use File | Settings | File Templates.
 */

class DatabaseExport {

public static void main(String[] args) throws Exception {
        // database connection
        Connection jdbcConnection = DBImportConnection.getConnection();
        exportFullDataBase(jdbcConnection, "compound-repository-test");

        Set<String> set = new HashSet<String>();

        set.add("secuser");


        exportPartialDataSet(jdbcConnection, set, "./test/datasets/secuser");
    }

    /**
     * Export the full database in XML file
     *
     * @param jdbcConnection
     * @param fileName
     * @throws Exception
     */
    public static void exportFullDataBase(Connection jdbcConnection, String fileName) throws Exception {
        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);

        // full database export
        IDataSet fullDataSet = connection.createDataSet();
        XmlDataSet.write(fullDataSet, new FileOutputStream(fileName + ".xml"));
    }

    /**
     * Export the data for a perticuler table
     *
     * @param jdbcConnection
     * @param tableName
     * @param fileName
     * @throws Exception
     */
    public static void exportPartialDataSet(Connection jdbcConnection, Set<String> tableName, String fileName) throws Exception {
        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);

        // partial database export
        QueryDataSet partialDataSet = new QueryDataSet(connection);

        for (String s : tableName) {
            partialDataSet.addTable(s);
        }

        XmlDataSet.write(partialDataSet, new FileOutputStream(fileName + ".xml"));
    }


}
