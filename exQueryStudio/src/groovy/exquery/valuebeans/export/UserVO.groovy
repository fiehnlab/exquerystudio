package exquery.valuebeans.export

/**
 * Created by IntelliJ IDEA.
 * User: OptiPlex330
 * Date: May 25, 2011
 * Time: 1:58:57 PM
 * To change this template use File | Settings | File Templates.
 */
class UserVO {

    def SrNo

    def userId

    def userName

    def displayName

    def email

    def experimentCount = 0L

    def classesCount = 0L

    def samplesCount = 0L

    String toString(){
        return "User: ${displayName}"
    }

}
