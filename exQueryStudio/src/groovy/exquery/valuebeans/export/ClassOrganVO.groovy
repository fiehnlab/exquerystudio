package exquery.valuebeans.export

/**
 * User: pradeep
 * Date: Aug 1, 2010
 * Time: 1:19:18 AM
 * to hold the information of a classOrgan domain, it use for export
 */
class ClassOrganVO {

  def SrNo

  def id

  def classId

  def experimentId

  def organName

  def sampleId

  def label

  def chromatof

  def comments

  def event

  def sampleMetaData
}
