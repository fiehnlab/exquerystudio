package exquery.valuebeans.export

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/9/11
 * Time: 8:13 PM
 * To change this template use File | Settings | File Templates.
 */
class SpeciesVO {

  def SrNo

  def ncbiId

  def speciesName

  def scientificName

  def synonyms

  def experimentCount = 0L

  def classesCount = 0L

  def samplesCount = 0L

  def organs

}
