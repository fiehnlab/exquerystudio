package exquery.valuebeans.export

/**
 * User: pradeep
 * Date: Jul 31, 2010
 * Time: 9:09:00 PM
 * to hold the information of a experiment domain, it use for export
 */
class ExperimentVO {

  def SrNo
  
  def id

  def title

  def experimentAbstract

  def description

  def comments

  def classesCount = 0L

  def samplesCount = 0L
  
}
