package exquery.valuebeans.export

/**
 * User: pradeep
 * Date: Aug 1, 2010
 * Time: 1:09:55 AM
 * to hold the information of a classSpecies domain, it use for export
 */
class ClassSpeciesVO {

  def SrNo

  def id

  def classId

  def experimentId

  def speciesName

  def sampleId

  def label

  def chromatof

  def comments

  def event

  def sampleMetaData
}
