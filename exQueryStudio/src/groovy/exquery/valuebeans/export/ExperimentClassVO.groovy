package exquery.valuebeans.export

/**
 * User: pradeep
 * Date: Jul 31, 2010
 * Time: 9:56:07 PM
 * to hold the information of a experimentClass domain, it use for export
 */
class ExperimentClassVO {

  def SrNo

  def id

  def experimentId

  def organName

  def speciesName

  def tissue

  def genotype

  def methodName

  def cellType

  def subcellularCelltype

  def samplesCount = 0L

}
