package exquery.valuebeans.export

/**
 * User: pradeep
 * Date: Aug 1, 2010
 * Time: 12:18:53 AM
 * to hold the information of a classSample domain, it use for export
 */
class ClassSampleVO {
  def SrNo

  def id

  def classId

  def experimentId

  def chromatof

  def label

  def comments

  def event

  def sampleMetaData

  def genotype

  def tissue

  def speciesName

  def organsName
}
