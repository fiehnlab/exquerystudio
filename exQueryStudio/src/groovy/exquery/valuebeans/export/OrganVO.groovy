package exquery.valuebeans.export

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/20/11
 * Time: 9:22 PM
 * to hold the information of an Organ
 */
class OrganVO {

    def SrNo

    def organName

    def species

    def experimentCount = 0L

    def classesCount = 0L

    def samplesCount = 0L

    String toString(){
        //return "organName: ${organName}, species: ${species}, experimentCount: ${experimentCount}, classesCount: ${classesCount}, samplesCount: ${samplesCount}"
        return "organName: ${organName}"
    }
}
