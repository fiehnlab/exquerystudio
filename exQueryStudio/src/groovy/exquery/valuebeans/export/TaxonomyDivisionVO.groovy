package exquery.valuebeans.export

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 5/5/11
 * Time: 11:16 AM
 * Taxonomy Division Value Object
 */
class TaxonomyDivisionVO {

    def SrNo

    def divisionId

    def divisionCode

    def divisionName

    def comments

    def speciesName

    def experimentCount = 0L

    def classesCount = 0L

    def samplesCount = 0L

    String toString(){
        return "(${divisionId})-${divisionName}"
    }

}
