package exquery.valuebeans.webservice

/**
 * User: pradeep
 * Date: Oct 15, 2010
 * Time: 1:19:18 AM
 * to hold the information of a classOrgan domain, it use for web service
 */
class ClassOrganVO {

  def id

  def organId 

  def organName

}
