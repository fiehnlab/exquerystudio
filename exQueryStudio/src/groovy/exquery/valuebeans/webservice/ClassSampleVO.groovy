package exquery.valuebeans.webservice

/**
 * User: pradeep
 * Date: Oct 15, 2010
 * Time: 12:18:53 AM
 * to hold the information of a classSample domain, it use web service
 */
class ClassSampleVO {

  def id

  def label

  def comments

  def event

  def sampleChromatofVOSet

  def sampleMetaDataVOSet
}
