package exquery.valuebeans.webservice

/**
 * User: pradeep
 * Date: Oct 15, 2010
 * Time: 9:09:00 PM
 * to hold the information of a experiment domain, it use for web service
 */
class ExperimentVO {

  def id

  def title

  def experimentAbstract

  def description

  def comments

  def experimentClassVOSet

  def experimentPublicationVOSet
    
}
