package exquery.valuebeans.webservice

/**
 * User: pradeep
 * Date: Oct 15, 2010
 * Time: 1:12:21 PM
 * To change this template use File | Settings | File Templates.
 */
class ExperimentPublicationVO {

  def id

  def publishedBy

  def publishedDate

  def label
}
