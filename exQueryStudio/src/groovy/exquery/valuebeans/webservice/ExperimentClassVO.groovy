package exquery.valuebeans.webservice

/**
 * User: pradeep
 * Date: Oct 15, 2010
 * Time: 9:56:07 PM
 * to hold the information of a experimentClass domain, it use for web service
 */
class ExperimentClassVO {

  def id

  def tissue

  def genotype

  def methodName

  def cellType

  def subcellularCelltype

  def classSampleVOSet

  def classOrganVOSet

  def classSpeciesVOSet

}
