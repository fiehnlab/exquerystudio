package exquery.valuebeans.webservice

/**
 * User: pradeep
 * Date: Oct 15, 2010
 * Time: 1:09:55 AM
 * to hold the information of a classSpecies domain, it use for web service
 */
class ClassSpeciesVO {

  def id

  def speciesId
  
  def speciesName
  
}
