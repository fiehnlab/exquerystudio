package exquery.util

/**
 * User: pradeep
 * Date: Jul 2, 2010
 * Time: 2:15:22 PM
 * To validate inputs.
 */
class ValidationUtil {

  private static  ValidationUtil instance = null

  /**
   * returns the instance
   * @return
   */
  public static  ValidationUtil getInstance() {
    if (instance == null) {
      instance = new  ValidationUtil()
    }

    return instance
  }

  /**
   * check for Long value
   * @param value
   * @return
   */
  public boolean isLong(String value) {
      String validString = "0123456789";
      boolean res = true

      if(value.trim().length() == 0){
        return false;
      }
    
      value.each {  charVal->
        if( res ){
            if( validString.indexOf(charVal) == -1 ){
              res = false
            }
        }
       }

      return res
    }

}
