package exquery.util

import exquery.ExperimentInfoService
import exquery.SearchService
import exquery.annotations.AnnotationHelper
import exquery.annotations.QueryObject
import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Jul 1, 2010
 * Time: 4:47:16 PM
 * Helper class to collect Annotation info.
 */
class SearchHelper {

  Logger gLogger = Logger.getLogger(SearchHelper.class)
  def g = new org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib()

  /**
   * index over the required keys
   */
  private Map<String, String> searchMethodNames = new HashMap<String, String>()
  private Map<String, String> experimentInfoMethodNames = new HashMap<String, String>()

  /**
   * internal instance
   */
  private static  SearchHelper instance = null

  /**
   * Initialize sets for search and info services
   * @return
   */
  private SearchHelper() {

    gLogger.info "Comming into SearchHelper method of SearchHelper"

    //contains all possible searchSet
    Collection<QueryObject> searchSet = AnnotationHelper.getQueryableAnnotations(SearchService.class)
    Collection<QueryObject> infoSet = AnnotationHelper.getQueryableAnnotations(ExperimentInfoService.class)


    gLogger.info "searchSet :: ${searchSet}"
    gLogger.info "infoSet :: ${infoSet}"

    //generate our lookup association
    for (QueryObject q: searchSet) {
      searchMethodNames.put(q.getAnnotation().name().toLowerCase(), q.getMethod().getName())
    }

    //generate our lookup association
    for (QueryObject q: infoSet) {
      experimentInfoMethodNames.put(q.getAnnotation().name().toLowerCase(), q.getMethod().getName())
    }

  }

  /**
   * returns the instance
   * @return
   */
  public static  SearchHelper getInstance() {
    if (instance == null) {
      instance = new  SearchHelper()
    }

    return instance
  }
  /**
   * To get all search serice methods name
   */
  Collection getSearchMethods() {
    return getOptionList(searchMethodNames.keySet().sort())
  }

 /**
   * gets the experiment info name
   * @param name
   * @return
   */
 String getSearchMethodName(String name) {
    if(name!=null){
      return searchMethodNames.get(name.toLowerCase())
    }else {
      return null;
    }
  }

 /**
   * To get all experiment info methods name
   */
 Collection getExperimentInfoMethods() {
    return getOptionList(experimentInfoMethodNames.keySet().sort())
 }

 /**
   * gets the search method name
   * @param name
   * @return
   */
 String getExperimentInfoMethodName(String name) {

    gLogger.info "name :: ${name}"
    gLogger.info "experimentInfoMethodNames.get(name.toLowerCase()) :: ${experimentInfoMethodNames.get(name.toLowerCase())}"
    gLogger.info "experimentInfoMethodNames :: ${experimentInfoMethodNames}"

    if(name!=null){
      return experimentInfoMethodNames.get(name.toLowerCase())
    }else {
      return null;
    }
 }

 /**
  * Generate the option list
  */
 private Collection getOptionList(def data) {
    def list = new ArrayList();
    def map = [:]
    data.each {String s ->
      map = new HashMap()
      map.put("ID", s)
      map.put("VALUE", g.message(code: s, default: s, encodeAs: "HTML"))
      list.add(map)
    }
    //println list
    return list
 }
}
