package exquery.util

/**
 * User: pradeep
 * Date: Jul 2, 2010
 * Time: 12:04:44 PM
 * Utility class to handle wild cards
 */
class WildCardUtil {

  private static WildCardUtil instance = null

  /**
   * returns the instance
   * @return
   */
  public static WildCardUtil getInstance() {
    if (instance == null) {
      instance = new WildCardUtil()
    }

    return instance
  }

  /**
   * converts a lucense query string to a like string, or attemps to. It's a rather simple version of doing this
   * @param value
   * @return
   */
  public String convertToLikeWildCard(def value) {

    value = value.replaceAll("\\*", "%")
    value = value.replaceAll("\\?", "_")

    return value
  }

  /**
   * checks if we have wildcards
   * @param value
   * @return
   */
   public boolean hasWildCards(String value) {
      if (value.contains("*")) {
        return true
      }

      if (value.contains("?")) {
        return true
      }

      return false
    }
  }