package exquery.util.excel

import org.apache.log4j.Logger
import exquery.util.excel.valuebeans.ExcelVO
import exquery.util.excel.valuebeans.SheetVO

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook
import exquery.util.excel.valuebeans.CellVO
import exquery.util.excel.valuebeans.RowVO


/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/18/11
 * Time: 11:06 AM
 * Excel Writer.
 */
class ExcelWriterUtil {

    Logger gLogger = Logger.getInstance(ExcelWriterUtil.class)
    private static  ExcelWriterUtil instance = null

    /**
     * returns the instance
     * @return
     */
    public static  ExcelWriterUtil getInstance() {
      if (instance == null) {
        instance = new  ExcelWriterUtil()
      }

      return instance
    }

    /**
     * Generate Excel from excel value bean
     * @param excelVO
     * @return
     */
    def generateExcel(ExcelVO excelVO){
       gLogger.info "Comming into generateExcel method of ExcelWriterUtil"
       HSSFWorkbook workBook = null;

       if(excelVO!=null){
           String excelName=""
           if(excelVO.getExcelName()!=null){
               excelName = excelVO.excelName
           }

           // Create a New XL Document
           workBook = new HSSFWorkbook();

           Row row
           Cell cell
           excelVO?.sheets?.each {SheetVO sheetVO->
               gLogger.info "sheetVO :: ${sheetVO}"
               if(sheetVO!=null){
                   // Make a worksheet in the XL document created
                   Sheet sheet = workBook.createSheet(sheetVO.sheetName)

                   // Create header row
                   RowVO headerData = sheetVO.headerData
                   // Set header row
                   if(headerData!=null){
                       if(headerData.getIndex()!=null){
                           row = sheet.createRow(headerData.getIndex())
                       }else{
                           row = sheet.createRow((short)0)
                       }

                       headerData?.getCells()?.each {CellVO headerCell->
                           if(headerCell.getIndex()!=null){
                               cell = row.createCell(headerCell.getIndex())
                           }else{
                               cell = row.createCell((short)0)
                           }
                           cell.setCellValue(headerCell.getValue())
                       }
                   }

                   def rowData = sheetVO.rowData
                   // Create header row
                   if(rowData!=null){
                       rowData?.each {RowVO data->
                           if(data?.getIndex()!=null){
                               row = sheet.createRow(data.getIndex())
                           }else{
                               row = sheet.createRow((short)0)
                           }

                           data?.getCells()?.each {CellVO dataCell->
                               if(dataCell.getIndex()!=null){
                                   cell = row.createCell(dataCell.getIndex())
                               }else{
                                   cell = row.createCell((short)0)
                               }
                               cell.setCellValue(dataCell.getValue())
                           }
                       }
                   }
               }
           }
       }
       gLogger.info "Out from generateExcel method of ExcelWriterUtil"
       return workBook
    }
}
