package exquery.util.excel.valuebeans

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/18/11
 * Time: 11:12 AM
 * value bean to hold the sheet data of the excel.
 */
class SheetVO {

    String sheetName
    RowVO headerData // RowVO
    Collection<RowVO> rowData //Collection of RowVO
}
