package exquery.util.excel.valuebeans

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/18/11
 * Time: 11:11 AM
 * value bean to hold the excel data.
 */
class ExcelVO {
    String excelName
    Collection<SheetVO> sheets
}
