package exquery.util.excel.valuebeans

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/18/11
 * Time: 11:12 AM
 * value bean to hold the excel cell data.
 */
class CellVO {
    int index
    def value
}
