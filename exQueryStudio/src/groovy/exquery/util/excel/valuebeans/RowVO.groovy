package exquery.util.excel.valuebeans

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/18/11
 * Time: 11:16 AM
 * value bean to hold the row data of the excel.
 */
class RowVO {
    int index
    Collection<CellVO> cells
}
