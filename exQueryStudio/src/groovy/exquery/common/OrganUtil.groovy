package exquery.common

import org.apache.log4j.Logger
import exquery.ExperimentService
import exquery.ClassSampleService
import exquery.OrgansService
import exquery.SpeciesService
import exquery.valuebeans.export.OrganVO

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/20/11
 * Time: 10:36 PM
 * Organ Util Class.
 */
class OrganUtil {

     Logger gLogger = Logger.getInstance(OrganUtil.class)
     private static  OrganUtil instance = null
     def experimentService = new ExperimentService()
     def classSampleService = new ClassSampleService()
     def organsService = new OrgansService()
     def speciesService = new SpeciesService()

    /**
      * returns the instance
      * @return
      */
    public static  OrganUtil getInstance() {
        if (instance == null) {
          instance = new  OrganUtil()
        }

        return instance
    }

    /**
     * get organ data by organ name
     * @param organName
     * @return
     */
    OrganVO getOrganDataByOrganName(String organName){
       //gLogger.info "organName:: ${organName}"
       OrganVO organVO = new OrganVO()
       organVO.setOrganName(organName)

       def classIds = organsService.getClassIdsByOrganName(organName)

       if(classIds!=null){
           if( classIds instanceof Collection && classIds?.size()==0){
                return null
           }
           def experimentIds = experimentService.getExperimentIdsByClassIds(classIds)
           def sampleIds = classSampleService.getSampleIdsByClassIds(classIds)
           def species = speciesService.getSpeciesByClassIds(classIds)
           organVO.setExperimentCount(experimentIds.size())
           organVO.setClassesCount(classIds.size())
           organVO.setSamplesCount(sampleIds.size())
           organVO.setSpecies(species)
       }else{
           return null
       }
       return organVO
    }

    private static Collection<OrganVO> organVOList = null
    public void setUniqueSxOrgans(){
        organVOList = organsService.getUniqueSxOrgans()
    }

    public Collection<OrganVO> getUniqueSxOrgans(){
        if( organVOList == null ){
            setUniqueSxOrgans()
        }

        return organVOList?.sort {it.samplesCount}?.reverse()
    }
}
