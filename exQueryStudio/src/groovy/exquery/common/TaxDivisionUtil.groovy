package exquery.common

import org.apache.log4j.Logger
import exquery.valuebeans.export.TaxonomyDivisionVO
import exquery.SpeciesService

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 5/5/11
 * Time: 10:45 PM
 * To change this template use File | Settings | File Templates.
 */
class TaxDivisionUtil {
    Logger gLogger = Logger.getInstance(TaxDivisionUtil.class)
    private static  TaxDivisionUtil instance = null

    private static Collection<TaxonomyDivisionVO> taxonomyDivisionVOList = null

    /**
      * returns the instance
      * @return
      */
    public static  TaxDivisionUtil getInstance() {
        if (instance == null) {
          instance = new  TaxDivisionUtil()
        }

        return instance
    }

    public void setUniqueDivisions(){
        SpeciesService service = new SpeciesService()

        taxonomyDivisionVOList = service.getUniqueDivisions()
    }

    public Collection<TaxonomyDivisionVO> getUniqueDivisions(){
        if( taxonomyDivisionVOList == null ){
            setUniqueDivisions()
        }

        return taxonomyDivisionVOList
    }

}
