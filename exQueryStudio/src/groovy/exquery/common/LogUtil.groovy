package exquery.common

import org.apache.log4j.Logger
import exquery.IELogService

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 5/10/11
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */
class LogUtil {

    Logger gLogger = Logger.getInstance(LogUtil.class)
    private static  LogUtil instance = null
    private static String lastUpdateDate = null
    def logService = new IELogService()

    /**
      * returns the instance
      * @return
      */
    public static  LogUtil getInstance() {
        if (instance == null) {
          instance = new  LogUtil()
        }

        return instance
    }

    public void setLastUpdateDate(){
        lastUpdateDate = logService.getAppsUpdatedDate()
    }

    public String getLastUpdateDate(){
        if( lastUpdateDate == null ){
            setLastUpdateDate()
        }

        return lastUpdateDate
    }
}
