package exquery.common

import org.apache.log4j.Logger

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 5/12/11
 * Time: 11:57 AM
 * To change this template use File | Settings | File Templates.
 */
class ProjectInitMap {

    Logger gLogger = Logger.getInstance(OrganUtil.class)
    private static  ProjectInitMap instance = null
    private static HashMap initMap = null


    /**
      * returns the instance
      * @return
      */
    public static  ProjectInitMap getInstance() {
        if (instance == null) {
          instance = new  ProjectInitMap()
        }

        return instance
    }

    void put(def key,def value){
        if(initMap==null){
            initMap = new HashMap();
        }
        initMap.put(key,value)
    }

    def get(def key){
       if(initMap==null){
            return null
       }

       if(key!=null){
        return initMap.get(key)
       }
       return null
    }

    def remove(def key){
       if(initMap==null){
            return null
       }

       if(key!=null){
         def value = initMap.remove(key)
         return value
       }

       return null
    }

}
