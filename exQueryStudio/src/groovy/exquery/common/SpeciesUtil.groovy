package exquery.common

import org.apache.log4j.Logger
import exquery.valuebeans.export.SpeciesVO
import taxonomy.TaxonomyNodes
import exquery.SpeciesService
import exquery.OrgansService
import exquery.ClassSampleService
import exquery.ExperimentService
import taxonomy.NCBITaxonomyService

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/20/11
 * Time: 10:21 PM
 * Species Util Class.
 */
class SpeciesUtil {

     Logger gLogger = Logger.getInstance(SpeciesUtil.class)
     private static  SpeciesUtil instance = null
     def taxonomyService = new NCBITaxonomyService()
     def experimentService = new ExperimentService()
     def classSampleService = new ClassSampleService()
     def organsService = new OrgansService()
     def speciesService = new SpeciesService()

     /**
      * returns the instance
      * @return
      */
     public static  SpeciesUtil getInstance() {
        if (instance == null) {
          instance = new  SpeciesUtil()
        }

        return instance
     }

    /**
     * get species by species name
     * @param speciesName
     * @return
     */
    SpeciesVO getSXSpeciesBySpeciesName(String speciesName){
       //gLogger.info "speciesName:: ${speciesName}"
       SpeciesVO speciesVO = null

       if(speciesName!=null){
           speciesVO = new SpeciesVO()
           speciesVO.setSpeciesName(speciesName)
           def taxNodes = taxonomyService.searchTaxonomyByTaxonomyName(speciesName)
           //gLogger.info "taxNodes:: ${taxNodes}"

           if(taxNodes!=null){

               if(taxNodes instanceof Collection<TaxonomyNodes> && taxNodes.size()>0){
                  TaxonomyNodes node = taxNodes.get(0)
                  speciesVO.setNcbiId(node?.id)
               }else if(taxNodes instanceof TaxonomyNodes){
                  speciesVO.setNcbiId(taxNodes?.id)
               }

               if(speciesVO.getNcbiId()!=null){
                   def taxonomyScientificName = taxonomyService.getTaxonomyScientificName(speciesVO.getNcbiId())
                   def taxonomySynonyms = taxonomyService.getTaxonomySynonyms(speciesVO.getNcbiId())

                   speciesVO.setScientificName(taxonomyScientificName)
                   speciesVO.setSynonyms(taxonomySynonyms)
               }

           }

           def classIds = speciesService.getClassIdsBySpeciesName(speciesName)
           //gLogger.info "classIds:: ${classIds}"
           if(classIds!=null){
               if( classIds instanceof Collection && classIds?.size()==0){
                    return null
               }
               def experimentIds = experimentService.getExperimentIdsByClassIds(classIds)
               def sampleIds = classSampleService.getSampleIdsByClassIds(classIds)
               def organs = organsService.getOrgansByClassIds(classIds)
               speciesVO.setExperimentCount(experimentIds.size())
               speciesVO.setClassesCount(classIds.size())
               speciesVO.setSamplesCount(sampleIds.size())
               speciesVO.setOrgans(organs)
           }
       }

       return speciesVO
    }

    private static Collection<SpeciesVO> SpeciesVOList = null
    public void setUniqueSxSpecies(){
        SpeciesVOList = speciesService.getUniqueSxSpecies()
    }

    public Collection<SpeciesVO> getUniqueSxSpecies(){
        if( SpeciesVOList == null ){
            setUniqueSxSpecies()
        }

        return SpeciesVOList?.sort {it.samplesCount}?.reverse()
    }
}
