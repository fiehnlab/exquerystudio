package exquery.annotations

import java.lang.annotation.*

/**
 * User: pradeep
 * Date: Jul 1, 2010
 * Time: 4:52:35 PM
 * used to determine if this method is queryable by the transform server
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface Queryable {

  /**
   * the name of the field
   */
  String name()
}