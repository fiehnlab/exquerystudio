grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir	= "target/test-reports"
grails.project.war.file = "target/${appName}.war"
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits( "global" ) {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {        
        grailsPlugins()
        grailsHome()

        // uncomment the below to enable remote dependency resolution
        // from public Maven repositories
        //mavenLocal()
        mavenCentral()
        mavenRepo "http://snapshots.repository.codehaus.org"
        mavenRepo "http://repository.codehaus.org"
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"
    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

        runtime(
          [group:'postgresql', name:'postgresql', version:'8.2-504.jdbc3'],
          [group:'org.jdom', name:'jdom', version:'1.1'],
          [group:'org.apache.poi', name:'poi', version:'3.6']
        )

        /* for war generation */
        runtime('org.hibernate:hibernate-validator:3.1.0.GA') {
              excludes 'sl4j-api', 'hibernate.core',
             'hibernate-commons-annotations', 'hibernate-entitymanager'//,'slf4j-log4j12-1.5.8'
        }
    }

}
