dataSource {
  pooled = true
  driverClassName = "org.hsqldb.jdbcDriver"
  username = "sa"
  password = ""
}
hibernate {
  cache.use_second_level_cache = true
  cache.use_query_cache = true
  cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
}

// environment specific settings
environments {
  development {
    dataSource {
      pooled = true
      loggingSql = true
      dbCreate = "update"
      url = "jdbc:postgresql://uranus.fiehnlab.ucdavis.edu:5432/binbase-scheduler-test"
      driverClassName = "org.postgresql.Driver" // use this driver to enable p6spy logging
      username = "binbase-scheduler"
      password = "180sub"
    }
  }

  test {
    dataSource {
      pooled = true
      loggingSql = true
      driverClassName = "org.hsqldb.jdbcDriver"
      username = "sa"
      password = ""
      dbCreate = "create-drop" // one of 'create', 'create-drop','update'
      url = "jdbc:hsqldb:mem:devDB"
    }
  }


  production {
    dataSource {
      pooled = true
      loggingSql = false
      dbCreate = "update"
      url = "jdbc:postgresql://uranus.fiehnlab.ucdavis.edu:5432/binbase-scheduler"
      driverClassName = "org.postgresql.Driver" // use this driver to enable p6spy logging
      username = "binbase-scheduler"
      password = "180sub"
    }
  }
}