package exquery

import setupx.entity.ExperimentClass
import org.apache.log4j.Logger

class ExperimentClassService {

    static transactional = true
    Logger gLogger = Logger.getLogger(ExperimentClassService.class)

    /**
     * to get Classes by list of class Ids
     * @param classIds
     * @return
     */
    def getClassesByClassIds(def classIds){
        Collection<ExperimentClass> result

        if(classIds!=null){
            if( classIds instanceof Collection ){
                if( classIds?.size()!=0 ){
                    result  = ExperimentClass.executeQuery("Select distinct ec from setupx.entity.ExperimentClass ec where ec.id in (:classIds) ",["classIds":classIds])
                }
            }else if( classIds instanceof Long ){
                result  = ExperimentClass.executeQuery("Select distinct ec from setupx.entity.ExperimentClass ec where ec.id = :classIds ",["classIds":classIds])
            }

        }
        return result
    }

    /**
     * to get the list of class Ids for list of experiment Ids
     * @param experimentIds
     * @return
     */
    def getClassIdsByExperimentIds(def experimentIds){
        def result
        if(experimentIds!=null){
            if( experimentIds instanceof Collection ){
                if(experimentIds?.size()!=0){
                    result  = ExperimentClass.executeQuery("Select distinct ec.id from setupx.entity.ExperimentClass ec where ec.experiment.id in (:experimentIds) ",["experimentIds":experimentIds])
                }
            }else if( experimentIds instanceof Long ){
                result  = ExperimentClass.executeQuery("Select distinct ec.id from setupx.entity.ExperimentClass ec where ec.experiment.id = :experimentIds ",["experimentIds":experimentIds])
            }

        }
        return result
    }

    /**
     * to count number of classes into the list of experiments
     * @param experimentIds
     * @return
     */
    Long getClassesCountByExperimentIds(def experimentIds){

        if(experimentIds!=null){
            if( experimentIds instanceof Collection ){
                if(experimentIds?.size()!=0){
                    def result  = ExperimentClass.executeQuery("Select count(ec) from setupx.entity.ExperimentClass ec where ec.experiment.id in (:experimentIds) ",["experimentIds":experimentIds])
                    return result[0]
                }
            }else if( experimentIds instanceof Long ){
                def result  = ExperimentClass.executeQuery("Select count(ec) from setupx.entity.ExperimentClass ec where ec.experiment.id = :experimentIds ",["experimentIds":experimentIds])
                return result[0]
            }

        }
        return 0L
    }
}
