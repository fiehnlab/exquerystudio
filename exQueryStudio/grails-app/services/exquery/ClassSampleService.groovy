package exquery

import org.apache.log4j.Logger
import setupx.entity.ClassSample

class ClassSampleService {

    boolean transactional = true
    Logger gLogger = Logger.getLogger(ClassSampleService.class)
    ExperimentClassService experimentClassService = new ExperimentClassService()

    /**
     * To get class sample Id by class sample Label
     * @param sampleLabel
     * @return sampleId info String format
     */
    def getClassSampleIdBySampleLabel(String sampleLabel) {

     Collection<ClassSample> result  = ClassSample.executeQuery("Select s from setupx.entity.ClassSample s where upper(s.label) = :search order by s.id", [search:sampleLabel?.toUpperCase()])

     gLogger.info result?.size()

     ClassSample classSample
     if(result!=null){
       if(result instanceof Collection && result.size()>0){
         classSample = result.get(0)
       }else{
         classSample = result
       }
     }

     return classSample?.id as String
    }

    /**
     * to get sample Ids list (includes null or empty labeled samples) for classIds
     * @param classIds
     * @return
     */
    def getAllSampleIdsByClassIds(def classIds){

        def idList
        if(classIds!=null){
            if( classIds instanceof Collection ){
                if(classIds?.size()!=0){
                    idList = ClassSample.executeQuery("Select distinct cs.id from setupx.entity.ClassSample cs where cs.experimentclass.id in (:classIds) ",["classIds":classIds])
                }
            }else if( classIds instanceof Long){
                idList = ClassSample.executeQuery("Select distinct cs.id from setupx.entity.ClassSample cs where cs.experimentclass.id = :classIds ",["classIds":classIds])
            }

        }
        gLogger.info "SampleIds Count :: ${idList?.size()}"

        return idList
    }

    /**
     * Only those sample who has label values
     * @param classIds
     * @return
     */
    def getSampleIdsByClassIds(def classIds){

        def idList
        if(classIds!=null){
            if( classIds instanceof Collection ){
                if(classIds?.size()!=0){
                    idList = ClassSample.executeQuery("Select distinct cs.id from setupx.entity.ClassSample cs where cs.experimentclass.id in (:classIds) and cs.label != null and cs.label!='' ",["classIds":classIds])
                }
            }else if( classIds instanceof Long ){
                idList = ClassSample.executeQuery("Select distinct cs.id from setupx.entity.ClassSample cs where cs.experimentclass.id = :classIds and cs.label != null and cs.label!='' ",["classIds":classIds])
            }

        }
        gLogger.info "SampleIds Count :: ${idList?.size()}"

        return idList
    }

    /**
     * get all samples by class Ids
     * @param classIds
     * @return
     */
    def getSamplesByClassIds(def classIds){
        Collection<ClassSample> result

        if(classIds!=null){
            if( classIds instanceof Collection ){
                if(classIds?.size()!=0){
                    result  = ClassSample.executeQuery("Select distinct cs from setupx.entity.ClassSample cs where cs.experimentclass.id in (:classIds) ",["classIds":classIds])
                }
            }else if( classIds instanceof Long ){
                result  = ClassSample.executeQuery("Select distinct cs from setupx.entity.ClassSample cs where cs.experimentclass.id = :classIds ",["classIds":classIds])
            }

        }
        gLogger.info "Sample Count :: ${result?.size()}"

        return result
    }

    /**
     * to get samples count for given list of class Ids 
     * @param classIds
     * @return
     */
    Long getSamplesCountByClassIds(def classIds){
        def result
        //gLogger.info "classIds :: ${classIds?.size()}"
        if(classIds!=null){
            if( classIds instanceof Collection ){
                if( classIds?.size()!=0 ){
                    result  = ClassSample.executeQuery("Select count(cs) from setupx.entity.ClassSample cs where cs.experimentclass.id in (:classIds) ",["classIds":classIds])
                }
                if(result!=null){
                    return result[0]
                }
            }else if( classIds instanceof Long ){
                result  = ClassSample.executeQuery("Select count(cs) from setupx.entity.ClassSample cs where cs.experimentclass.id = :classIds ",["classIds":classIds])
                if(result!=null){
                    return result[0]
                }
            }
        }
        return 0L
    }

    /**
     * to get samples count for the given experiments
     * @param experimentIds
     * @return
     */
    Long getSamplesCountByExperimentIds(def experimentIds){
        if(experimentIds!=null){
            if( experimentIds instanceof Collection && experimentIds?.size()==0){
                return 0L
            }
            def classIds = experimentClassService.getClassIdsByExperimentIds(experimentIds)
            if(classIds!=null && classIds?.size()>0){
                def result = getSamplesCountByClassIds(classIds)
                return result
            }
        }
        return 0L
    }

    /**
     * to get samples for the given experiments
     * @param experimentIds
     * @return
     */
    Collection<ClassSample> getSamplesByExperimentIds(def experimentIds){
        Collection<ClassSample> result
        if(experimentIds!=null){

            if( experimentIds instanceof Collection && experimentIds?.size()>0){
                def classIds = experimentClassService.getClassIdsByExperimentIds(experimentIds)
                if(classIds!=null && classIds?.size()>0){
                    result = getSamplesByClassIds(classIds)
                    return result
                }
            }
        }
        return result
    }

    /**
     * get samples list by sampleIds
     * @param samplesIds
     * @return
     */
    Collection<ClassSample> getSamplesBySamplesIds(def samplesIds){
        Collection<ClassSample> result

        if(samplesIds!=null){
            if( samplesIds instanceof Collection ){
                if(samplesIds?.size()!=0){
                    result  = ClassSample.executeQuery("Select cs from setupx.entity.ClassSample cs where cs.id in (:samplesIds) ",["samplesIds":samplesIds])
                }
            }else if( samplesIds instanceof Long ){
                result  = ClassSample.executeQuery("Select cs from setupx.entity.ClassSample cs where cs.id = :samplesIds ",["samplesIds":samplesIds])
            }

        }
        gLogger.info "Sample Count :: ${result?.size()}"

        return result
    }


}
