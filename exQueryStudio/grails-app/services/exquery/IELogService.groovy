package exquery

import java.text.SimpleDateFormat
import org.apache.log4j.Logger
import setupx.importexport.ImportExportLog

class IELogService {

    boolean transactional = true
    Logger gLogger = Logger.getLogger(IELogService.class)


    /**
     * to get the latest date of application update 
     * @return
     */
    String getAppsUpdatedDate() {
      String updateDate = null
      def date = ImportExportLog.executeQuery("select max(l.startDate) from setupx.importexport.ImportExportLog l where l.status = :status", [status: true])
      if (date != null && date.size() > 0 && date[0]!=null) {
        SimpleDateFormat formetter = new SimpleDateFormat("EEE MMM dd, yyyy 'at' hh:mm:ss aaa z");
        //SimpleDateFormat formetter = new SimpleDateFormat("EEE MMM dd, yyyy");
        updateDate = formetter.format(date[0])
      }
      return updateDate
    }
}
