package exquery

import exquery.annotations.Queryable
import exquery.util.SearchHelper
import exquery.util.ValidationUtil
import org.apache.log4j.Logger
import setupx.entity.ClassSample
import setupx.entity.Experiment
import setupx.entity.ExperimentClass

class ExperimentInfoService {

    Logger gLogger = Logger.getLogger(ExperimentInfoService.class)
    boolean transactional = true
    def grailsApplication
    def setting

    void afterPropertiesSet() { this.setting = grailsApplication.config.setting }

    SearchHelper helper = SearchHelper.getInstance()
    LookupService lookupService = new LookupService()

    /**
     * to get experiment info
     * @param infoBy
     * @param id
     * @param params
     * @return
     */
    def getExperimentInfo(def infoBy,def id,Map params = [:]){
      gLogger.info "comming into getExperimentInfo method of ExperimentInfoService"

      //get the search method name
      String infoMethod = helper.getExperimentInfoMethodName(infoBy)

      if (infoMethod == null) {
        return ["resultMap" : ["ErrorMsg" : "wrong info criteria, ${infoBy}"]]
      }

      def result = "${infoMethod}"(id,params)
      gLogger.info "out from getExperimentInfo method of ExperimentInfoService"
      return result
    }

    /**
     * to get experiment info by experiment id
     * @param id
     * @param params
     * @return
     */
    @Queryable(name = "ExperimentId")
    def getExperimentInfoByExperimentId(def id,Map params = [:]){
      gLogger.info "comming into getExperimentInfoByExperimentId method of ExperimentInfoService"
      def resultMap = [:];
      if( ValidationUtil.getInstance().isLong(id.toString()))
      {
          Long longId = Long.parseLong(id.toString())
          Experiment e = lookupService.searchExperimentById(longId)
          resultMap = ["Experiment":e]
      }

      resultMap.putAt "AppsHeader","Experiment Info by Experiment Id ${id}"

      if(resultMap.Experiment == null ){
        //resultMap.putAt "ErrorMsg","Invalid Experiment Id ${id}"
        resultMap.putAt "ErrorMsg","No Data Found..."
      }

      gLogger.info "resultMap : ${resultMap}"

      gLogger.info "out from getExperimentInfoByExperimentId method of ExperimentInfoService"
      return resultMap
    }

    /**
     * to get experiment info by class id
     * @param id
     * @param params
     * @return
     */
    @Queryable(name = "ExperimentClassId")
    def getExperimentInfoByExperimentClassId(def id,Map params = [:]){
      gLogger.info "comming into getExperimentInfoByExperimentId method of ExperimentInfoService"
      def resultMap = [:];
      if( ValidationUtil.getInstance().isLong(id.toString()))
      {
          Long longId = Long.parseLong(id.toString())
          ExperimentClass ec = lookupService.searchExperimentClassById(longId)
          resultMap = ["Experiment":ec?.experiment,"ExperimentClass":ec]
      }

      resultMap.putAt "AppsHeader","Experiment Info by Class Id ${id}"

      if(resultMap.Experiment == null ){
        //resultMap.putAt "ErrorMsg","Invalid ExperimentClass Id ${id}"
        resultMap.putAt "ErrorMsg","No Data Found..."
      }

      gLogger.info "resultMap : ${resultMap}"

      gLogger.info "out from getExperimentInfoByExperimentId method of ExperimentInfoService"
      return resultMap
    }

    /**
     * to get experiment info by sample id
     * @param id
     * @param params
     * @return
     */
    @Queryable(name = "ClassSampleId")
    def getExperimentInfoByClassSampleId(def id,Map params = [:]){
      gLogger.info "comming into getExperimentInfoByExperimentId method of ExperimentInfoService"
      def resultMap = [:];
      if( ValidationUtil.getInstance().isLong(id.toString()))
      {
          Long longId = Long.parseLong(id.toString())
          ClassSample cs = lookupService.searchSampleById(longId)
          resultMap = ["Experiment":cs?.experimentclass?.experiment,"ExperimentClass":cs?.experimentclass,"ClassSample":cs]
      }

      resultMap.putAt "AppsHeader","Experiment Info by Sample Id ${id}"

      if(resultMap.Experiment == null ){
        //resultMap.putAt "ErrorMsg","Invalid Sample Id ${id}"
        resultMap.putAt "ErrorMsg","No Data Found..."
      }

      gLogger.info "resultMap : ${resultMap}"

      gLogger.info "out from getExperimentInfoByExperimentId method of ExperimentInfoService"
      return resultMap
    }
}
