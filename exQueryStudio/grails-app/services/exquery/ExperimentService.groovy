package exquery

import auth.entity.SecUser
import org.apache.log4j.Logger
import setupx.entity.Experiment
import setupx.entity.ExperimentClass

class ExperimentService {

    boolean transactional = true
    Logger gLogger = Logger.getLogger(ExperimentService.class)
    def grailsApplication
    def setting

    void afterPropertiesSet() { this.setting = grailsApplication.config.setting }

    /**
     * to get Experiments by list of experiment Ids
     * @param experimentIds
     * @return
     */
    def getExperimentsByExperimentIds(def experimentIds){
        Collection<Experiment> result

        if(experimentIds!=null){
            if( experimentIds instanceof Collection ){
                if( experimentIds?.size()!=0 ){
                    result  = Experiment.executeQuery("Select distinct e from setupx.entity.Experiment e where e.id in (:experimentIds) ",["experimentIds":experimentIds])
                }
            }else if( experimentIds instanceof Long ){
                result  = Experiment.executeQuery("Select distinct e from setupx.entity.Experiment e where e.id = :experimentIds ",["experimentIds":experimentIds])
            }

        }
        return result
    }

    /**
     * to get list of experiments belongs to the user
     * @param userId
     * @return
     */
    def getExperimentIdsByUserId(Long userId) {
      gLogger.info "comming into getExperimentIdsByUserId method of ExperimentService"
      def idList
      def user = SecUser.get(userId)

      if(user!=null && user instanceof SecUser) {
        def experimentUserRights = user?.experimentUserRights

        if(experimentUserRights!=null){
          idList = new ArrayList()
          experimentUserRights.each { e->
            idList.add(e.experiment.id)
          }
        }
      }
      gLogger.info "Out from getExperimentIdsByUserId method of ExperimentService"
      return idList
    }

    /**
     * to get list of experiments belongs to the user
     * @param userId
     * @return
     */
    Collection<Experiment> getAllExperimentsByUserId(Long userId) {
      gLogger.info "comming into getAllExperimentsByUserId method of ExperimentService"
      Collection<Experiment> resultCollection  = null;

      def user = SecUser.get(userId)

      def experimentUserRights = user?.experimentUserRights

      if(experimentUserRights!=null){
        resultCollection = new ArrayList()
        experimentUserRights.each { e->
          resultCollection.add(e.experiment)
        }
      }

      gLogger.info "Out from getAllExperimentsByUserId method of ExperimentService"
      return resultCollection
    }

    /**
     * to get experiment id of the class 
     * @param classIds
     * @return
     */
    def getExperimentIdsByClassIds(def classIds){

        def idList

        if(classIds!=null){
            if( classIds instanceof Collection ){
                if(classIds.size() != 0){
                    idList = ExperimentClass.executeQuery("Select distinct ec.experiment.id from setupx.entity.ExperimentClass ec where ec.id in (:classIds) ",["classIds":classIds])
                }
            }else if( classIds instanceof Long){
                idList = ExperimentClass.executeQuery("Select distinct ec.experiment.id from setupx.entity.ExperimentClass ec where ec.id = :classIds ",["classIds":classIds])
            }
        }
        //gLogger.info idList

        return idList
    }

    /**
     * to get the distinct list of experiments by list of class Ids  
     * @param classIds
     * @return
     */
    def getExperimentsByClassIds(def classIds){

        Collection<Experiment> resultCollection
        if(classIds!=null){
            if( classIds instanceof Collection ){
                if(classIds.size() != 0){
                    resultCollection = ExperimentClass.executeQuery("Select distinct ec.experiment from setupx.entity.ExperimentClass ec where ec.id in (:classIds) ",["classIds":classIds])
                }
            }else if( classIds instanceof Long ){
                resultCollection = ExperimentClass.executeQuery("Select distinct ec.experiment from setupx.entity.ExperimentClass ec where ec.id = :classIds ",["classIds":classIds])
            }
        }

        return resultCollection
    }


}
