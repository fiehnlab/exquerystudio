package exquery

import auth.entity.SecUser
import setupx.entity.Experiment
import org.apache.log4j.Logger
import exquery.valuebeans.export.UserVO
import auth.util.UserUtil

class UserService {

    static transactional = true
    Logger gLogger = Logger.getLogger(UserService.class)
    def inValidUsers = [999999L,71L,176L,51L,94L,12L]

    /**
     * to get the list of all users
     * @return
     */
    Collection<UserVO> getAllUsers(){
      Collection<UserVO> userVoList

      def result = SecUser.executeQuery("select u from auth.entity.SecUser u where u.id not in (:inValidUsers) ",[inValidUsers:inValidUsers])

      if(result!=null && result instanceof Collection){
        userVoList = new ArrayList<UserVO>();
        UserVO userVO
        result.each {SecUser user->
          userVO = UserUtil.getInstance().getUserDetails(user)
          if(userVO!=null){
            userVoList.add(userVO)
          }
        }
      }

      return userVoList
    }

    /**
     * to get the user by there id
     * @param userId
     * @return
     */
    UserVO getUserById(Long userId){
      return UserUtil.getInstance().getUserDetails(userId)
    }

    /**
     * search user by text
     * @param userName
     * @param params
     * @return
     */
    Collection<UserVO> searchUser(String userName, Map params = [:]){
      Collection<UserVO> userVoList

      def result = SecUser.executeQuery("select u from auth.entity.SecUser u where u.id not in (:inValidUsers) and ( upper(u.username) like :userName or  upper(u.displayname) like :userName ) ",[userName:userName.toUpperCase(),inValidUsers:inValidUsers],params)

      if(result!=null && result instanceof Collection){
        userVoList = new ArrayList<UserVO>();
        UserVO userVO
        result.each {SecUser user->
          userVO = UserUtil.getInstance().getUserDetails(user)
          if(userVO!=null){
            userVoList.add(userVO)
          }
        }
      }
      return userVoList
    }
}
