package exquery

import exquery.util.WildCardUtil
import org.apache.log4j.Logger
import setupx.entity.*

import exquery.valuebeans.export.SpeciesVO
import taxonomy.TaxonomyNodes
import exquery.valuebeans.export.OrganVO
import exquery.common.OrganUtil
import exquery.common.SpeciesUtil
import exquery.util.CommonUtil
import exquery.valuebeans.export.UserVO

class LookupService {

    Logger gLogger = Logger.getLogger(LookupService.class)
    boolean transactional = true

    def grailsApplication
    def setting
    WildCardUtil wildCard = WildCardUtil.getInstance();
    void afterPropertiesSet() { this.setting = grailsApplication.config.setting }
    def NCBITaxonomyService
    def speciesService
    def organsService
    def userService
    
    def serviceMethod() {

    }

  /**
   * search Experiment by experiment_id
   * @param id
   * @param params
   * @return
   * can return 0 - 1
   */
  Experiment searchExperimentById(Long id, Map params = [:]) {
    return Experiment.get(id)
  }

  /**
   * Get the list of all experiments
   * can return 0 - n
   */
  Collection<Experiment> getAllExperiments() {

    return Experiment.getAll()
  }

  /**
   * search Experiment by free form text it searchs into abstract, title, description and comment
   * @param id
   * @param params
   * @return
   * can return 0 - n
   */
  Collection<Experiment> searchExperiments(String searchText, Map params = [:],def wildcards = true) {

    Collection< Experiment> resultCollection  = null;
    if ( searchText instanceof String && wildcards) {
        searchText = wildCard.convertToLikeWildCard(searchText)
    }

    resultCollection  =  Experiment.executeQuery("Select e from setupx.entity.Experiment e where upper(e.title) like :search or upper(e.experiment_abstract) like :search or upper(e.description) like :search or upper(e.comments) like :search  order by e.id", [search:searchText.trim().toUpperCase()],params)

    return resultCollection
  }

  /**
   * get searched experiments count
   */
  Long searchExperimentsCount(String searchText, Map params = [:],def wildcards = true) {

    if ( searchText instanceof String && wildcards) {
        searchText = wildCard.convertToLikeWildCard(searchText)
    }

    def recCount  =  Experiment.executeQuery("Select count(e.id) from setupx.entity.Experiment e where upper(e.title) like :search or upper(e.experiment_abstract) like :search or upper(e.description) like :search or upper(e.comments) like :search", [search:searchText.trim().toUpperCase()],params)

    return recCount[0]
  }

  /**
   * search ExperimentClass by class_id
   * @param id
   * @param params
   * @return
   * can return 0 - 1
   */
   ExperimentClass searchExperimentClassById(Long id, Map params = [:]) {
    return  ExperimentClass.get(id)
  }

  /**
   * Get the list of all ExperimentClass
   * can return 0 - n
   */
  Collection< ExperimentClass> getAllExperimentClasses() {
    return  ExperimentClass.getAll()
  }

  /**
   * search ExperimentClass by free form text it searchs into organ_name, genotype, tissue, detailed_location
   * @param id
   * @param params
   * @return
   * can return 0 - n
   */
  Collection<ExperimentClass> searchExperimentClasses(String searchText, Map params = [:],def wildcards = true) {

    Collection< ExperimentClass> resultCollection  = null;
    if ( searchText instanceof String && wildcards) {
        searchText = wildCard.convertToLikeWildCard(searchText)
    }

    resultCollection  =  ExperimentClass.executeQuery("Select e from setupx.entity.ExperimentClass e where upper(e.genotype) like :search  or upper(e.green_house) like :search or upper(e.preparation) like :search or upper(e.information) like :search or upper(e.method_name) like :search or upper(e.detailed_location) like :search or upper(e.extraction) like :search ", [search:searchText.trim().toUpperCase()],params)

    return resultCollection
  }

  /**
   * get searched classes count
   */
  Long searchExperimentClassesCount(String searchText, Map params = [:],def wildcards = true) {

    if ( searchText instanceof String && wildcards) {
        searchText = wildCard.convertToLikeWildCard(searchText)
    }

    def recCount  =  ExperimentClass.executeQuery("Select count(e.id) from setupx.entity.ExperimentClass e where upper(e.genotype) like :search  or upper(e.green_house) like :search or upper(e.preparation) like :search or upper(e.information) like :search or upper(e.method_name) like :search or upper(e.detailed_location) like :search or upper(e.extraction) like :search", [search:searchText.trim().toUpperCase()],params)

    return recCount[0]
  }

  /**
   * search ClassSample by sample_id
   * @param id
   * @param params
   * @return
   * can return 0 - 1
   */
   ClassSample searchSampleById(Long id, Map params = [:]) {
    return  ClassSample.get(id)
  }

   /**
    * to get samples by sample id list
    * @param  ids
    * @param  params
    * @return
    * can return 0 - n
    */
    Collection< ClassSample> getSamplesById(def ids, Map params = [:]) {
        Collection<ClassSample> result
        if(ids!=null){
            if(ids instanceof Collection || ids instanceof Long[]){
                result = ClassSample.executeQuery("Select s from setupx.entity.ClassSample s where s.id in (:sampleList) order by s.id", [sampleList:ids],params)
            }else if (ids instanceof Long){
                result = ClassSample.executeQuery("Select s from setupx.entity.ClassSample s where s.id = :sampleList order by s.id", [sampleList:ids],params)
            }
        }

        return result
    }

    /**
    * to get classes by class id list
    * @param  ids
    * @param  params
    * @return
    * can return 0 - n
    */
    Collection< ExperimentClass> getClassesById(def ids,Map params = [:]){
        Collection<ExperimentClass> result
        if(ids!=null){
            if(ids instanceof Collection || ids instanceof Long[]){
                result = ExperimentClass.executeQuery("Select c from setupx.entity.ExperimentClass c where c.id in (:classList) order by c.id", [classList:ids],params)
            }else if (ids instanceof Long){
                result = ExperimentClass.executeQuery("Select c from setupx.entity.ExperimentClass c where c.id = :classList order by c.id", [classList:ids],params)
            }
        }

        return result
    }

    /**
    * to get experiments by experiment id list
    * @param  ids
    * @param  params
    * @return
    * can return 0 - n
    */
    Collection<Experiment> getExperimentsById(def ids,Map params = [:]){
        Collection<Experiment> result
        if(ids!=null){
            if(ids instanceof Collection || ids instanceof Long[]){
                result = Experiment.executeQuery("Select e from setupx.entity.Experiment e where e.id in (:experimentList) order by e.id", [experimentList:ids],params)
            }else if (ids instanceof Long){
                result = Experiment.executeQuery("Select e from setupx.entity.Experiment e where e.id = :experimentList order by e.id", [experimentList:ids],params)
            }
        }

        return result
    }


   /**
    * to get chromatofs by Chromatof FileId list
    * @param  chromatofFileIds
    * @param  params
    * @return
    * can return 0 - n
    */
    Collection<SampleChromatof> getChromatofsByFileId(def chromatofFileIds,Map params = [:]){
        Collection<SampleChromatof> result = null

        if(chromatofFileIds!=null){
            if(chromatofFileIds instanceof Collection || chromatofFileIds instanceof String[]){
                result = SampleChromatof.executeQuery("Select sc from setupx.entity.SampleChromatof sc where upper(sc.chromatofFileId) in (:chromatofFileList) order by sc.chromatofFileId", [chromatofFileList: CommonUtil.getInstance().toUpper(chromatofFileIds)],params)
            }else if( chromatofFileIds instanceof String){
                result = SampleChromatof.executeQuery("Select sc from setupx.entity.SampleChromatof sc where upper(sc.chromatofFileId) = :chromatofFileList order by sc.chromatofFileId", [chromatofFileList: chromatofFileIds.toString().trim().toUpperCase()],params)
            }
        }

        return result
    }


  /**
   * Get the list of all Samples
   * can return 0 - n
   */
  Collection<ClassSample> getAllSamples() {
    return  ClassSample.getAll()
  }

  /**
   * search Samples by free form text it searchs into label, comments, event
   * @param id
   * @param params
   * @return
   * can return 0 - n
   */
  Collection<ClassSample> searchSamples(String searchText, Map params = [:],def wildcards = true) {

    Collection< ClassSample> resultCollection  = null;
    if ( searchText instanceof String && wildcards) {
        searchText = wildCard.convertToLikeWildCard(searchText)
    }

    resultCollection  =  ClassSample.executeQuery("Select s from setupx.entity.ClassSample s where upper(s.label) like :search or upper(s.comments) like :search or upper(s.event) like :search  order by s.id", [search:searchText.trim().toUpperCase()],params)
    //resultCollection  = ClassSample.executeQuery("Select s from setupx.entity.ClassSample s where s.id in (1359576,1359316,1359564,1359344,1359416,1359548,1359484,1359384,1359452,1359436,1359536,1359512,1359504,1359400,1359360,1359604,1359640,1359340,1359428,1359516,1359624,1359448,1359372,1359376,1359472,1359444,1359596,1359412,1359324,1359480,1359508,1359404,1359408,1359468,1359332,1359524,1359432,1359440,1359456,1359616,1359560,1359388,1359368,1359632,1359584,1359588,1359520,1359348,1359568,1359540,1359352,1359476,1359356,1359420,1359600,1359380,1359620,1359464,1359608,1359544,1359496,1359460,1359328,1359488,1359572,1359552,1359392,1359612,1359532,1359396,1359628,1359336,1359556,1359636,1359424,1359592,1359364,1359492,1359320,1359500,1359528,1359580,1352699,1352567,1352543,1352187,1352855,1352527,1352475,1352851,1352143,1352271,1352031,1352775,1352603,1352679,1352207,1352711,1352607,1352375,1352495,1352555,1352583,1352491,1352103,1352019,1352259,1352619,1352815,1352767,1352647,1352035,1352255,1351947,1352227,1352407,1351963,1352651,1352339,1351971,1352231,1352123,1352687,1352055,1352599,1352763,1352063,1352683,1352075,1352887,1352367,1352911,1352883,1352791,1352879,1352015,1352675,1352307,1352319,1352839,1352399,1352311,1352563,1352415,1352243,1352847,1352659,1352539,1352279,1352479,1352191,1352239,1352203,1351979,1352183,1352667,1352551,1352623,1352107,1351975,1352007,1352039,1352707,1352639,1351943,1352411,1352079,1351983,1352771,1352199,1352719,1352051,1352431,1352759,1352091,1351983,1352867,1352427,1352043,1352215,1352459,1352303,1352827,1352331,1351939,1352735,1351939,1352263,1351951,1352747,1352195,1352903,1352511,1351967,1352287,1352419,1351987,1351951,1352111,1352579,1352175,1352671,1352379,1352095,1351995,1352799,1352099,1352787,1351995,1352547,1352819,1352755,1352235,1352471,1352803,1352467,1352023,1352835,1352159,1352023,1352731,1352359,1352859,1352127,1352823,1352559,1352139,1352655,1352439,1351955,1351955,1352151,1352631,1352891,1352267,1352283,1352171,1352919,1352843,1352295,1352435,1352011,1352147,1352047,1351959,1352083,1352247,1352575,1352211,1352131,1352907,1352223,1351959,1352515,1352695,1352315,1352915,1352115,1352003,1352587,1352167,1352595,1352503,1352003,1352027,1352163,1352451,1352355,1352351,1352523,1352635,1352447,1352739,1352463,1352027,1352323,1352155,1352723,1352875,1352251,1352327,1352179,1352703,1352807,1352663,1352155,1352071,1352347,1352627,1352863,1352395,1352715,1352343,1352391,1352087,1352591,1352071,1352455,1352571,1351999,1352795,1352335,1352507,1352779,1352275,1352811,1352487,1351999,1352727,1352611,1352535,1352743,1352387,1352135,1352443,1352895,1352219,1352383,1352135,1351991,1352371,1352519,1352899,1352643,1352423,1352059,1352291,1352783,1313186,1313886,1313230,1314734,1313270,1313386,1314790,1313406,1314558,1313282,1314674,1313818,1313994,1314746,1314846,1314250,1314078,1314530,1313530,1313930,1313518,1313182,1313210,1313442,1314054,1313478,1314470,1313650,1314214,1314290,1313454,1313114,1313522,1313290,1314698,1313622,1314582,1313950,1313626,1313514,1313158,1314270,1313366,1313438,1313302,1313722,1313458,1313630,1314822,1313110,1314302,1313314,1314602,1313746,1314418,1314678,1314202,1313934,1314298,1313774,1314498,1313794,1313166,1314458,1313874,1313594,1314006,1314058,1314314,1314714,1314758,1314002,1313150,1314590,1313470,1313134,1313362,1313334,1313266,1313942,1313382,1314818,1314518,1313686,1314490,1313154,1314510,1314474,1314670,1314578,1313998,1313510,1314546,1314422,1314026,1313506,1314778,1313910,1314874,1313142,1313890,1314182,1313534,1314082,1314570,1314798,1314454,1314634,1314538,1314574,1313086,1314094,1314514,1313914,1313970,1313974,1314210,1313646,1314730,1314482,1313094,1313814,1314354,1313546,1314810,1313326,1314394,1314566,1313702,1313658,1313090,1313306,1314438,1313486,1313902,1314306,1314726,1313850,1314266,1313178,1313482,1314814,1313474,1313198,1313714,1313414,1314866,1313862,1313250,1313354,1313666,1313682,1313214,1314098,1314430,1313766,1314622,1314402,1314598,1313234,1314642,1313554,1313954,1313678,1314010,1313770,1313538,1313978,1314710,1313242,1314334,1314706,1314086,1314862,1313350,1314838,1314346,1313138,1313462,1313782,1313738,1313662,1313758,1314370,1313586,1313990,1313966,1313206,1314282,1314258,1313870,1313146,1314382,1313130,1314494,1314638,1314278,1314030,1314226,1314502,1314742,1314722,1313846,1313810,1313402,1314646,1313162,1314694,1314362,1314826,1313670,1313878,1313322,1314342,1313558,1314358,1314090,1313318,1314046,1313690,1313982,1314690,1313398,1314442,1313106,1313542,1314390,1314074,1313490,1313634,1314378,1313754,1313806,1313066,1313262,1314478,1313730,1314750,1314426,1313426,1314854,1314410,1314450,1313394,1313498,1314738,1313422,1314310,1313882,1314366,1313802,1314534,1313986,1314802,1314206,1313258,1314286,1314594,1313550,1314754,1314434,1313926,1314782,1314326,1313122,1313866,1314230,1314398,1314554,1313726,1313222,1313342,1314606,1314242,1313918,1314850,1313190,1314666,1314794,1313798,1314350,1314522,1313274,1313358,1313410,1313170,1313430,1314238,1313450,1313254,1313434,1313590,1314198,1313278,1314254,1313194,1314618,1313734,1314322,1313674,1314318,1314630,1313762,1313370,1313074,1313830,1314662,1314834,1313298,1314338,1313338,1314766,1314406,1314702,1313082,1313494,1313346,1313718,1313102,1314186,1314842,1314018,1314374,1313698,1314042,1313098,1313898,1314414,1313938,1314462,1314686,1314246,1314014,1313778,1314066,1314050,1313786,1313174,1314830,1313330,1313834,1314038,1314562,1313742,1314190,1314274,1314446,1313906,1313502,1314762,1314062,1313526,1313858,1314786,1313390,1314294,1313946,1314386,1313238,1313582,1313126,1313294,1313466,1314222,1314870,1313838,1314034,1313070,1313654,1314506,1313694,1313706,1314654,1313922,1314610,1313310,1313446,1314486,1314330,1313962,1313418,1314070,1314770,1314586,1313642,1313894,1314542,1313078,1313638,1313598,1313246,1314626,1313378,1313374,1314718,1314022,1314194,1313286,1313062,1314774,1313854,1314682,1313826,1314650,1313218,1314550,1313958,1313118,1313710,1314614,1313842,1313202,1314858,1314218,1313226,1314526,1314806,1313822,1314234,1314262,1314658,1313750,1314466,1313790,970076,970236,970180,970268,969796,969976,969832,969992,970088,970128,970220,970164,970208,970116,970168,969920,970188,970100,969840,970008,969808,970064,970280,969900,970048,970060,970056,970176,969968,969828,970036,969904,970160,969812,969888,970244,970028,969844,970200,969984,970144,970172,969940,969848,970040,969876,970044,969916,970112,970260,969980,969820,970120,970256,970068,970204,970136,969816,969864,969936,969928,970108,969852,970196,970080,970004,970224,970024,969860,969956,970032,970152,969804,970264,969884,969800,969896,970228,969964,970140,969892,969912,970232,969908,969952,969960,970016,970096,970192,970052,970020,969836,970132,969944,970276,970084,969868,970272,969948,970104,969996,970248,970148,970156,969972,970252,969924,969880,970240,969872,970072,970124,970012,970212,969988,970184,970216,970092,970000,969932) order by s.id")
    return resultCollection
  }

  /**
   * get searched samples count
   */
  Long searchSamplesCount(String searchText, Map params = [:],def wildcards = true) {

    if ( searchText instanceof String && wildcards) {
        searchText = wildCard.convertToLikeWildCard(searchText)
    }

    def recCount  =  ClassSample.executeQuery("Select count(s.id) from setupx.entity.ClassSample s where upper(s.label) like :search or upper(s.comments) like :search or upper(s.event) like :search", [search:searchText.trim().toUpperCase()],params)
    //def recCount  = ClassSample.executeQuery("Select count(s.id) from setupx.entity.ClassSample s where s.id in (1359576,1359316,1359564,1359344,1359416,1359548,1359484,1359384,1359452,1359436,1359536,1359512,1359504,1359400,1359360,1359604,1359640,1359340,1359428,1359516,1359624,1359448,1359372,1359376,1359472,1359444,1359596,1359412,1359324,1359480,1359508,1359404,1359408,1359468,1359332,1359524,1359432,1359440,1359456,1359616,1359560,1359388,1359368,1359632,1359584,1359588,1359520,1359348,1359568,1359540,1359352,1359476,1359356,1359420,1359600,1359380,1359620,1359464,1359608,1359544,1359496,1359460,1359328,1359488,1359572,1359552,1359392,1359612,1359532,1359396,1359628,1359336,1359556,1359636,1359424,1359592,1359364,1359492,1359320,1359500,1359528,1359580,1352699,1352567,1352543,1352187,1352855,1352527,1352475,1352851,1352143,1352271,1352031,1352775,1352603,1352679,1352207,1352711,1352607,1352375,1352495,1352555,1352583,1352491,1352103,1352019,1352259,1352619,1352815,1352767,1352647,1352035,1352255,1351947,1352227,1352407,1351963,1352651,1352339,1351971,1352231,1352123,1352687,1352055,1352599,1352763,1352063,1352683,1352075,1352887,1352367,1352911,1352883,1352791,1352879,1352015,1352675,1352307,1352319,1352839,1352399,1352311,1352563,1352415,1352243,1352847,1352659,1352539,1352279,1352479,1352191,1352239,1352203,1351979,1352183,1352667,1352551,1352623,1352107,1351975,1352007,1352039,1352707,1352639,1351943,1352411,1352079,1351983,1352771,1352199,1352719,1352051,1352431,1352759,1352091,1351983,1352867,1352427,1352043,1352215,1352459,1352303,1352827,1352331,1351939,1352735,1351939,1352263,1351951,1352747,1352195,1352903,1352511,1351967,1352287,1352419,1351987,1351951,1352111,1352579,1352175,1352671,1352379,1352095,1351995,1352799,1352099,1352787,1351995,1352547,1352819,1352755,1352235,1352471,1352803,1352467,1352023,1352835,1352159,1352023,1352731,1352359,1352859,1352127,1352823,1352559,1352139,1352655,1352439,1351955,1351955,1352151,1352631,1352891,1352267,1352283,1352171,1352919,1352843,1352295,1352435,1352011,1352147,1352047,1351959,1352083,1352247,1352575,1352211,1352131,1352907,1352223,1351959,1352515,1352695,1352315,1352915,1352115,1352003,1352587,1352167,1352595,1352503,1352003,1352027,1352163,1352451,1352355,1352351,1352523,1352635,1352447,1352739,1352463,1352027,1352323,1352155,1352723,1352875,1352251,1352327,1352179,1352703,1352807,1352663,1352155,1352071,1352347,1352627,1352863,1352395,1352715,1352343,1352391,1352087,1352591,1352071,1352455,1352571,1351999,1352795,1352335,1352507,1352779,1352275,1352811,1352487,1351999,1352727,1352611,1352535,1352743,1352387,1352135,1352443,1352895,1352219,1352383,1352135,1351991,1352371,1352519,1352899,1352643,1352423,1352059,1352291,1352783,1313186,1313886,1313230,1314734,1313270,1313386,1314790,1313406,1314558,1313282,1314674,1313818,1313994,1314746,1314846,1314250,1314078,1314530,1313530,1313930,1313518,1313182,1313210,1313442,1314054,1313478,1314470,1313650,1314214,1314290,1313454,1313114,1313522,1313290,1314698,1313622,1314582,1313950,1313626,1313514,1313158,1314270,1313366,1313438,1313302,1313722,1313458,1313630,1314822,1313110,1314302,1313314,1314602,1313746,1314418,1314678,1314202,1313934,1314298,1313774,1314498,1313794,1313166,1314458,1313874,1313594,1314006,1314058,1314314,1314714,1314758,1314002,1313150,1314590,1313470,1313134,1313362,1313334,1313266,1313942,1313382,1314818,1314518,1313686,1314490,1313154,1314510,1314474,1314670,1314578,1313998,1313510,1314546,1314422,1314026,1313506,1314778,1313910,1314874,1313142,1313890,1314182,1313534,1314082,1314570,1314798,1314454,1314634,1314538,1314574,1313086,1314094,1314514,1313914,1313970,1313974,1314210,1313646,1314730,1314482,1313094,1313814,1314354,1313546,1314810,1313326,1314394,1314566,1313702,1313658,1313090,1313306,1314438,1313486,1313902,1314306,1314726,1313850,1314266,1313178,1313482,1314814,1313474,1313198,1313714,1313414,1314866,1313862,1313250,1313354,1313666,1313682,1313214,1314098,1314430,1313766,1314622,1314402,1314598,1313234,1314642,1313554,1313954,1313678,1314010,1313770,1313538,1313978,1314710,1313242,1314334,1314706,1314086,1314862,1313350,1314838,1314346,1313138,1313462,1313782,1313738,1313662,1313758,1314370,1313586,1313990,1313966,1313206,1314282,1314258,1313870,1313146,1314382,1313130,1314494,1314638,1314278,1314030,1314226,1314502,1314742,1314722,1313846,1313810,1313402,1314646,1313162,1314694,1314362,1314826,1313670,1313878,1313322,1314342,1313558,1314358,1314090,1313318,1314046,1313690,1313982,1314690,1313398,1314442,1313106,1313542,1314390,1314074,1313490,1313634,1314378,1313754,1313806,1313066,1313262,1314478,1313730,1314750,1314426,1313426,1314854,1314410,1314450,1313394,1313498,1314738,1313422,1314310,1313882,1314366,1313802,1314534,1313986,1314802,1314206,1313258,1314286,1314594,1313550,1314754,1314434,1313926,1314782,1314326,1313122,1313866,1314230,1314398,1314554,1313726,1313222,1313342,1314606,1314242,1313918,1314850,1313190,1314666,1314794,1313798,1314350,1314522,1313274,1313358,1313410,1313170,1313430,1314238,1313450,1313254,1313434,1313590,1314198,1313278,1314254,1313194,1314618,1313734,1314322,1313674,1314318,1314630,1313762,1313370,1313074,1313830,1314662,1314834,1313298,1314338,1313338,1314766,1314406,1314702,1313082,1313494,1313346,1313718,1313102,1314186,1314842,1314018,1314374,1313698,1314042,1313098,1313898,1314414,1313938,1314462,1314686,1314246,1314014,1313778,1314066,1314050,1313786,1313174,1314830,1313330,1313834,1314038,1314562,1313742,1314190,1314274,1314446,1313906,1313502,1314762,1314062,1313526,1313858,1314786,1313390,1314294,1313946,1314386,1313238,1313582,1313126,1313294,1313466,1314222,1314870,1313838,1314034,1313070,1313654,1314506,1313694,1313706,1314654,1313922,1314610,1313310,1313446,1314486,1314330,1313962,1313418,1314070,1314770,1314586,1313642,1313894,1314542,1313078,1313638,1313598,1313246,1314626,1313378,1313374,1314718,1314022,1314194,1313286,1313062,1314774,1313854,1314682,1313826,1314650,1313218,1314550,1313958,1313118,1313710,1314614,1313842,1313202,1314858,1314218,1313226,1314526,1314806,1313822,1314234,1314262,1314658,1313750,1314466,1313790,970076,970236,970180,970268,969796,969976,969832,969992,970088,970128,970220,970164,970208,970116,970168,969920,970188,970100,969840,970008,969808,970064,970280,969900,970048,970060,970056,970176,969968,969828,970036,969904,970160,969812,969888,970244,970028,969844,970200,969984,970144,970172,969940,969848,970040,969876,970044,969916,970112,970260,969980,969820,970120,970256,970068,970204,970136,969816,969864,969936,969928,970108,969852,970196,970080,970004,970224,970024,969860,969956,970032,970152,969804,970264,969884,969800,969896,970228,969964,970140,969892,969912,970232,969908,969952,969960,970016,970096,970192,970052,970020,969836,970132,969944,970276,970084,969868,970272,969948,970104,969996,970248,970148,970156,969972,970252,969924,969880,970240,969872,970072,970124,970012,970212,969988,970184,970216,970092,970000,969932)")

    return recCount[0]
  }


  /**
   * search ClassSpecies by species NCBI Id
   * @param id
   * @param params
   * @return
   * can return 0 - 1
   */
   SpeciesVO searchSpeciesById(Long id, Map params = [:]) {
       SpeciesVO speciesVO = speciesService.getSXSpeciesByNCBISpeciesId(id)

       return  speciesVO
   }

  /**
   * Get the list of all Species
   * can return 0 - n
   */
  Collection< SpeciesVO> getAllSpecies() {
    //return  speciesService.getUniqueSxSpecies()?.sort {it.samplesCount}?.reverse()
      return SpeciesUtil.getInstance().getUniqueSxSpecies()?.sort {it.samplesCount}?.reverse()
  }

  /**
   * search Species by free form text search by species_name
   * @param id
   * @param params
   * @return
   * can return 0 - n
   */
  Collection<SpeciesVO> searchSpecies(String searchText, Map params = [:],def wildcards = true) {

    Collection<SpeciesVO> resultCollection  = null;
    def uniqueSxSpeciesIds = speciesService.getUniqueSxSpeciesNCBIIds()
    params.put("uniqueSxSpeciesIds",uniqueSxSpeciesIds)

    def nodesList = NCBITaxonomyService.searchTaxonomyByTaxonomyNameWithWildCards(searchText,params,wildCard)
    SpeciesVO speciesVO
    if(nodesList!=null){
        def SxSpeciesNCBIIds = speciesService.getUniqueSxSpeciesNCBIIds()
        resultCollection = new ArrayList<SpeciesVO>()
        nodesList.each {TaxonomyNodes node->
            gLogger.info "nodeId :: ${node.id}"
            if(SxSpeciesNCBIIds!=null && SxSpeciesNCBIIds.contains(node.id) ){
                speciesVO = speciesService.getSXSpeciesByNCBISpeciesId(node.id)
                if(speciesVO!=null){
                    resultCollection.add(speciesVO)
                }
            }else{
                gLogger.info "Skiped"
            }

        }
    }

    gLogger.info "resultCollection.size :: ${resultCollection?.size()}"
    //resultCollection  =  getAllSxSpeciesByTexonomyNames(taxonomyNames)

    return resultCollection?.sort {it.samplesCount}.reverse()
  }

  /**
   * get searched species count
   */
  /*Long searchSpeciesCount(String searchText, Map params = [:],def wildcards = true) {

    if ( searchText instanceof String && wildcards) {
        searchText = wildCard.convertToLikeWildCard(searchText)
    }

    def recCount  =  ClassSpecies.executeQuery("Select count(cs.id) from setupx.entity.ClassSpecies cs where cs.species_name like :search ", [search:searchText],params)

    return recCount[0]
  }*/

 /**
   * search Organ by sample_id
   * @param id
   * @param params
   * @return
   * can return 0 - 1
   */
   OrganVO searchOrganById(Long id, Map params = [:]) {
    OrganVO organVO
    ClassOrgan organ = ClassOrgan.findByOrgan_id(id)
    if(organ!=null && organ.organ_name!=null && organ.organ_name.toString().trim().length()>0 ){
        organVO = organsService.getOrganDataByOrganName(organ.organ_name)
    }
    return  organVO
  }

  /**
   * Get the list of all Organs
   * can return 0 - n
   */
  Collection<OrganVO> getAllOrgans() {
    //return  organsService.getUniqueSxOrgans()?.sort {it.samplesCount}?.reverse()
      return OrganUtil.getInstance().getUniqueSxOrgans()
  }

  /**
   * search Organs by free form text it searchs into organ_name
   * @param id
   * @param params
   * @return
   * can return 0 - n
   */
  Collection<OrganVO> searchOrgans(String searchText, Map params = [:],def wildcards = true) {

    Collection<OrganVO> resultCollection  = organsService.getOrganDataByOrganNameWithWildCards(searchText,params,wildcards)

    gLogger.info "resultCollection.size :: ${resultCollection?.size()}"

    return resultCollection?.sort {it.samplesCount}.reverse()
  }

  /**
   * get searched organs count
   */
//  Long searchOrgansCount(String searchText, Map params = [:],def wildcards = true) {
//
//    if ( searchText instanceof String && wildcards) {
//        searchText = wildCard.convertToLikeWildCard(searchText)
//    }
//
//    def recCount  =  ClassOrgan.executeQuery("Select count(co.id) from setupx.entity.ClassOrgan co where co.organ_name like :search", [search:searchText],params)
//
//    return recCount[0]
//  }

  Collection<UserVO> getAllUsers(){
      Collection<UserVO> result = userService.getAllUsers()
      return result
  }

  UserVO searchUserById(Long id, Map params = [:]){
      UserVO result = userService.getUserById(id)
      return result
  }

  Collection<UserVO> searchUser(String searchText, Map params = [:],def wildcards = true){
      if ( searchText instanceof String && wildcards) {
          searchText = wildCard.convertToLikeWildCard(searchText)
      }

      Collection<UserVO> result = userService.searchUser(searchText,params)
      return result
 }

}
