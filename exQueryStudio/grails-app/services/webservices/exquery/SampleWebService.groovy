package webservices.exquery

import exquery.ClassSampleService

class SampleWebService {

    boolean transactional = true
    static expose=['cxf']
    ClassSampleService classSampleService

    /**
     * To get class sample Id by class sample Label
     * @param sampleLabel
     * @return sampleId info String format
     */
    String getSampleIdBySampleLabel(String sampleLabel){
      assert (sampleLabel != null);

      def sampleId = classSampleService.getClassSampleIdBySampleLabel(sampleLabel)

      return sampleId?.toString()
    }

}
