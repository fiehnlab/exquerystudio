package webservices.exquery

import exquery.LookupService
import exquery.util.WebServiceFormatUtil
import org.jdom.Document

/* provides a webservice to get experiment info */
class ExperimentWebService {

    boolean transactional = true

    static expose=['cxf']

    LookupService lookupService
    WebServiceFormatUtil webServiceFormatUtil = new WebServiceFormatUtil()


   /**
    * To get experiment info by experiment id
    * @param experimentId
    * @return experiment info in XML format
    */
    String experimentMetaDataXMLByExperimentId(Long experimentId){
      assert (experimentId != null)

      Document doc = webServiceFormatUtil.getExperimentInfoByExperimentId(experimentId)

      return webServiceFormatUtil.castXmlDocumentIntoString(doc)
    }

    /**
    * To get experiment info by experiment ClassId
    * @param classId
    * @return experiment info in XML format
    */
    String experimentMetaDataXMLByClassId(Long classId){
      assert (classId != null)

      Document doc = webServiceFormatUtil.getExperimentInfoByExperimentClassId(classId)

      return webServiceFormatUtil.castXmlDocumentIntoString(doc)
    }

    /**
    * To get experiment info by class SampleId
    * @param sampleId
    * @return experiment info in XML format
    */
    String experimentMetaDataXMLBySampleId(Long sampleId){
      assert (sampleId != null)

      Document doc = webServiceFormatUtil.getExperimentInfoByClassSampleId(sampleId)

      return webServiceFormatUtil.castXmlDocumentIntoString(doc)
    }

}