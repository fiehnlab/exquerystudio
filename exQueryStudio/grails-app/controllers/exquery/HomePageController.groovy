package exquery

import exquery.util.CommonUtil
import org.apache.log4j.Logger

import jofc2.model.elements.PieChart
import jofc2.model.Chart
import jofc2.model.elements.PieChart.Slice
import exquery.valuebeans.export.TaxonomyDivisionVO
import exquery.common.TaxDivisionUtil
import jofc2.model.elements.Tooltip
import exquery.common.OrganUtil
import exquery.common.SpeciesUtil
import exquery.common.ProjectInitMap;


class HomePageController {

    Logger gLogger = Logger.getLogger(HomePageController.class)

    def speciesService
  
    def index = {
      gLogger.info  'comming into init method of HomePageController'


      def PrototypeInfo = ["experimentSize":ProjectInitMap.getInstance().get("EXPERIMENT_COUNT"),"classesSize":ProjectInitMap.getInstance().get("CLASSES_COUNT"),"samplesSize":ProjectInitMap.getInstance().get("SAMPLES_COUNT")]

      println PrototypeInfo

      //session.setAttribute("PrototypeInfo", PrototypeInfo);

      /* Remove session varibles*/
      if(session.getAttribute("resultData")!=null){
        session.removeAttribute("resultData")
      }
      if(session.getAttribute("txtIds")!=null){
        session.removeAttribute("txtIds")
      }
      
      if(session.getAttribute("APPS_UPDATE_DATE")!=null){
        session.removeAttribute("APPS_UPDATE_DATE")
      }

      def sxObjectType = CommonUtil.getInstance().getSXObjectType();
      def getSXSearchObjectType = CommonUtil.getInstance().getSXSearchObjectType();

      //Added by pradeep on 6th April - Provide top organs and species list on home page :Start
      def sxOrgansList = OrganUtil.getInstance().getUniqueSxOrgans()
      def sxSpeciesList = SpeciesUtil.getInstance().getUniqueSxSpecies()
      //Added by pradeep on 6th April - Provide top organs and species list on home page :End
      gLogger.info  'out from init method of HomePageController'
      return [PrototypeInfo:PrototypeInfo,sxObjectType:sxObjectType,getSXSearchObjectType:getSXSearchObjectType,sxOrgansList:sxOrgansList,sxSpeciesList:sxSpeciesList]
    }

    /**
     * Prepare Species chart data
     */
    def SPECIES_PIE_CHART = {
            def pieChart = new PieChart()
            pieChart.setAnimate(true)
            pieChart.setStartAngle(35)
            pieChart.setBorder(2)
            pieChart.setAlpha(0.8f)
            //def uniqueDivisions = speciesService.getUniqueDivisions()

            def uniqueDivisions = TaxDivisionUtil.getInstance().getUniqueDivisions()

            if(uniqueDivisions!=null && uniqueDivisions.size()>0){
                Slice slice
                String strToolTip
                uniqueDivisions.each {TaxonomyDivisionVO taxonomyDivisionVO->
                    slice = new Slice(taxonomyDivisionVO?.samplesCount,"${taxonomyDivisionVO?.divisionName} [${taxonomyDivisionVO?.samplesCount}]")

                    strToolTip  = "Brief Details<br>Experiment Count: ${taxonomyDivisionVO?.experimentCount}"
                    strToolTip += "<br>Classes Count: ${taxonomyDivisionVO?.classesCount}"
                    strToolTip += "<br>Species Name: "//${taxonomyDivisionVO?.speciesName.toString().replaceAll(";",";<br>                      ")}"

                    def speciesNames = taxonomyDivisionVO?.speciesName.toString().split("; ")
                    //println speciesNames
                    if(speciesNames!=null && speciesNames.length>0){
                        for(int index=0; index<speciesNames.length; index++){
                            if(index<10){
                                if(index==0){
                                    strToolTip += "${speciesNames[index]}"
                                }else{
                                    strToolTip += ";<br>                      ${speciesNames[index]}"
                                }
                            }else{
                                strToolTip += ";<br>                       more..."
                                break;
                            }
                        }
                    }

                    slice.setTip(strToolTip)
                    pieChart.addSlices(slice)
                    pieChart.setColours("#${getNewColor()}")
                }

            }

            //pieChart.setTooltip("#val# of #total#<br>#percent# of 100%")

            Chart c = new Chart("Divisions of Species and Samples Count").addElements(pieChart)

            //c.setBackgroundColour("#E0ECF8") //to change graph's Background Colour
            Tooltip tip = new Tooltip()
            tip.setBackgroundColour("#E0ECF8")
            tip.setColour("#E0ECF8")
            c.setTooltip(tip)

            render c.toString()
    }


    private colorList = []
    //Random generator = new Random((new Date()).getDate());
    Random generator = new Random(4238423);
    /**
     * Generate Random Color Code
     * @return
     */
    private int getNewColor(){

        int code = generator.nextInt(999999) + 1

        if(colorList!=null && !colorList.contains(code)){
          colorList.add(code)
          //println colorList
          return code
        }else{
          code = getNewColor()
          if(code==-1){
            code = getNewColor()
          }
        }
        return -1
    }



}
