package exquery

import org.apache.log4j.Logger
import exquery.util.SearchHelper
import exquery.valuebeans.export.UserVO
import exquery.util.ValidationUtil
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import exquery.util.FormatResultUtil
import exquery.util.ExportExperimentUtil

class OwnerController {

    Logger gLogger = Logger.getLogger(OwnerController.class)
    SearchHelper helper = SearchHelper.getInstance()
    def experimentService
    def experimentClassService
    def classSampleService
    def exportService
    def searchService
    def userService

    def index = {
        redirect( controller:"search" ,params:[cmbSearchBy:"User",searchText:"*"],action:"search")
    }

    def datalist={
      gLogger.info 'comming into action:datalist of controller:owner'
      gLogger.info "params :: ${params}"
      def userId = params.id
      def lookupBy = params.lookupBy
      def title="Result List By User"
      def isExport = false
      if(params?.format && params.format != "html"){
        isExport = true
      }

      Long liUserId = 0L
      if( ValidationUtil.getInstance().isLong(userId) ){
        liUserId = Long.parseLong(userId)
      }

      UserVO userVO = userService.getUserById(liUserId)
      def resultData = null

      if(userVO!=null){
            if(lookupBy!=null && lookupBy.toString().trim().equalsIgnoreCase("experiments")){

                 title="Experiment List By User '${userVO.getDisplayName()}'"
                 def result = experimentService.getAllExperimentsByUserId(liUserId)
                 resultData = FormatResultUtil.getInstance().prepareExperimentsResult(result,params,isExport)

                 if( result == null || result.size()==0 ){
                    resultData.putAt "ErrorMsg","No Recoed Found"
                 }else{
                    resultData?.putAt "TotalRecordCount",result.size()
                    resultData?.putAt "isExport",true
                 }

            }else if(lookupBy!=null && lookupBy.toString().trim().equalsIgnoreCase("classes")){

                 title="Class List By User '${userVO.getDisplayName()}'"
                 def experimentsIds = experimentService.getExperimentIdsByUserId(liUserId)
                 def classIds = experimentClassService.getClassIdsByExperimentIds(experimentsIds)
                 def result = experimentClassService.getClassesByClassIds(classIds)
                 resultData = FormatResultUtil.getInstance().prepareClassesResult(result,params,isExport)

                 if( result == null || result.size()==0 ){
                    resultData.putAt "ErrorMsg","No Recoed Found"
                 }else{
                    resultData?.putAt "TotalRecordCount",result.size()
                    resultData?.putAt "isExport",true
                 }


            }else if(lookupBy!=null && lookupBy.toString().trim().equalsIgnoreCase("samples")){
             title="Sample List By User '${userVO.getDisplayName()}'"
             def experimentsIds = experimentService.getExperimentIdsByUserId(liUserId)
             def result = classSampleService.getSamplesByExperimentIds(experimentsIds)

             resultData = FormatResultUtil.getInstance().prepareSamplesResult(result,params,isExport)

             if( result == null || result.size()==0 ){
                resultData.putAt "ErrorMsg","No Recoed Found"
             }else{
                resultData?.putAt "TotalRecordCount",result.size()
                resultData?.putAt "isExport",true
             }
          }
      }


        // Export Result
        if(params?.format && params.format != "html"){
          response.contentType = ConfigurationHolder.config.grails.mime.types[params.format]
          response.setHeader("Content-disposition", "attachment; filename=${lookupBy}LookUpByUser.${params.extension}")
          if(resultData?.parameters?.title!=null){
            resultData?.parameters?.title = title
          }
          exportService.export(params.format, response.outputStream, resultData.resultList, resultData.fieldList, resultData.labelMap, resultData.formatters, resultData.parameters)
        }

      gLogger.info 'out from action:datalist of controller:owner'
      return ["userName":userVO?.getDisplayName(),"lookupBy":lookupBy,"title":title,"userInfo":userVO,"result": resultData]
    }

    def exportexp={
      gLogger.info 'coming into action:exportexp of controller:owner'
      gLogger.info "params :: ${params}"
      def userId = params.id

      Long liUserId = 0L
      if( ValidationUtil.getInstance().isLong(userId) ){
        liUserId = Long.parseLong(userId)
      }
      def result = experimentService.getAllExperimentsByUserId(liUserId)

      gLogger.info "result :: ${result}"

      def workBook = ExportExperimentUtil.getInstance().exportExperiment(result)
      gLogger.info "workBook :: ${workBook}"

      // Export Result
      if(params?.format && params.format != "html"){

        UserVO userVO = userService.getUserById(liUserId)
        String fileName = "User"
        if(userVO!=null){
            fileName = userVO.getDisplayName().toString().replace(" ","_")
        }

        response.contentType = ConfigurationHolder.config.grails.mime.types[params.format]
        response.setHeader("Content-disposition", "attachment; filename=ExperimentsOf_${fileName}.${params.extension}")

        File temp = File.createTempFile("${fileName}", ".${params.extension}");
        temp.deleteOnExit();

        // The Output file is where the xls will be created
        FileOutputStream fOut = new FileOutputStream(temp);
        // Write the XL sheet
        workBook.write(fOut);
        fOut.flush();
        // Done Deal..
        fOut.close();

        response.outputStream << temp.getBytes()
        response.outputStream.flush()
      }
      gLogger.info 'out from action:exportexp of controller:owner'
      return
    }

    def list={
      //set lookup params
      params.put("cmbSearchBy","User")
      params.put("searchText","*")


      def searchBy = params.cmbSearchBy
      def searchText = params.searchText

      // Export Result
      def resultData = []
      if(params?.format && params.format != "html"){
          response.contentType = ConfigurationHolder.config.grails.mime.types[params.format]
          response.setHeader("Content-disposition", "attachment; filename=${searchBy}List.${params.extension}")
          resultData = processSearch(searchBy,searchText,params,true)

          if(resultData?.parameters?.title!=null){
              resultData?.parameters?.title = "Users List"
          }

          exportService.export(params.format, response.outputStream, resultData.resultList, resultData.fieldList, resultData.labelMap, resultData.formatters, resultData.parameters)
      }else{

          resultData = processSearch(searchBy,searchText,params)
    }


    return [result: resultData]
  }


  private def processSearch(def searchBy,def searchText, def params = [:],boolean isExport = false){
    //call the service to search the data
      def resultSearch = searchService.search(searchBy, searchText, params)

      Collection<Map<?, ?>> result = resultSearch?.result
      def resultData = null
      resultData = FormatResultUtil.getInstance()."prepare${searchBy}Result"(result,params,isExport)

      if( resultSearch?.ErrorMsg != null ){
        resultData.putAt "ErrorMsg",resultSearch?.ErrorMsg
      }

      resultData?.putAt "isPaginate",resultSearch?.isPaginate
      resultData?.putAt "TotalRecordCount",resultSearch?.TotalRecordCount
      resultData?.putAt "isExport",resultSearch?.isExport

      return resultData
  }
}
