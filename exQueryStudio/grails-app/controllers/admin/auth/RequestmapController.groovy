package admin.auth

import auth.entity.Requestmap
import auth.entity.SecRole

class RequestmapController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 15, 100)
        [requestmapInstanceList: Requestmap.list(params), requestmapInstanceTotal: Requestmap.count()]
    }

    def create = {
        def requestmapInstance = new Requestmap()
        requestmapInstance.properties = params

        def roleList = SecRole.getAll()
        return [requestmapInstance: requestmapInstance,configAttributeList:roleList?.sort()]
    }

    def save = {
        def requestmapInstance = new Requestmap(params)

        // Start: prepare configAttribute String
        if (requestmapInstance.configAttribute != params.configAttribute) {
          def configAttribute = configAttribute(params.configAttribute)
          params.configAttribute = configAttribute
        }
        requestmapInstance.properties = params
        // End: prepare configAttribute String

        if (requestmapInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), requestmapInstance.id])}"
            redirect(action: "show", id: requestmapInstance.id)
        }
        else {
            render(view: "create", model: [requestmapInstance: requestmapInstance])
        }
    }

    def show = {
        def requestmapInstance = Requestmap.get(params.id)
        if (!requestmapInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])}"
            redirect(action: "list")
        }
        else {
            [requestmapInstance: requestmapInstance]
        }
    }

    def edit = {
        def requestmapInstance = Requestmap.get(params.id)
        if (!requestmapInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])}"
            redirect(action: "list")
        }
        else {

            def roleList = SecRole.getAll()
            return [requestmapInstance: requestmapInstance,configAttributeList:roleList?.sort()]
        }

    }

    def update = {
        def requestmapInstance = Requestmap.get(params.id)

        System.out.println("params :: "+params)

        // Start: prepare configAttribute String
        if (requestmapInstance.configAttribute != params.configAttribute) {
          def configAttribute = configAttribute(params.configAttribute)
          params.configAttribute = configAttribute
        }
        requestmapInstance.properties = params
        // End: prepare configAttribute String

        System.out.println("params :: "+params)

        def roleList = SecRole.getAll()


        if (requestmapInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (requestmapInstance.version > version) {

                    requestmapInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'requestmap.label', default: 'Requestmap')] as Object[], "Another user has updated this Requestmap while you were editing")
                    render(view: "edit", model: [requestmapInstance: requestmapInstance,configAttributeList:roleList?.sort()])
                    return
                }
            }
            requestmapInstance.properties = params
            if (!requestmapInstance.hasErrors() && requestmapInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), requestmapInstance.id])}"
                redirect(action: "show", id: requestmapInstance.id)
            }
            else {
                render(view: "edit", model: [requestmapInstance: requestmapInstance,configAttributeList:roleList?.sort()])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def requestmapInstance = Requestmap.get(params.id)
        if (requestmapInstance) {
            try {
                requestmapInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])}"
            redirect(action: "list")
        }
    }

  private String configAttribute(def configAttribute){

        String configAttributeResult = "";
        if(configAttribute instanceof Collection ){
          for(int index=0; index<configAttribute.size(); index++){
            String string = configAttribute.get(index);
            if( configAttributeResult.length() == 0 ){
               configAttributeResult = string
            }else{
               configAttributeResult += ", " + string
            }
          }
        }else{
          configAttributeResult = configAttribute
        }
        return configAttributeResult
    }


}
