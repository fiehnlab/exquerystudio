package admin.auth

import auth.entity.SecRole
import auth.entity.SecUser
import auth.entity.SecUserSecRole

class UserController {

    def springSecurityService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 15, 100)
        [userInstanceList: SecUser.list(params), userInstanceTotal: SecUser.count()]
    }

    def create = {
        def userInstance = new SecUser()
        userInstance.properties = params

        def roleList = SecRole.getAll()
        return [userInstance: userInstance,allAuthorities:roleList]
    }

    def save = {
        def userInstance = new SecUser(params)
        // Start : Password Encryption
        userInstance.password = springSecurityService.encodePassword(params.password)
        // End : Password Encryption

        if (userInstance.save(flush: true)) {
            // Start: Add User and Role Mapping
            def authoritie = params.authoritie
            SecRole roleInstance

            if(authoritie!=null){
               SecUserSecRole.removeAll(userInstance)

               authoritie.each { roleId->
                  Long id = Long.parseLong(roleId)
                  roleInstance = SecRole.get(id)
                  if(roleInstance!=null){
                    SecUserSecRole.create userInstance, roleInstance, true
                  }
               }
            }
            // End: Add User and Role Mapping
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])}"
            redirect(action: "show", id: userInstance.id)
        }
        else {
            render(view: "create", model: [userInstance: userInstance])
        }
    }

    def show = {
        def userInstance = SecUser.get(params.id)
        if (!userInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])}"
            redirect(action: "list")
        }
        else {
            [userInstance: userInstance]
        }
    }

    def edit = {
        def userInstance = SecUser.get(params.id)
        if (!userInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])}"
            redirect(action: "list")
        }
        else {
            def roleList = SecRole.getAll()
            return [userInstance: userInstance,allAuthorities:roleList]
        }
    }

    def update = {
        def userInstance = SecUser.get(params.id)

        // Start : Password Encryption
        if (userInstance.password != params.password) {
          params.password = springSecurityService.encodePassword( params.password)
        }
        userInstance.properties = params
        // End : Password Encryption
        if (userInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (userInstance.version > version) {
                    def roleList = SecRole.getAll()

                    userInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'user.label', default: 'User')] as Object[], "Another user has updated this User while you were editing")
                    render(view: "edit", model: [userInstance: userInstance,allAuthorities:roleList])
                    return
                }
            }
            userInstance.properties = params
            if (!userInstance.hasErrors() && userInstance.save(flush: true)) {

                // Start: Add User and Role Mapping
                def authoritie = params.authoritie
                SecRole roleInstance

                if(authoritie!=null){
                   SecUserSecRole.removeAll(userInstance)

                   authoritie.each { roleId->
                      Long id = Long.parseLong(roleId)
                      roleInstance = SecRole.get(id)
                      if(roleInstance!=null){
                        SecUserSecRole.create userInstance, roleInstance, true
                      }
                   }
                }
                // End: Add User and Role Mapping
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])}"
                redirect(action: "show", id: userInstance.id)
            }
            else {
                def roleList = SecRole.getAll()
                render(view: "edit", model: [userInstance: userInstance,allAuthorities:roleList])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def userInstance = SecUser.get(params.id)
        if (userInstance) {
            try {
                userInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'user.label', default: 'User'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])}"
            redirect(action: "list")
        }
    }
}
