package exquery

/**
 * author: Larry Headlund
 * modifier: Pradeep K Haldiya
 */

class RadioGroupTagLib implements com.opensymphony.module.sitemesh.RequestConstants{

  def out // to facilitate testing

  protected getPage() {
    return request[PAGE]
  }

  /*
    .radiogroup{
      font: 11px verdana, arial, helvetica, sans-serif;
      margin-right: 5px;
    }

  */
  
  def renderRadio = { name, value, label, checked, remainingAttributes  ->
      out << '<span class="radiogroup">' //add by Pradeep
      out << '<INPUT TYPE=radio '
      out << "NAME='${name}' "
      out << "VALUE='${value.toString().encodeAsHTML()}' "
      if(checked) {
        out << 'checked '
      }
      outputAttributes(remainingAttributes)
      out << " ></INPUT> "
      out << "${label}"
      out << " </span>"
  }

  /**
   * A helper tag for creating HTML radiogroups following the select helper tag
   *
   * Examples:
   * <g:radiogroup name="user.age" from="${18..65}" value="${age}" />
   * <g:radiogroup name="user.company.id" from="${Company.list()}" value="${user?.company.id}" optionKey="id" />
     *
     * It has the additional attribute 'horizontal' ( which
     * determines if the layour is horizontal or vertical
   */
  def radiogroup = { attrs ->
    def name = attrs.remove('name')
    def from = attrs.remove('from')
    def keys = attrs.remove('keys')
    def radioKey = attrs.remove('radioKey')
    def radioValue = attrs.remove('radioValue')
    def value = attrs.remove('value')
    def horizontal = attrs.remove('horizontal')
    def noSelection = attrs.remove('noSelection')
    def noSelectionChecked = (attrs.remove('noSelectionChecked') ? true : false)

    // create radio buttons from list
    if(from) {
      from.eachWithIndex { el,i ->
        def val = "";
        def label = "";
        def checked = false;

        if(keys) {
          val = keys[i]
          if(keys[i] == value) {
            checked = true;
          }
        } else if(radioKey) {
          def keyValue = null
          if(radioKey instanceof Closure) {
            keyValue = radioKey(el)
            val = keyValue
          } else if(el !=null && radioKey == 'id' && grailsApplication.getGrailsDomainClass(el.getClass().name)) {
            keyValue = el.ident()
            val = keyValue
          } else {
            //keyValue = el.properties[radioKey]
            keyValue = el.getAt(radioKey) // Modified By Pradeep
            val = keyValue
          }

          if(keyValue == value) {
            checked = true
          }
        } else {
          val = el
          if(el == value) {
            checked = true
          }
        }
        if(radioValue) {
          if(radioValue instanceof Closure) {
            label = radioValue(el).toString().encodeAsHTML()
          } else {
            //label = el.properties[radioValue].toString().encodeAsHTML()
            label = el.getAt(radioValue)?.toString().encodeAsHTML() // Modified By Pradeep
          }
        } else {
          def s = el.toString()
          if(s) {
            label = s.encodeAsHTML()
          }
        }
        renderRadio(name, val, label, checked,attrs)
        if (horizontal != "true") {
          out << "<br>"
        }
        //out << "n" // Modified By Pradeep
      }
    }

    if (noSelection != null) {
      noSelection = noSelection.entrySet().iterator().next()
      renderRadio(name, noSelection.key, noSelection.value, noSelectionChecked,attrs)
    }
  }

  /**
   * Dump out attributes in HTML compliant fashion
   */
  void outputAttributes(attrs)
  {
      attrs.remove( 'tagName') // Just in case one is left
      attrs.each { k,v ->
          out << k << "='" << v.encodeAsHTML() << "'"
      }
  }
}
