package setupx.importexport

class ImportExportLog {

    Date startDate
    Date endDate
    Boolean status

	static mapping = {
        table 'import_export_log'
        version false
        id column: 'import_export_log_id'
	}

	static constraints = {
		startDate nullable: true
        endDate nullable: true
	}
}
