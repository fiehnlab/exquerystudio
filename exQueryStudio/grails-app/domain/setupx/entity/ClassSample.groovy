package setupx.entity

class ClassSample {

    static mapping = {
          table 'classsample'
          version false
          columns {
              id column:'classsample_id'
              //experimentclass column:'class_id'
              sampleChromatofSet joinTable:[name:'SampleChromatof', key:'classsample_id', column:'samplechromatof_id']
              sampleMetadataSet joinTable:[name:'SampleMetadata', key:'classsample_id', column:'samplemetadata_id']
          }
    }

    static belongsTo = [experimentclass:ExperimentClass]

    static hasMany = [sampleChromatofSet:SampleChromatof,sampleMetadataSet:SampleMetadata]

    String label
    String comments
    String event

    // Transients
    static transients = ['sampleMetaData']
    /** Sample Metadata */
    def sampleMetaData

    static constraints = {
      label(maxSize: 255, nullable: true)
      comments(maxSize: 1000, nullable: true)
      event(maxSize: 255, nullable: true)
    }

}
