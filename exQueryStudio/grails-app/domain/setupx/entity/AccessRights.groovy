package setupx.entity

/* Domain class to access rights */
class AccessRights {

    static mapping = {
      table 'access_rights'
      version false
      columns {
          id column:'access_rights_id'
          experimentUserRights joinTable:[name:'ExperimentUserRights', key:'access_rights_id', column:'experiment_user_rights_id']
      }
    }  

    static hasMany = [experimentUserRights:ExperimentUserRights]
    String right_description
    String remarks

    static constraints = {
      right_description(maxSize: 200, nullable: false)
      remarks(maxSize: 500, nullable: true)
    }

}
