package setupx.entity

class ExperimentClass {

    static mapping = {
          table 'experimentclass'
          version false
          columns {
            id column:'experimentclass_id'
            classMetadataSet joinTable:[name:'ClassMetadata', key:'experimentclass_id', column:'classmetadata_id']
            sampleSet joinTable:[name:'ClassSample', key:'experimentclass_id', column:'classsample_id']
            organSet joinTable:[name:'ClassOrgan', key:'experimentclass_id', column:'classorgan_id']
            speciesSet joinTable:[name:'ClassSpecies', key:'experimentclass_id', column:'classspecies_id']
          }
      }

    static belongsTo = [experiment:Experiment]
    static hasMany = [classMetadataSet:ClassMetadata,sampleSet:ClassSample,organSet:ClassOrgan,speciesSet:ClassSpecies]

    String genotype
    String green_house
    String preparation
    String information
    String method_name
    String detailed_location
    String extraction
    
    static constraints = {
        genotype(maxSize: 255, nullable: true)
        green_house(maxSize: 500, nullable: true)
        preparation(maxSize: 6000, nullable: true)
        information(maxSize: 6000, nullable: true)
        method_name(maxSize: 255, nullable: true)
        detailed_location(maxSize: 6000, nullable: true)
        extraction(maxSize: 500, nullable: true)
    }
}