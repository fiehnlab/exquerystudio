package setupx.entity

class Experiment {

    static mapping = {
          table 'experiment'
          version false
          columns {
              id column:'experiment_id'
              experimentclassSet joinTable:[name:'ExperimentClass', key:'experiment_id', column:'experimentclass_id']
              experimentUserRights joinTable:[name:'ExperimentUserRights', key:'experiment_id', column:'experiment_user_rights_id']
              experimentPublication joinTable:[name:'ExperimentPublication', key:'experiment_id', column:'experimentpublication_id']
          }
      }

    static hasMany = [experimentclassSet:ExperimentClass,experimentUserRights:ExperimentUserRights,experimentPublication:ExperimentPublication]

    String title
    String experiment_abstract
    String description
    String comments
    
    static constraints = {
      title(maxSize: 500, nullable: true)
      experiment_abstract(maxSize: 4000, nullable: true)
      description(maxSize: 2000, nullable: true)
      comments(maxSize: 4000, nullable: true)
    }
  
}
