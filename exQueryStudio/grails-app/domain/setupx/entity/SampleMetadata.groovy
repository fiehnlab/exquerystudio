package setupx.entity

class SampleMetadata {

    static mapping = {
      table 'samplemetadata'
      version false
      columns {
         id column:'samplemetadata_id'
      }
    }

    static belongsTo = [classSample:ClassSample]

    String fieldLogicalId
    String fieldTitle
    String fieldValue

    static constraints = {
      fieldLogicalId(maxSize: 500,nullable: true)
      fieldTitle(maxSize: 500,nullable: true)
      fieldValue(maxSize: 4000,nullable: true)
    }
}
