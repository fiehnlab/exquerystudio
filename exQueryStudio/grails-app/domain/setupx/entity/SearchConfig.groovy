package setupx.entity

class SearchConfig {

    static mapping = {
              table 'searchconfig'
              version false
    }

    String object
    String title
    Long orderBy
    String groupkey
    String description
    String objectAs
    String objectDbType = "VARCHAR"
    String db_object

    static constraints = {
      object(maxSize: 500,nullable: false)
      title(maxSize: 500, nullable: false)
      groupkey(maxSize: 10,nullable: false)
      description(maxSize: 2000, nullable: true)
      objectAs(maxSize: 10,nullable: true)
      orderBy(nullable: true)
      objectDbType(maxSize: 25, nullable: true)
      db_object(maxSize: 500,nullable: false)
    }
}
