package setupx.entity

class ExperimentPublication {

    static mapping = {
          table 'experimentpublication'
          version false
          columns {
            id column:'experimentpublication_id'
          }
      }

    static belongsTo = [experiment:Experiment]

  static constraints = {
      label(maxSize: 500, nullable: true)
      publishedDate(nullable: false)      
    }

    Long publishedBy
    Date publishedDate
    String label
}
