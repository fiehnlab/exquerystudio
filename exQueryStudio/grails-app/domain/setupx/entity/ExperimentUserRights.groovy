package setupx.entity

import auth.entity.SecUser

class ExperimentUserRights {

    static mapping = {
        table 'experiment_user_rights'
        version false
        columns {
        id column:'experiment_user_rights_id'
      }
    }

    static belongsTo = [experiment:Experiment,accessRights:AccessRights,secUser:SecUser]
    String remarks
  

    static constraints = {
      remarks(maxSize: 500, nullable: true)
    }
}
