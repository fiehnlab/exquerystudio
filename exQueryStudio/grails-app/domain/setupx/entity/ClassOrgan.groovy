package setupx.entity

class ClassOrgan {

    static mapping = {
          table 'classorgan'
          version false
          columns {
             id column:'classorgan_id'
             //experimentclass column:'class_id'
          }
    }

    static belongsTo = [experimentclass:ExperimentClass]

    Long organ_id
    String organ_name
    String tissue
    String cell_type
    String subcellular_celltype

    static constraints = {
      organ_id(nullable: false)
      organ_name(maxSize: 255, nullable: true)
      tissue(maxSize: 255, nullable: true)
      cell_type(maxSize: 255, nullable: true)
      subcellular_celltype(maxSize: 255, nullable: true)
    }
}
