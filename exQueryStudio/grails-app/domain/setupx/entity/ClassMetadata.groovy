package setupx.entity

class ClassMetadata {

    static mapping = {
      table 'classmetadata'
      version false
      columns {
         id column:'classmetadata_id'
      }
    }

    static belongsTo = [experimentclass:ExperimentClass]

    String fieldLogicalId
    String fieldTitle
    String fieldValue

    static constraints = {
      fieldLogicalId(maxSize: 500,nullable: true)
      fieldTitle(maxSize: 500,nullable: true)
      fieldValue(maxSize: 4000,nullable: true)
    }
}
