package setupx.entity

class SampleChromatof {

    static mapping = {
          table 'samplechromatof'
          version false
          columns {
             id column:'samplechromatof_id'
          }
      }

    static belongsTo = [classSample:ClassSample]

    String chromatofFileId
    String dateCreated
    String dateDetected
    
    static constraints = {
      chromatofFileId(maxSize: 500,nullable: false)
      dateCreated(maxSize: 50,nullable: false)
      dateDetected(maxSize: 50,nullable: false)
    }
}
