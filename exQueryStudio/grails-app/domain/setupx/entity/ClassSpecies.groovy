package setupx.entity

class ClassSpecies {

    static mapping = {
          table 'classspecies'
          version false
          columns {
              id column:'classspecies_id'
              //experimentclass column:'class_id'
          }
     }
    static belongsTo = [experimentclass:ExperimentClass]

    Long species_id
    String species_name

    static constraints = {
      species_id(nullable: false)
      species_name(maxSize: 255, nullable: true)
    }
}
