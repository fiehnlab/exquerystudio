package auth.entity

class Requestmap {

	String url
	String configAttribute
    String remarks

	static mapping = {
        version false
        id column: 'request_id'
		cache true
	}

	static constraints = {
		url blank: false, unique: true
		configAttribute blank: false
        remarks nullable: true
	}
}