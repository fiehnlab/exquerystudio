package auth.entity

import setupx.entity.ExperimentUserRights

class SecUser {

	String username
    String displayname
	String password
	boolean enabled
    String email
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static constraints = {
		username blank: false, unique: false
        displayname blank: false,nullable: false
		password blank: false,nullable: false
        email blank: true,nullable: true
	}

	static mapping = {
      table 'secuser'
      version false
	  id column: 'sec_user_id'
      password column: 'password'
      experimentUserRights joinTable:[name:'ExperimentUserRights', key:'sec_user_id', column:'experiment_user_rights_id']

	}


    static hasMany = [experimentUserRights:ExperimentUserRights]


	Set<SecRole> getAuthorities() {
		SecUserSecRole.findAllBySecUser(this).collect { it.secRole } as Set
	}
}
