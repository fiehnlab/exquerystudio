package auth.entity

class SecRole {

	String authority
    String description
    String remarks

	static mapping = {
        table 'secrole'
        version false
        id column: 'sec_role_id'
		cache true
	}


	static constraints = {
		authority blank: false, unique: true
        description nullable: true
        remarks nullable: true
	}
}
