package taxonomy

class TaxonomyNodes {

    static mapping = {
          table 'taxonomy_nodes'
          version false
          columns {
              id column:'tax_id'
          }
    }

    static belongsTo = [division:TaxonomyDivisions]
    static hasMany = [taxonomyNamesSet:TaxonomyNames]

    String rank
    String prefix
    String comments
    TaxonomyNodes parentTax

    static constraints = {
      rank(nullable: false)
      prefix(nullable: true)
      comments(nullable: true)
    }
}
