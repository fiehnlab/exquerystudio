package taxonomy

class TaxonomyDivisions {

    static mapping = {
          table 'taxonomy_divisions'
          version false
          columns {
              id column:'division_id'
          }
    }

    static hasMany = [taxonomyNodesSet:TaxonomyNodes]

    String divisionCode
    String divisionName
    String comments

    static constraints = {
      divisionCode(nullable: false)
      divisionName(nullable: false)
      comments(nullable: true)
    }
}
