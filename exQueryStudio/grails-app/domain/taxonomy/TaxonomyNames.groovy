package taxonomy

class TaxonomyNames {

    static mapping = {
          table 'taxonomy_names'
          version false
          columns {
              id column:'tax_name_id'
          }
    }

    static belongsTo = [tax:TaxonomyNodes]

    String nameTxt
    String uniqueName
    String nameClass

    static constraints = {
      nameTxt(nullable: false)
      uniqueName(nullable: true)
      nameClass(nullable: false)
    }

    public TaxonomyNames findByNameTxt1(String nameTxt){
        return TaxonomyNames.find(" where upper(nameTxt)= :nameTxt",["nameTxt":nameTxt.toUpperCase()])
    }
}
