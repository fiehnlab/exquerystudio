<%--
  User: pradeep
  Date: Nov 30, 2010
  Time: 2:56:17 PM
  Sample Search Result Page
--%>
<%@ page contentType="text/html;charset=UTF-8" %>


<html>
<head>
  <meta name="layout" content="main"/>
</head>

<body>
<div id="container">
       <div id="header">
            <g:render template="../templates/header"/>
       </div>

      <div id="main">
        <div class="box">

         <div class="nav">
            <span class="menuButton"><g:link controller="homePage" action="index" class="home">Home</g:link></span>
            <span class="info_alert" onclick="window.open('${resource(dir:'',file:'')}/help.gsp?application=samplesextraction', 'HelpDocs', 'menubar=0,resizable=0,location=1,status=0,scrollbars=1,  width=800,height=600');" target="_blank" title="info">&nbsp;&nbsp;&nbsp;</span>
         </div>



       <g:form name="searchForm" controller="query">
       <div class="groupbox">

         <div class="appsHeader"> <g:message code="query.samples.title" /> </div>
         <g:render template="../templates/error"/>
         <div class="info">

            <table cellpadding="0" border="0" cellspacing="0" class="table_noborder">
              <tr>
                <td width="10%" class="label_homepage"> <g:message  code="query.samples.ids" /></td>
                <td width="2%" class="label_homepage"> : </td>
                <td width="30%">
                  <g:textArea id="txtIds" name="txtIds" class="full-width" value="${params?.txtIds}">${params?.txtIds}</g:textArea>
                 </td>
                <td width="48%">
                  <div class="message"><g:message code="samplesextraction.ids.info" /></div>
                </td>
              </tr>

              <tr>
                <td width="10%" class="label_homepage"> <g:message  code="query.samples.idtype" /> </td>
                <td width="2%" class="label_homepage"> : </td>
                <td width="30%">
                  <g:radiogroup	radioKey="ID" radioValue="VALUE"	from="${sxObjectType}"	name="rdType" horizontal="true" value="${params?.rdType}"/>
                </td>
                <td width="48%">
                  <div class="message"><g:message code="samplesextraction.type.info" /></div>
                </td>
              </tr>

            </table>

         </div>
         </div>
          <div class="buttons">
             <span class="button"><g:actionSubmit class="submit" action="samples_result" value="${message(code: 'default.button.submit.label', default: 'Search')}" /></span>                    
          </div>
        </div>
        <!-- End div groupbox -->
          </g:form>
        <div class="groupbox">
          <g:render template="../templates/blank"/>
        </div>
    </div>
    <!-- End div main -->

  <div id="footer">
     <g:render template="../templates/footer"/>
  </div>
</div>


</body>
</html>