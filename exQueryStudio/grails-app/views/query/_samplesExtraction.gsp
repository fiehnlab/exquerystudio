<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: 12/24/10
  Time: 5:43 PM
  Samples Extraction application template
--%>

       <g:form name="searchForm" controller="query">
       <div class="groupbox">

         %{--<g:render template="../templates/error"/>--}%
         <div class="info">

            <table cellpadding="0" border="0" cellspacing="0" class="table_noborder">
              <tr>
                <td width="10%" class="label_homepage"> <g:message  code="query.samples.ids" /></td>
                <td width="2%" class="label_homepage"> : </td>
                <td width="40%">
                  <g:textArea id="txtIds" name="txtIds" class="full-width" value="${params?.txtIds}" title="${message(code: 'samplesextraction.ids.info', default: 'Ids should be comma(,) or tab or new line separated')}">${params?.txtIds}</g:textArea>
                 </td>

              </tr>

              <tr>
                <td width="10%" class="label_homepage"> <g:message  code="query.samples.idtype" /> </td>
                <td width="2%" class="label_homepage"> : </td>
                <td width="40%" valign="middle" title="${message(code: 'samplesextraction.type.info', default: 'select the appropriate id type')}">
                  <g:radiogroup	radioKey="ID" radioValue="VALUE"	from="${sxObjectType}"	name="rdType" horizontal="true" value="${params?.rdType}"/>
                </td>

              </tr>

            </table>

         </div>
       </div>

        <div class="buttons">
             <span class="button"><g:actionSubmit class="submit" action="samples_result" value="${message(code: 'default.button.submit.label', default: 'Search')}" /></span>
        </div>
        <!-- End div groupbox -->
          </g:form>