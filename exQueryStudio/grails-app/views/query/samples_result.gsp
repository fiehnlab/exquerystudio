<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Jul 1, 2010
  Time: 12:28:13 PM
  Search Result Page
--%>

<%@ page contentType="text/html;charset=UTF-8" %>


<html>
<head>
  <meta name="layout" content="main"/>
  <export:resource />
</head>

<body id="search">
<div id="container">
       <div id="header">
            <g:render template="../templates/header"/>
       </div>
      <!-- Start div main -->
      <div id="main">
        <div class="box">

         
          <div class="nav">
            <span class="menuButton"><g:link controller="homePage" action="index" class="home">Home</g:link></span>
            <span class="menuButton"><g:link controller="query" action="samples_search" class="search" params="[rdType:params.rdType]">Sample Search</g:link></span>
          </div>

          <div class="appsHeader"> Search Result </div>


      <div class="info">

      <table class="result">

        <%
          if (result?.fieldList != null && result?.fieldList?.size() > 0) {
        %>
       <tr>
        <g:each var="headerField" in="${result.fieldList}">
        <%
              def cssClass = result.cssClassMap.get(headerField)
              def header = result.labelMap.get(headerField)
        %>
          <th class="${cssClass}">${header}</th>

        </g:each>
        </tr>

        <%
          }  %>

        <%
          if (result?.resultList != null && result?.resultList.size() > 0) {
            long rowNo=1;
        %>
           <g:each var="row" in="${result.resultList}" status="index">
           <%
              String lStrClass = "";

              if (index % 2 == 0) {
                lStrClass = "even"
              } else {
                lStrClass = "odd"
              }
          %>

          <tr class='<%=lStrClass%>' onMouseOver="this.className='highlight'" onMouseOut="this.className='<%=lStrClass%>'" >

            <g:each var="columnfield" in="${result.fieldList}">
            <%
                  cssClass = result.cssClassMap.get(columnfield)
                  def data = row.get(columnfield)
            %>
            <td class="${cssClass}">${data}</td>
            </g:each>
          </tr>
        </g:each>

        <%
          } else {
        %>
        <tr>
          <td colspan="${result?.fieldList?.size}" class='even'>
            <div class="errors">

              ${result?.ErrorMsg}

            </div>
          </td>

        </tr>
        <% }
        %>
      </table>

     <% if( result?.TotalRecordCount != null ){
     %>
     <div class="pagination_top">
       <g:customPaginate isPaginate="false" total="${result?.TotalRecordCount}" />
     </div>
     <% } %>
     <export:formats formats="['csv', 'excel', 'pdf', 'rtf']" params="[rdType:params.rdType]" />
        
      </div>
      <!-- End div info : result -->
    </div>
    <!-- End div box -->
     <div class="groupbox">
       <g:render template="../templates/blank"/>
     </div>


    </div>
    <!-- End div main -->
    <div id="footer">
       <g:render template="../templates/footer"/>
    </div>
</div>
</body>

</html>