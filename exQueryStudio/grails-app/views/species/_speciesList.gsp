<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: 5/6/11
  Time: 12:38 PM
  To change this template use File | Settings | File Templates.
--%>

<div>
    <g:if test="${sxSpeciesList!=null && sxSpeciesList?.size()>0}">

        <%
            int maxCount=10
            def species
            for(int index=0; index < sxSpeciesList.size() && index < maxCount; index++){
                species = sxSpeciesList.get(index)

        %>
        %{--<g:each in="${sxSpeciesList}" var="species" status="index">--}%

            <g:if test="${index==0}">
                <span style="cursor:pointer;" title="ExperimentCount:${species.experimentCount}, ClassesCount:${species.classesCount} and SamplesCount:${species.samplesCount}">${species.speciesName}[${species.samplesCount}]</span>
            </g:if>
            <g:else>
                ;&nbsp;&nbsp;<span style="cursor:pointer;" title="ExperimentCount:${species.experimentCount}, ClassesCount:${species.classesCount} and SamplesCount:${species.samplesCount}">${species.speciesName}[${species.samplesCount}]</span>
            </g:else>
        %{--</g:each>--}%
        <%
            }
        %>
        <g:if test="${sxSpeciesList.size()>maxCount}">
             <span style="cursor:pointer;" > <g:link controller="species" action="list" title="click to get the list of species">more...</g:link> </span>
        </g:if>
    </g:if>
    <g:else>
       <div class="errors">
        No Species
       </div>
    </g:else>
</div>