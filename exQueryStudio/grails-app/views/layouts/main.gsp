<html>
    <head>
        <title><g:layoutTitle default="${message(code: 'ProjectName', default: 'SetupX-Query')}" /></title>
        <link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
        <link rel="stylesheet" href="${resource(dir:'css',file:'formlayout.css')}" />
        <link rel="shortcut icon" href="${resource(dir:'images',file:'SetupXQueryLogo.png')}" />
       <!-- <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" /> -->
        <g:layoutHead />
        <g:javascript library="application" />
        <ofchart:resources/>
    </head>
    <body>
        <div id="spinner" class="spinner" style="display:none;">
            <img src="${resource(dir:'images',file:'spinner.gif')}" alt="Spinner" />
        </div>
        
        <g:layoutBody />
    </body>
</html>