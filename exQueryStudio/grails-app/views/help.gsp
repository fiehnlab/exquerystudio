<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: 2/25/11
  Time: 1:59 PM
  To change this template use File | Settings | File Templates.
--%>

<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="layout" content="main" />
      <link rel="stylesheet" href="${resource(dir:'css',file:'help.css')}" />
      <title>Help Docs</title>
  </head>

  <body>
      <div class="helpbox">

        <div class="menuButton">
           <g:link onclick="window.close()" class="close" title="Close">Close</g:link>
        </div>
          <% if(request?.getParameter("application") != null){%>
            <g:render template='../help/templates/${request.getParameter("application") }' />
          <% } %>
      </div>
      <br>
  </body>
</html>