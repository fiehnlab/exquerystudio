
<%@ page import="auth.entity.SecUser" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title>${message(code: 'ProjectName', default: 'exQueryStudio')}: <g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
    <div id="container">
  <div id="header">
        <g:render template="../templates/header"/>
  </div>

  <!-- Start div main -->
  <div id="main">
    <div class="box">
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="groupbox">
            <div class="appsHeader"><g:message code="default.list.label" args="[entityName]" /></div>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
          <div class="info">
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'user.id.label', default: 'Id')}" />

                            <g:sortableColumn property="displayname" title="${message(code: 'user.displayname.label', default: 'Name')}" />

                            <g:sortableColumn property="username" title="${message(code: 'user.username.label', default: 'Username')}" />
                        
                            %{--<g:sortableColumn property="password" title="${message(code: 'user.password.label', default: 'Password')}" />--}%
                        
                            <g:sortableColumn property="email" title="${message(code: 'user.email.label', default: 'Email')}" />
                        
                            <g:sortableColumn property="enabled" title="${message(code: 'user.enabled.label', default: 'Enabled')}" />
                        
                            <g:sortableColumn property="passwordExpired" title="${message(code: 'user.passwordExpired.label', default: 'Password Expired')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${userInstanceList}" status="i" var="userInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: userInstance, field: "displayname")}</td>

                            <td>${fieldValue(bean: userInstance, field: "username")}</td>
                        
                            %{--<td>${fieldValue(bean: userInstance, field: "password")}</td>--}%
                        
                            <td>${fieldValue(bean: userInstance, field: "email")}</td>
                        
                            <td><g:formatBoolean boolean="${userInstance.enabled}" /></td>
                        
                            <td><g:formatBoolean boolean="${userInstance.passwordExpired}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            %{--<div class="paginateButtons">--}%
                %{--<g:paginate total="${userInstanceTotal}" />--}%
            %{--</div>--}%
          <div class="pagination_bottom">
        <g:customPaginate total="${userInstanceTotal}" prev="&lt; previous" next="next &gt;"/>
      </div>
       </div>
        </div>

    </div>
          </div>
            <!-- End div groupbox -->

            <div class="groupbox">
              <g:render template="../templates/blank"/>
            </div>
        </div>
        <!-- End div main -->

      <div id="footer">
                <g:render template="../templates/footer"/>
      </div>
        </div>

    

    </body>
</html>
