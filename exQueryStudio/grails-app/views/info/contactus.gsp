<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Jul 23, 2010
  Time: 12:43:59 AM
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <link rel="stylesheet" href="${resource(dir:'css',file:'help.css')}" />
        <title>${message(code: 'ProjectName', default: 'exQueryStudio')}: Contact us </title>
</head>

<body>

<div id="container">
  <div id="header">
        <g:render template="../templates/header"/>
  </div>

  <!-- Start div main -->
  <div id="main">
       <div class="groupbox">
           <div class="appsHeader"> Contact us </div>
            <div class="info">
            <table align="center" border="0">
              <tr>
                <td class="info_title" > Name</td>
                <td class="info_gap"> : </td>
                <td class="info_value"> Pradeep K Haldiya  </td>
              </tr>

              <tr>
                <td class="info_title"> Role</td>
                <td class="info_gap"> : </td>
                <td class="info_value"> Project Owner  </td>
              </tr>

              <tr>
                <td class="info_title"> Mail Id </td>
                <td class="info_gap"> : </td>
                <td class="info_value"> <a href="mailto:phaldiya@ucdavis.edu">phaldiya@ucdavis.edu</a> </td>
              </tr>

              <tr>
                <td class="info_title"> Phone </td>
                <td class="info_gap"> : </td>
                <td class="info_value">
                <div>(O): +1-(530)-752-9922</div>
                </td>
              </tr>

              <tr class="prop" >
                  <td class="info_title"> Profile Link </td>
                  <td class="info_gap"> : </td>
                  <td class="info_value"> <a href="http://fiehnlab.ucdavis.edu/staff/haldiya/" target="_blank" > http://fiehnlab.ucdavis.edu/staff/haldiya/ </a> </td>
              </tr>
            </table>
             <br>
            <table align="center" border="0">
              <tr>
                <td class="info_title" > Name</td>
                <td class="info_gap"> : </td>
                <td class="info_value"> Gert Wohlgemuth  </td>
              </tr>

              <tr>
                <td class="info_title"> Role</td>
                <td class="info_gap"> : </td>
                <td class="info_value"> Project Owner  </td>
              </tr>

              <tr>
                <td class="info_title"> Mail Id </td>
                <td class="info_gap"> : </td>
                <td class="info_value"> <a href="mailto:wohlgemuth@ucdavis.edu">wohlgemuth@ucdavis.edu</a> </td>
              </tr>

              <tr>
                <td class="info_title"> Phone </td>
                <td class="info_gap"> : </td>
                <td class="info_value">
                <div>(O): +1-(530)-752-9922</div>
                </td>
              </tr>


              <tr class="prop" >
                  <td class="info_title"> Profile Link </td>
                  <td class="info_gap"> : </td>
                  <td class="info_value"> <a href="http://fiehnlab.ucdavis.edu/staff/wohlgemuth/" target="_blank" > http://fiehnlab.ucdavis.edu/staff/wohlgemuth/ </a> </td>
              </tr>

           </table>
          </div>
       </div>
       <!-- End div groupbox -->
       <div class="groupbox">
            <g:render template="../templates/blank"/>
       </div>
    </div>
    <!-- End div main -->

  <div id="footer">
            <g:render template="../templates/footer"/>
  </div>
    </div>
</div>
</body>
</html>