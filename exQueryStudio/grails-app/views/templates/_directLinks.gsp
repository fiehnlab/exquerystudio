<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Nov 30, 2010
  Time: 1:43:27 PM
  To change this template use File | Settings | File Templates.
--%>

  <ul>
    <li class="listbox"><a href="http://uranus.fiehnlab.ucdavis.edu:8080/minix/" target="_blank">miniX</a>
      <p>BinBase Studie Creation and Calculation Tool </p>
    </li>

    <li class="listbox"><a href="http://eros.fiehnlab.ucdavis.edu:8080/binbase-compound/" target="_blank">BinBase</a>
      <p>To compare mass spectra across more than 60 species in over 24,000 samples. </p>
    </li>

    <li class="listbox"><a href="http://cts.fiehnlab.ucdavis.edu" target="_blank">Chemical Translation Service</a>
      <p>A web-based tool to improve standardization of chemical identifiers.</p>
    </li>

    <li class="listbox"><a href="http://fiehnlab.ucdavis.edu:8080/m1/" target="_blank">SetupX</a>
      <p>A study design database </p>
    </li>

    %{--<li class="controller"><g:link controller="search">Search</g:link>--}%
      %{--<p>Search Description</p>--}%
    %{--</li>--}%
    %{--<li class="controller"><g:link controller="search">Search</g:link>--}%
      %{--<p>Search Description</p>--}%
    %{--</li>--}%
  </ul>