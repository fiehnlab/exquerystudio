<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: 5/4/11
  Time: 7:11 PM
  Species Samples Chart
--%>

<div class="pie_chart">
        <ofchart:chart name="species_pie_chart" url="${createLink(action:'SPECIES_PIE_CHART',controller:'homePage')}" width="100%" height="300" />
</div>