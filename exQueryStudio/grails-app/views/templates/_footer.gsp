<%@ page import="exquery.common.LogUtil" contentType="text/html;charset=UTF-8" %>

<div id="footerBG" align="center" valign="middle">
  <!--<div  class="LabelFooter" align="center" valign="middle" style="padding:5px;">
            <a class="Footer" href="http://binbase.fiehnlab.ucdavis.edu:8080/jira/secure/Dashboard.jspa">Report a problem</a>
          | <a class="Footer" href="http://binbase.fiehnlab.ucdavis.edu:8080/jira/secure/Dashboard.jspa">Request a feature</a>
          | <a class="Footer" href="http://binbase.fiehnlab.ucdavis.edu:8080/jira/secure/Administrators.jspa">Contact administrators</a>
  </div> -->
  <div class="LabelFooter1" align="center" valign="middle" style="padding-top:0px;">
    %{--<p>&nbsp;</p>--}%
  %{--<br>--}%
<table class="table_noborder" width="100%" border="0">
  <tr>
    <td align="left" width="50%">
      <a href="http://www.gnu.org/licenses/lgpl.html">© GNU Lesser General Public License</a>
    </td>
    <td align="right" width="50%">
     <span style="float:right"> Contact:<a href="mailto:phaldiya@ucdavis.edu?subject=Feedback on SetupX-Query">Pradeep K Haldiya</a> </span>
    </td>
  </tr>
  <tr>
    <td align="left"> <a href="http://fiehnlab.ucdavis.edu/"> <font color="#0B3861">Powered By Fiehn Lab, UC Davis</font></a> </td>
    <td align="right">
      <span style="float:right">
       <%  String date = LogUtil.getInstance().getLastUpdateDate()
           if(date!=null&& date.trim().length()>0){
         %>
             <span style="color:#0B3B0B"> Last modified ${date} </span>
       <%}%>
        </span>
    </td>
  </tr>  
</table>
</div>

</div>