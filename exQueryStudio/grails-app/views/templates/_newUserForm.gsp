<g:form name="newUserForm" controller="createUser" method="post" class="newUserForm">

    <table width="100%" cellpadding="0" cellspacing="0" class="table_noborder">
      <tr>
        <td width="30%" class="label_homepage"> name</td>
        <td width="5%" class="label_homepage"> : </td>
        <td width="50%">
          <input type="text"/>
        </td>
      </tr>

      <tr>
        <td width="30%" class="label_homepage"> organisation</td>
        <td width="5%" class="label_homepage"> : </td>
        <td width="50%">
        <input type="text"/>
        </td>        
      </tr>

      <tr>
        <td width="30%" class="label_homepage"> eMail</td>
        <td width="5%" class="label_homepage"> : </td>
        <td width="50%">
        <input type="text"/>
        </td>        
      </tr>

      <tr >
           <td colspan="3" align="left" ><div title="Click to submit" >
            <g:actionSubmit name="submit" action="search" class="buttonSubmit" value="Go"/>
           </div> </td>
      </tr>


      <tr>
        <td colspan="3" class="label_homepage">
          <font color='red'>*</font> Accounts are only needed for active collaborators who submit samples for
        data acquisition to either the UCD metabolomics core laboratory or the UCD Fiehn metabolomics
        research laboratory.
        </td>
      </tr>

    </table>
</g:form>