<%@ page import="java.text.SimpleDateFormat" %>
<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Jul 1, 2010
  Time: 11:28:26 AM
  To change this template use File | Settings | File Templates.
--%>



<div id="projectLogo">
<table class="table_noborder">
 <tr>
   <td>
     <div class="projectHeader" >
       <g:link controller="homePage" action="index" title="${message(code: 'ProjectName', default: 'SetupX-Query')}">
        <img src="${resource(dir:'images',file:'SetupX-Query.png')}" alt="BinBase Scheduler" border="0" width="435px" class="noborder"/>
       </g:link>
     </div>
      </td>
      <td>
        <table class="table_noborder">
          <tr>
            <td>

                <div class="LoginInfo">
                    <table class=table_noborder align="right">
                     <tr>
                       <td>
                         <div align="right">
                           Welcome
                           <sec:ifLoggedIn>  <sec:username/> </sec:ifLoggedIn>
                           <sec:ifNotLoggedIn> Guest </sec:ifNotLoggedIn>

                         </div>

                       </td>
                       <td rowspan="4" align="center" width="25%">
                        <div align="center">
                            <a href="http://www.genomecenter.ucdavis.edu/" title="UC Davis, Genome center" target="_blank">
                             <img src="${resource(dir:'images',file:'UCD_Genome_Logo.png')}" alt="UC Davis, Genome center" border="0" width="150px" align="right" style="vertical-align:bottom" class="noborder"/>
                            </a>
                        </div>
                       </td>
                     </tr>

                    <tr>
                       <td align="right">
                         <div align="right">
                           <%
                             String DATE_FORMAT = "EEE MMM dd, yyyy 'at' hh:mm:ss aaa z";
                             SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
                             String lStrDate = sdf.format(Calendar.getInstance().getTime())
                           %>
                           ${lStrDate}
                         </div>
                       </td>
                    </tr>

                  </table>
                </div>
          </td>
         </tr>

         <tr>
           <td >
             <div align="right" id="homepageMenu">

               <span class="homepageMenu"><g:link controller="homePage" action="index" class="home">Home</g:link></span> |

               <sec:ifNotLoggedIn>
               <span class="homepageMenu"><g:link controller="login" action="index" class="login">Login</g:link></span> |
               %{--<span class="homepageMenu"><g:link controller="login" action="register" class="register">Registration</g:link></span> |--}%
               </sec:ifNotLoggedIn>

               <sec:ifLoggedIn>
               <span class="homepageMenu"><g:link controller="logout" action="index" class="logout" >Logout</g:link></span>  |
               </sec:ifLoggedIn>

               <span class="homepageMenu"><g:link controller="info" action="contactus" class="contactus">Contact us</g:link></span>

             </div>
           </td>

     </tr>
      </table>
   </td>
   <td width="1px">&nbsp;</td>
 </tr>
</table>  
</div>