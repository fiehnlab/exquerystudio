<%--
  User: pradeep
  Date: Sep 30, 2010
  Time: 4:46:35 PM
  Template to show errors
--%>

<g:if test="${flash.message}">

  <div class="errors">
    &nbsp;${flash.message}
  </div>

</g:if>