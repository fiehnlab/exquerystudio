<%@ page contentType="text/html;charset=UTF-8" %>


<html>
<head>
  <meta name="layout" content="main"/>
</head>

<body>
<div id="container">
       <div id="header">
            <g:render template="../templates/header"/>
       </div>

      <div id="main">
        <div class="box">

          %{--<div id="pagePath">--}%
            %{--<h1><g:link controller="homePage" action="index" class="home">Home</g:link> <span>&raquo;</span> <a class="search"> Search </a> </h1>--}%
          %{--</div>--}%

         <div class="nav">
            <span class="menuButton"><g:link controller="homePage" action="index" class="home">Home</g:link></span>
            <span class="info_alert" onclick="window.open('${resource(dir:'',file:'')}/help.gsp?application=search', 'HelpDocs', 'menubar=0,resizable=0,location=1,status=0,scrollbars=1,  width=800,height=600');" target="_blank" title="info">&nbsp;&nbsp;&nbsp;</span>
         </div>

         <div class="appsHeader"> Search </div>
         <g:render template="../templates/error"/>


       <g:form name="searchFormTemplate" controller="search">
  <div class="groupbox">
  <div class="info">
    <table cellpadding="0" border="0" cellspacing="0" class="table_noborder">
      <tr>
        <td width="5%" class="label_homepage"> Search by </td>
        <td width="1%" class="label_homepage"> : </td>
        <td width="15%">
          <g:select name="cmbSearchBy" from="${getSXSearchObjectType}" optionValue="VALUE" optionKey="ID" value="${params?.cmbSearchBy}"/>
        </td>
        <td width="48%">
          <div class="message"><g:message code="search.searchby.info" /></div>
        </td>
      </tr>

      <tr>
        <td width="5%" class="label_homepage"> Value</td>
        <td width="1%" class="label_homepage"> : </td>
        <td width="15%">
          <g:textField id="searchText" name="searchText" class="full-width" value="${params?.searchText}"></g:textField>
        </td>
        <td width="48%">
          <div class="message"><g:message code="search.value.info" /> </div>
        </td>
      </tr>
    </table>
  </div>
  </div>
    <div class="buttons">
       <span class="button"><g:actionSubmit class="submit" action="search" value="${message(code: 'default.button.submit.label', default: 'Search')}" /></span>
    </div>
</g:form>


        </div>
        <!-- End div groupbox -->

        <div class="groupbox">
          <g:render template="../templates/blank"/>
        </div>  
    </div>
    <!-- End div main -->

  <div id="footer">
            <g:render template="../templates/footer"/>
  </div>
</div>


</body>
</html>