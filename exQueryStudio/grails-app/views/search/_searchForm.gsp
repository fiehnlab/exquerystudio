<g:form name="searchFormTemplate" controller="search">
  <div class="groupbox">
  <div class="info">
    <table cellpadding="0" border="0" cellspacing="0" class="table_noborder">
      <tr>
        <td width="10%" class="label_homepage"> Search by </td>
        <td width="2%" class="label_homepage"> : </td>
        <td width="40%">
          <g:select name="cmbSearchBy" from="${getSXSearchObjectType}" optionValue="VALUE" optionKey="ID" value="${params?.cmbSearchBy}" title="${message(code: 'search.searchby.info', default: 'select an Object in which you want to perform search')}"/>
        </td>
      </tr>
      <tr>
        <td width="10%" class="label_homepage"> Value</td>
        <td width="2%" class="label_homepage"> : </td>
        <td width="40%">
          <g:textField id="searchText" name="searchText" class="full-width" title="enter search data (Minimum three(3) characters required)"></g:textField>
        </td>
      </tr>

    </table>
  </div>
  </div>
    <div class="buttons">
       <span class="button"><g:actionSubmit class="submit" action="search" value="${message(code: 'default.button.submit.label', default: 'Search')}" /></span>
       <span class="button"><g:actionSubmit class="genericsearch" action="genericSearch" value="Generic Search" /></span>
    </div>
</g:form>