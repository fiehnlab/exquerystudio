<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Jul 7, 2010
  Time: 3:04:26 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="exquery.ExperimentClassService; exquery.ClassSampleService; setupx.entity.ClassMetadata; exquery.util.CommonUtil; exquery.util.FormatResultUtil; setupx.entity.Experiment; setupx.entity.ClassSample; setupx.entity.ExperimentClass" contentType="text/html;charset=UTF-8" %>

<html>
<head>
  <meta name="layout" content="main"/>
</head>

<body id="searchInfo">

<%
  ExperimentClassService classService = new ExperimentClassService()
  ClassSampleService sampleService = new ClassSampleService()
  
  Experiment experiment = result?.Experiment
  def experimentClasses = result?.ExperimentClass
  if( experimentClasses == null )
  {
     experimentClasses = experiment?.experimentclassSet
  }

  if( experimentClasses != null ){
    if(experimentClasses instanceof Collection){
      experimentClasses = experimentClasses.sort{it.id}
    }    
  }



  ExperimentClass objExperimentClass

  ClassSample classSample = result?.ClassSample
  def classSamples
  ClassSample objClassSample
%>




<div id="container">
       <div id="header">
            <g:render template="../templates/header"/>
       </div>
      <!-- Start div main -->
      <div id="main">
        <div class="box">


          <div class="nav">
            <span class="menuButton"><g:link controller="homePage" action="index" class="home">Home</g:link></span>
            <span class="menuButton"><a href="javascript://nop" class="back" onclick="history.back(1)">Back to result</a></span>
            <span class="menuButton"><g:link controller="search" action="exportexp" class="exportexp" params="[id:experiment?.id,format:'excel',extension:'xls']">Export Experiment</g:link></span>
          </div> 


            <div class="appsHeader">
                   ${result?.AppsHeader}
            </div>


        <%
            if( experiment != null )
            {
              def classesCount
              def samplesCount
              ArrayList experimentMetaData = new ArrayList()
              if(experiment.id!=null && experiment.id.toString().trim().length()>0){
                experimentMetaData.add([id:"Experiment Id",value:experiment.id])

                classesCount = classService.getClassesCountByExperimentIds(Long.parseLong("0"+experiment.id.toString()))
                samplesCount = sampleService.getSamplesCountByExperimentIds(Long.parseLong("0"+experiment.id.toString()))
              }

              String title = CommonUtil.getInstance().customEncodeAsHTML(experiment.getTitle())
              if(title!=null && title.trim().length()>0){
                experimentMetaData.add([id:"Title",value:title])
              }

              String expAbstract = CommonUtil.getInstance().customEncodeAsHTML(experiment.getExperiment_abstract())
              if(expAbstract!=null && expAbstract.trim().length()>0){
                experimentMetaData.add([id:"Abstract",value:expAbstract])
              }

              String description = CommonUtil.getInstance().customEncodeAsHTML(experiment.getDescription())
              if(description!=null && description.trim().length()>0){
                experimentMetaData.add([id:"Description",value:description])
              }

              String comments = CommonUtil.getInstance().customEncodeAsHTML(experiment.getComments())
              if(comments!=null && comments.trim().length()>0){
                experimentMetaData.add([id:"Comments",value:comments])
              }

              if(classesCount!=null){
                experimentMetaData.add([id:"Classes",value:classesCount])
              }

              if(samplesCount!=null){
                experimentMetaData.add([id:"Samples",value:samplesCount])
              }

              String owner = CommonUtil.getInstance().prepareOwnersTextFromExpRights(experiment.experimentUserRights)
              if(owner!=null && owner.trim().length()>0){
                experimentMetaData.add([id:"Collaboration / Owner",value:owner])
              }

        %>
          <div class='info_header' style="vertical-align:bottom">
            %{--<span class="info_image">--}%
              %{--<img src="${resource(dir:'images/forms',file:'SearchCriteria.ico')}" width="18px" style="padding: 2px">--}%
              %{--</span>--}%
            <span class="info_title">&nbsp;Experiment</span>
          </div>
          <div class="info" title="Experiment Id: ${experiment.id}">
            <!-- Start: Experiment table -->
              <table width="90%" cellpadding="0" cellspacing="0" class="table_noborder">

                  <g:if test="${experimentMetaData !=null && experimentMetaData.size()>0}" >
                    <g:each var="experimentMap" in="${experimentMetaData}" status="experimentDataIndex">
                      <tr>
                          <td width="10%" class="forminputlabel"> ${experimentMap.id} </td>
                          <td width="1%"> : </td>
                          <td width="80%" class="name-with-largespace-break"> ${experimentMap.value} </td>
                      </tr>
                    </g:each>
                  </g:if>
              </table>
            <!-- End: Experiment table -->
          </div>

          <div class='info_header' style="vertical-align:bottom">
            %{--<span class="info_image">--}%
              %{--<img src="${resource(dir:'images/forms',file:'SearchCriteria.ico')}" width="18px" style="padding: 2px">--}%
              %{--</span>--}%
            <span class="info_title">&nbsp;Classes & Samples</span>
          </div>

          <div class="info">
          <!-- Start: Classes table -->
              <table width="100%" cellpadding="0" cellspacing="0" class="table_noborder">
              <g:each var="expClass" in="${experimentClasses}" status="index">
               <%
                  objExperimentClass = expClass
                  classSamples = null

                  if( classSample != null )
                  {
                    classSamples = [classSample]
                  }else{
                    classSamples = objExperimentClass.sampleSet?.sort{it.id}
                  }

                  List MetaDataList = FormatResultUtil.getInstance().prepareSampleMetaDataList(classSamples);
                  
                  String lStrClass = "";

                  if (index % 2 == 0) {
                    lStrClass = "even"
                  } else {
                    lStrClass = "odd"
                  }

                  /* set the species info : Start */
                  def speciesName = CommonUtil.getInstance().prepareSpeciesText(objExperimentClass?.speciesSet)
                  /* set the species info : End */

                  /* set the organs info : Start */
                  def organsName = CommonUtil.getInstance().prepareOrgansText(objExperimentClass?.organSet)
                  def tissue = CommonUtil.getInstance().prepareOrgansTissueText(objExperimentClass?.organSet)
                  def cell_type = CommonUtil.getInstance().prepareOrgansCellTypeText(objExperimentClass?.organSet)
                  def subcellular_celltype = CommonUtil.getInstance().prepareOrgansSubcellularCelltype(objExperimentClass?.organSet)
                  /* set the organs info : End */

                  ArrayList infoList = new ArrayList()
                  if(speciesName!=null && speciesName.toString().trim().length()>0){
                    infoList.add([id:"Species Name",value:speciesName.toString().trim()])
                  }
                   

                  if(organsName!=null && organsName.toString().trim().length()>0){
                    infoList.add([id:"Organ Name",value:organsName.toString().trim()])
                  }
                  if(tissue!=null && tissue.toString().trim().length()>0){
                    infoList.add([id:"Tissue",value:tissue.toString().trim()])
                  }
                  if(cell_type!=null && cell_type.toString().trim().length()>0){
                    infoList.add([id:"Cell Type",value:cell_type.toString().trim()])
                  }
                  if(subcellular_celltype!=null && subcellular_celltype.toString().trim().length()>0){
                    infoList.add([id:"Subcellular Celltype",value:subcellular_celltype.toString().trim()])
                  }
                  
                  if(objExperimentClass.getGenotype()!=null && objExperimentClass.getGenotype().trim().length()>0){
                    infoList.add([id:"Genotype",value:objExperimentClass.getGenotype()])
                  }
                  if(objExperimentClass.getGreen_house()!=null && objExperimentClass.getGreen_house().trim().length()>0){
                    infoList.add([id:"Green House",value:objExperimentClass.getGreen_house()])
                  }
                  if(objExperimentClass.getExtraction()!=null && objExperimentClass.getExtraction().trim().length()>0){
                    infoList.add([id:"Extraction",value:objExperimentClass.getExtraction()])
                  }
                  if(objExperimentClass.getMethod_name()!=null && objExperimentClass.getMethod_name().trim().length()>0){
                    infoList.add([id:"Method Name",value:objExperimentClass.getMethod_name()])
                  }

                  if(objExperimentClass.classMetadataSet !=null && objExperimentClass.classMetadataSet.size()>0){
                    def classMetadataSet =  objExperimentClass.classMetadataSet
                    classMetadataSet = classMetadataSet.sort{it.fieldTitle}

                    classMetadataSet.each {ClassMetadata classMetadata->
                      infoList.add([id:classMetadata.fieldTitle,value:classMetadata.fieldValue])
                    }
                  }

                  ArrayList largeDataList = new ArrayList()
                  String detailedLocation = CommonUtil.getInstance().customEncodeAsHTML(objExperimentClass.getDetailed_location())
                  if(detailedLocation!=null && detailedLocation.trim().length()>0){
                    largeDataList.add([id:"Detailed Location",value:detailedLocation])
                  }

                  String preparation = CommonUtil.getInstance().customEncodeAsHTML(objExperimentClass.getPreparation())
                  if(preparation!=null && preparation.trim().length()>0){
                    largeDataList.add([id:"Preparation",value:preparation])
                  }

                  String information = CommonUtil.getInstance().customEncodeAsHTML(objExperimentClass.getInformation())
                  if(information!=null && information.trim().length()>0){
                    largeDataList.add([id:"Information",value:information])
                  }

              %>

              <tr class='${lStrClass}' onMouseOver="this.className='highlight'" onMouseOut="this.className='${lStrClass}'" title="Experiment Id: ${experiment.id}; &nbsp;Class Id:&nbsp; ${objExperimentClass.id}">
                <td>
             <!-- Start: Class table -->
              <table align="center"  cellpadding="0" cellspacing="0">
                <tr>
                  <th width="1%">&nbsp;</th>
                  <th width="98%" colspan="12" ><div align="left">[${index+1}] &nbsp;Class Id:&nbsp; ${objExperimentClass.id}</div> </th>
                  <th width="1%">&nbsp;</th>
                </tr>

                 <!-- Start: Info Block -->
                 <g:if test="${infoList !=null && infoList.size()>0}" >
                    

                    <g:each var="map" in="${infoList}" status="infoIndex">

                       <g:if test="${ infoIndex == 0}">
                          <tr>
                            <td width="1%">&nbsp;</td>
                        </g:if>
                        
                        <g:if test="${ ((infoIndex+1)%3) == 0}">
                                <td width="10%" class="forminputlabel"> ${map.id} </td>
                                <td width="1%"> : </td>
                                <td width="23%">  ${map.value} </td>
                                <td width="1%">&nbsp;</td>
                            </tr>
                           <g:if test="${ infoIndex < infoList.size()-1}"> 
                            <tr>
                                <td width="1%">&nbsp;</td>
                           </g:if>
                        </g:if>
                        <g:else>
                            <td width="10%" class="forminputlabel"> ${map.id} </td>
                            <td width="1%"> : </td>
                            <td width="23%">  ${map.value} </td>
                            <td width="1%">&nbsp;</td>
                        </g:else>
                    </g:each>
                  </g:if>
                  <!-- End: Info Block -->

                  <!-- Start: Large Data Block -->
                 <g:if test="${largeDataList !=null && largeDataList.size()>0}" >


                    <g:each var="largeDatamap" in="${largeDataList}" status="largeDataIndex">
                      <tr >
                        <td width="1%">&nbsp;</td>
                        <td width="10%" class="forminputlabel"> ${largeDatamap.id} </td>
                        <td width="1%"> : </td>
                        <td colspan="10">  ${largeDatamap.value} </td>
                        <td width="1%">&nbsp;</td>
                      </tr>
                    </g:each>

                  </g:if>
                  <!-- End: Large Data Block -->



              <tr>
                <td width="1%">&nbsp;</td>
                <td colspan="12">
              <!-- Start: Sample table -->
                  <table width="98%" align="center" class="result">

                    <tr>
                      <th class="srno-no-break" style="width:5%">Sr. No.</th>
                      <th class="text-no-break" style="width:10%">Sample Id</th>
                      <th class="text-with-break" style="width:25%">chromatof Ids</th>
                      <th class="text-with-break" style="width:15%">Label</th>
                      <th class="text-with-largespace-break" style="width:25%">Comments</th>
                      %{--<th class="text-with-largespace-break">Event</th>--}%
                      <th class="text-with-largespace-break" style="width:20%">Variations</th>
                    </tr>

               <g:if test="${classSamples !=null && classSamples.size()>0}" >   

                   <g:each var="classSample1" in="${classSamples}" status="sampleIndex">
                   <%
                      objClassSample = classSample1

                      if (sampleIndex % 2 == 0) {
                        lStrClass = "even"
                      } else {
                        lStrClass = "odd"
                      }
                      String chromatofIds = chromatofIds = CommonUtil.getInstance().prepareChromatofsString(objClassSample?.sampleChromatofSet)


//                      def variations = objClassSample?.sampleMetadataSet
//                      String variationsString = ""
//                      variations?.each {metaData->
//                        if(variationsString.length()>0 ){
//                          variationsString += "; <b>${metaData.fieldTitle}</b>=${metaData.fieldValue}"
//                        } else{
//                           variationsString = "<b>${metaData.fieldTitle}</b>=${metaData.fieldValue}"
//                        }
//                      }
                     def variations = objClassSample?.sampleMetadataSet  
                     def metaData
                     String variationsString = ""
                     MetaDataList.each { logicalId->
                       metaData = FormatResultUtil.getInstance().getMetaDataObjectFromSet(logicalId,variations)
                       if(metaData!=null){
                        if(variationsString.length()>0 ){
                            variationsString += "<br><span class=\"forminputlabel\">${metaData.fieldTitle}</span>=${metaData.fieldValue != null ? metaData.fieldValue : ''}"
                        } else{
                            variationsString = "<span class=\"forminputlabel\">${metaData.fieldTitle}</span>=${metaData.fieldValue != null ? metaData.fieldValue : ''}"
                        }
                       }

                     } 

                  %>
                   <tr class='<%=lStrClass%>'  onMouseOver="this.className='highlight'" onMouseOut="this.className='${lStrClass}'"   title="Experiment Id: ${experiment.id}; &nbsp;Class Id: ${objExperimentClass.id};&nbsp; Sample Id:${objClassSample.id}" >
                     <td class="srno-no-break" style="width:5%">${sampleIndex+1}</td>
                     <td class="text-no-break" style="width:10%">${objClassSample.id}</td>
                     <td class="text-with-break" style="width:25%">${chromatofIds}</td>
                     <td class="text-with-break" style="width:15%">${objClassSample.getLabel()}</td>
                     <td class="text-with-largespace-break" style="width:25%">${objClassSample.getComments()}</td>
                     %{--<td class="text-with-largespace-break">${objClassSample.getEvent()}</td>--}%
                     <td class="text-with-largespace-break" style="width:20%">${variationsString}</td>
                   </tr>
                  </g:each>
              </g:if>
              <g:else>
                    <tr>
                      <td colspan="7" class='even'>
                        <div class="errors"> No Sample </div>
                      </td>

                    </tr>
              </g:else>

                </table>
             <!-- End: Sample table -->

                </td>
                <td width="1">&nbsp;</td>
              </tr>

              <tr><td colspan="14">&nbsp;</td></tr>
             </table>
           <!-- End: Class table -->

            </td>
          </tr>
          <tr><td>&nbsp;</td></tr>
        </g:each>
      </table>
    <!-- End: Classes table -->
   </div>

  </div>

    <% }else{ %>

      <div class='info_header' style="vertical-align:bottom">
            <span class="info_image">
              <img src="${resource(dir:'images/forms',file:'SearchCriteria.ico')}" width="18px" style="padding: 2px">
              </span>
            <span class="info_title">Result</span>
          </div>
          <div class="info">
            <div class="errors">

              ${result?.ErrorMsg}

            </div>
          </div>
    <% } %>


     <div class="groupbox">
       <g:render template="../templates/blank"/>
     </div>


    </div>
    <!-- End div main -->
    <div id="footer">
       <g:render template="../templates/footer"/>
    </div>
</div>
</body>

</html>