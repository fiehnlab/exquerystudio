<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Jul 1, 2010
  Time: 12:28:13 PM
  Search Result Page
--%>

<%@ page import="setupx.entity.Experiment" contentType="text/html;charset=UTF-8" %>


<html>
<head>
  <meta name="layout" content="main"/>
  <export:resource />
</head>

<body id="search">
<div id="container">
       <div id="header">
            <g:render template="../templates/header"/>
       </div>
      <!-- Start div main -->
      <div id="main">
        <div class="box">

          %{--<div id="pagePath">--}%
            %{--<h1><g:link controller="homePage" action="index" class="home">Home</g:link> <span>&raquo;</span> <g:link controller="search" params="[cmbSearchBy: params.cmbSearchBy,searchText:params.searchText]" action="index" class="search">Search</g:link> <span>&raquo;</span> <a class="searchresult"> Search Result </a> </h1>--}%
          %{--</div>--}%

          <div class="nav">
            <span class="menuButton"><g:link controller="homePage" action="index" class="home" >Home</g:link></span>
            <span class="menuButton"><g:link controller="search" action="index" class="search" params="[cmbSearchBy: params.cmbSearchBy,searchText:params.searchText]">Search</g:link></span>
          </div>
          
          <div class="appsHeader"> Search Result </div>




          <div class='info_header' style="vertical-align:bottom">
            %{--<span class="info_image">--}%
              %{--<img src="${resource(dir:'images/forms',file:'SearchCriteria.ico')}" width="18px" style="padding: 2px">--}%
              %{--</span>--}%
            <span class="info_title">&nbsp;Search Criteria</span>
          </div>
          <div class="info">
              <table width="100%" cellpadding="0" cellspacing="0" class="table_noborder">
                  <tr>
                    <td width="5%" class="forminputlabel"> search by </td>
                    <td width="1%"> : </td>
                    <td width="80%"> <g:message code="${params.cmbSearchBy}" default="${params.cmbSearchBy}" /> </td>
                  </tr>

                  <tr>
                    <td width="5%" class="forminputlabel"> search key</td>
                    <td width="1%"> : </td>
                    <td width="80%">   ${params.searchText} </td>
                  </tr>
              </table>
          </div>

      <div class='info_header' style="vertical-align:bottom">
        %{--<span class="info_image">--}%
          %{--<img src="${resource(dir:'images/forms',file:'ResultList.ico')}" width="18px" style="padding: 2px">--}%
          %{--</span>--}%
        <span class="info_title">&nbsp;Result List</span>
      </div>
      <div class="info">

        <!-- Start : Pagination Block -->
      %{--<%--}%
        %{--if( result?.isPaginate != null && result?.isPaginate ){--}%
          %{--if(result.resultList != null && result.resultList?.size() > 0){--}%
      %{--%>--}%
      %{--<div class="pagination_top">--}%
        %{--<g:customPaginate controller="search" action="search" params="[cmbSearchBy: params.cmbSearchBy,searchText:params.searchText]" total="${result?.TotalRecordCount}" prev="&lt; previous" next="next &gt;"/>--}%
      %{--</div>--}%
      %{--<% }--}%
       %{--} else{--}%
      %{--if( result?.TotalRecordCount != null ){--}%
      %{--%>--}%
      %{--<div class="pagination_top">--}%
        %{--<g:customPaginate isPaginate="false" total="${result?.TotalRecordCount}" />--}%
      %{--</div>--}%
      %{--<% } } %>--}%
        <!-- End : Pagination Block -->


      <table class="result">

        <%
          if (result.fieldList != null && result.fieldList.size() > 0) {
        %>
       <tr>
        <g:each var="headerField" in="${result.fieldList}">
        <%
              def cssClass = result.cssClassMap.get(headerField)
              def header = result.labelMap.get(headerField)
        %>
          <th class="${cssClass}">${header}</th>

          %{--<g:sortableColumn property="${headerField}" title="${header}" params="[cmbSearchBy: params.cmbSearchBy,searchText:params.searchText]" />--}%

        </g:each>
        </tr>

        <%
          }  %>

        <%
          if (result.resultList != null && result.resultList.size() > 0) {
            long rowNo=1;
        %>
           <g:each var="row" in="${result.resultList}" status="index">
           <%
              String lStrClass = "";

              if (index % 2 == 0) {
                lStrClass = "even"
              } else {
                lStrClass = "odd"
              }
          %>

          <tr class='<%=lStrClass%>' onMouseOver="this.className='highlight'" onMouseOut="this.className='<%=lStrClass%>'" >

            <g:each var="columnfield" in="${result.fieldList}">
            <%
                  cssClass = result.cssClassMap.get(columnfield)
                  def data = row.get(columnfield)
            %>
            <td class="${cssClass}">${data!=null && data.toString().trim().length()>0?data:"&nbsp;"}</td>
            </g:each>
          </tr>
        </g:each>

        <%
          } else {
        %>
        <tr>
          <td colspan="${result.fieldList?.size}" class='even'>
            <div class="errors">

              ${result?.ErrorMsg}

            </div>
          </td>

        </tr>
        <% }
        %>
      </table>


      <%
       if (result.resultList != null && result.resultList.size() > 0) {
            if( result?.isPaginate != null && result?.isPaginate ){
                if(result.resultList != null && result.resultList.size() > 0){
      %>
                  <div class="pagination_bottom">
                    <g:customPaginate  controller="search" action="search" params="[cmbSearchBy: params.cmbSearchBy,searchText:params.searchText]" total="${result?.TotalRecordCount}" prev="&lt; previous" next="next &gt;"/>
                  </div>

                  <export:formats formats="['csv', 'excel', 'pdf', 'rtf']" params="[cmbSearchBy: params.cmbSearchBy,searchText:params.searchText]" />

      <%        }
            } else {
                if( result?.TotalRecordCount != null ){
     %>
                   <div class="pagination_top">
                    <g:customPaginate isPaginate="false" total="${result?.TotalRecordCount}" />
                   </div>

                   <g:if test="${result?.isExport != null && result?.isExport}" >
                     <export:formats formats="['csv', 'excel', 'pdf', 'rtf']" params="[cmbSearchBy: params.cmbSearchBy,searchText:params.searchText]" />
                   </g:if>
     <%          }
             }
        }
     %>

      </div>
      <!-- End div info : result -->
    </div>
    <!-- End div box -->
     <div class="groupbox">
       <g:render template="../templates/blank"/>
     </div>


    </div>
    <!-- End div main -->
    <div id="footer">
       <g:render template="../templates/footer"/>
    </div>
</div>
</body>

</html>