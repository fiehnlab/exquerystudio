<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Aug 26, 2010
  Time: 5:23:29 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
  <meta name="layout" content="main"/>
  <export:resource />
</head>

<body id="search">
<div id="container">
       <div id="header">
            <g:render template="../templates/header"/>
       </div>
      <!-- Start div main -->
      <div id="main">
        <div class="box">

          %{--<div id="pagePath">--}%
            %{--<h1><g:link controller="homePage" action="index" class="home">Home</g:link> <span>&raquo;</span> <g:link controller="search" params="[cmbSearchBy: params.cmbSearchBy,searchText:params.searchText]" action="index" class="search">Search</g:link> <span>&raquo;</span> <a class="searchresult"> Search Result </a> </h1>--}%
          %{--</div>--}%

          <div class="nav">
            <span class="menuButton"><g:link controller="homePage" action="index" class="home">Home</g:link></span>
            <span class="menuButton"><a href="javascript://nop" class="search" onclick="history.back(1)">Generic Search</a></span>
          </div>

          <div class="appsHeader"> Generic Search Result </div>

          <div class='info_header' style="vertical-align:bottom">
            %{--<span class="info_image">--}%
              %{--<img src="${resource(dir:'images/forms',file:'SearchCriteria.ico')}" width="18px" style="padding: 2px">--}%
              %{--</span>--}%
            <span class="info_title">&nbsp;Search Criteria</span>
          </div>
          <div class="info">
              <table width="100%" cellpadding="0" cellspacing="0" class="table_noborder">
                  <tr>
                    <td width="1%"> </td>
                    <td width="95%" align="left" valign="top"> ${whereClause} </td>
                    <td width="1%"></td>
                  </tr>

                  %{--<tr>--}%
                    %{--<td width="5%"> search key</td>--}%
                    %{--<td width="1%"> : </td>--}%
                    %{--<td width="80%">   ${params.searchText} </td>--}%
                  %{--</tr>--}%
              </table>
          </div>

      <div class='info_header' style="vertical-align:bottom">
        %{--<span class="info_image">--}%
          %{--<img src="${resource(dir:'images/forms',file:'ResultList.ico')}" width="18px" style="padding: 2px">--}%
          %{--</span>--}%
        <span class="info_title">&nbsp;Result List</span>
      </div>
      <div class="info">

      <table class="result">

        <%

          if (result.fieldList != null && result.fieldList.size() > 0) {
          def orderByMap = orderByMap
          System.out.println("orderByMap :: "+orderByMap)
        %>
       <tr>
        <g:each var="headerField" in="${result.fieldList}">
        <%

              def cssClass = result.cssClassMap?.get(headerField)
              def header = result.labelMap?.get(headerField)
              def orderType = orderByMap?.getAt(headerField)
        %>
          <th class="${cssClass}">${header}

            <g:if test="${ (orderType != null && orderType.equals('asc')) }">
                <img src="${resource(dir:'images/skin',file:'sort_ascending.png')}" border="0"/>
            </g:if>
            <g:elseif test="${ (orderType != null && orderType.equals('desc')) }">
                <img src="${resource(dir:'images/skin',file:'sort_descending.png')}" border="0"/>
            </g:elseif>

          </th>

          %{--<g:sortableColumn property="${headerField}" title="${header}" params="[cmbSearchBy: params.cmbSearchBy,searchText:params.searchText]" />--}%

        </g:each>
        <th class="text-with-break">Export Experiment</th>
        </tr>

        <%
          }  %>

        <%
          if (result.resultList != null && result.resultList.size() > 0) {
            long rowNo=1;
            //System.out.println("result.resultList :: "+result.resultList)
        %>
           <g:each var="row" in="${result.resultList}" status="index">
           <%
              String lStrClass = "";

              if (index % 2 == 0) {
                lStrClass = "even"
              } else {
                lStrClass = "odd"
              }
              def experimentId
          %>
  
          <tr class='<%=lStrClass%>' onMouseOver="this.className='highlight'" onMouseOut="this.className='<%=lStrClass%>'"  >

            <g:each var="columnfield" status="j" in="${result.fieldList}">
            <%
                  cssClass = result.cssClassMap.get(columnfield)
                  def data = row.get(columnfield)
                  if(j==1){
                      experimentId = data
                  }

            %>

              <g:if test="${j == 1}">     %{--provide a link to view the Experiment Info--}%
                <td class="${cssClass}">
                  <g:link controller="search" action="showInfo" params="[infoBy:'ExperimentId',id:data]" title="Click to view" >${data}</g:link>
                </td>
              </g:if>
              <g:elseif test="${j != 1}">
                <td class="${cssClass}">${exquery.util.CommonUtil.getInstance().customEncodeAsHTML(data)}</td>
              </g:elseif>
              
            </g:each>
            <td class="text-with-break">
                  <g:link class="exportexp" controller="search" action="exportexp" params="[format:'excel',extension:'xls',id:experimentId]" title="Click to export experiment" >&nbsp;</g:link>
            </td>
          </tr>
        </g:each>
       </table>

        <div class="pagination_top">
         <g:customPaginate isPaginate="false" total="${result.resultList.size()}" />
        </div>

        <export:formats formats="['csv', 'excel','pdf', 'rtf']" params="[isExport: true]"/>

        <%
          } else {
        %>
        <tr>
          <td colspan="${result.fieldList?.size}" class='even'>
            <div class="errors">

              ${result?.ErrorMsg}

            </div>
          </td>

        </tr>
        </table>
        <% }
        %>


 </div>
      <!-- End div info : result -->
          
    </div>
    <!-- End div box -->
     <div class="groupbox">
       <g:render template="../templates/blank"/>
     </div>


    </div>
    <!-- End div main -->
    <div id="footer">
       <g:render template="../templates/footer"/>
    </div>
</div>
</body>

</html>