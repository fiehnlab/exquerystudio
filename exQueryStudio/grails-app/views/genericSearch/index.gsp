<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Aug 25, 2010
  Time: 4:10:53 PM
  GUI Page for Generic Search
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head><meta name="layout" content="main"/>

<script type="text/javascript">

/* to add a row into where block */    
function addRowToTable()
{
  var tbl = document.getElementById('tabWhere');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;

  if( parseInt(iteration) > 15)
	  return;

  document.getElementsByName('countWhereClause')[0].value = parseInt(document.getElementsByName('countWhereClause')[0].value)+1

  var row = tbl.insertRow(lastRow);

  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  var cell5 = row.insertCell(4);

  cell1.innerHTML = "<center>"+iteration+"</center>";
  cell2.innerHTML = "<center><select name='cmbOption"+iteration+"' > <option value='AND'> AND </option> <option value='OR'> OR </option>  </select></center>";
  cell3.innerHTML = "<center><select name='cmbCriteria"+iteration+"' style='width:200px'> ${SearchCriteriaString}  </select></center>";
  cell4.innerHTML = "<center><select name='cmbOperation"+iteration+"' > <option value='-1'> ---Select--- </option> <option value='='> Equals </option> <option value='<>'> Not Equals </option> <option value='like'> like </option> </select></center>";
  cell5.innerHTML = "<center><input name='txtSearchData"+iteration+"' value='' style='width:400px;'/></center>";


   if( parseInt(document.getElementsByName('countWhereClause')[0].value) > 1 ){
     document.getElementsByName('RemoveCriteria')[0].disabled = false;
   }else{
     document.getElementsByName('RemoveCriteria')[0].disabled = true;
   }

   if( parseInt(document.getElementsByName('countWhereClause')[0].value) < 15 ){
     document.getElementsByName('AddCriteria')[0].disabled = false;
   }else{
     document.getElementsByName('AddCriteria')[0].disabled = true;
   }

}

/* to remove last row from where block */
function removeRowFromTable()
{
  var tbl = document.getElementById('tabWhere');
  var lastRow = tbl.rows.length;
  if (lastRow > 2) {
    tbl.deleteRow(lastRow-1);
    document.getElementsByName('countWhereClause')[0].value = parseInt(document.getElementsByName('countWhereClause')[0].value)-1
  }

  if( parseInt(document.getElementsByName('countWhereClause')[0].value) > 1 ){
     document.getElementsByName('RemoveCriteria')[0].disabled = false;
  }else{
      document.getElementsByName('RemoveCriteria')[0].disabled = true;
  }

  if( parseInt(document.getElementsByName('countWhereClause')[0].value) < 15 ){
     document.getElementsByName('AddCriteria')[0].disabled = false;
   }else{
     document.getElementsByName('AddCriteria')[0].disabled = true;
   }
}

/* to add a row into orderby block */
function addRowToOrderByTable()
{
  var tbl = document.getElementById('tabOrderBy');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;

  if( parseInt(iteration) > 15)
	  return;

  document.getElementsByName('countOrderByClause')[0].value = parseInt(document.getElementsByName('countOrderByClause')[0].value) +1

  var row = tbl.insertRow(lastRow);

  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);

  cell1.innerHTML = "<center>"+iteration+"</center>";
  cell2.innerHTML = "<center><select name='cmbOrderByOption"+iteration+"' style='width:200px'> ${OrderMapString}  </select></center>";
  cell3.innerHTML = "<center><select name='cmbType"+iteration+"' > <option value='asc'> Ascending </option> <option value='desc'> Descending </option> </select></center>";

  if( parseInt(document.getElementsByName('countOrderByClause')[0].value) > 1 ){
     document.getElementsByName('RemoveOrder')[0].disabled = false;
   }else{
     document.getElementsByName('RemoveOrder')[0].disabled = true;
   }

  if( parseInt(document.getElementsByName('countOrderByClause')[0].value) < 15 ){
     document.getElementsByName('AddOrder')[0].disabled = false;
   }else{
     document.getElementsByName('AddOrder')[0].disabled = true;
   }
}

/* to remove last row from orderby block */
function removeRowFromOrderByTable()
{
  var tbl = document.getElementById('tabOrderBy');
  var lastRow = tbl.rows.length;
  if (lastRow > 2){
    tbl.deleteRow(lastRow-1);

    document.getElementsByName('countOrderByClause')[0].value = parseInt(document.getElementsByName('countOrderByClause')[0].value)-1
  }

  if( parseInt(document.getElementsByName('countOrderByClause')[0].value) > 1 ){
     document.getElementsByName('RemoveOrder')[0].disabled = false;
   }else{
     document.getElementsByName('RemoveOrder')[0].disabled = true;
   }

  if( parseInt(document.getElementsByName('countOrderByClause')[0].value) < 15 ){
     document.getElementsByName('AddOrder')[0].disabled = false;
   }else{
     document.getElementsByName('AddOrder')[0].disabled = true;
   }
}

/* Set the data into where Block row */
function setWhereBlock(i,cmbOption,cmbCriteria,cmbOperation,txtSearchData)
{
  if(document.getElementsByName('cmbOption'+i)[0]){
    document.getElementsByName('cmbOption'+i)[0].value = cmbOption
  }

  if(document.getElementsByName('cmbCriteria'+i)[0].value){
    document.getElementsByName('cmbCriteria'+i)[0].value = cmbCriteria
  }

  if(document.getElementsByName('cmbOperation'+i)[0]){
    document.getElementsByName('cmbOperation'+i)[0].value = cmbOperation
  }

  if(document.getElementsByName('txtSearchData'+i)[0]){
    document.getElementsByName('txtSearchData'+i)[0].value = txtSearchData
  }
}

/* Set the data into OrderBy Block row */
function setOrderByBlock(i,cmbOrderByOption,cmbType)
{
  if(document.getElementsByName('cmbOrderByOption'+i)[0]){
    document.getElementsByName('cmbOrderByOption'+i)[0].value = cmbOrderByOption
  }

  if(document.getElementsByName('cmbType'+i)[0].value){
    document.getElementsByName('cmbType'+i)[0].value = cmbType
  }
}



</script>

</head>

<body >
<div id="container">
      <div id="header">
            <g:render template="../templates/header"/>
      </div>

      <div id="main">

        <div class="box">

         <div class="nav">
            <span class="menuButton"><g:link controller="homePage" action="index" class="home">Home</g:link></span>
            <span class="info_alert" onclick="window.open('${resource(dir:'',file:'')}/help.gsp?application=genericsearch', 'HelpDocs', 'menubar=0,resizable=0,location=1,status=0,scrollbars=1,  width=800,height=600');" target="_blank" title="info">&nbsp;&nbsp;&nbsp;</span>
         </div>

          <div class="appsHeader"> Generic Search </div>

        <div class="groupbox">

        <g:form action="process"  method="post" >
          <table id='tabMain' border='0' width="60%" align='center' cellpadding="0" cellspacing="0" class="table_noborder">
            <tr>

              %{--Start: select block--}%

              <td colspan="3">

                <span>
                  <g:render template="../templates/error"/>
                </span>

                <div class='info_header' style="vertical-align:bottom">
                    <span class="info_title">&nbsp;Select properties to view into the result</span>
                </div>

                 <div class="info" >
                   <table id='tabSelect' border='0' cellpadding="0" cellspacing="0" class="table_noborder">

                      <%
                          String checked = "false"
                          ArrayList selectArrayList = new ArrayList()

                          System.out.println("paramSelectList.classs :: "+paramSelectList.getClass())

                          if(paramSelectList!=null){

                            if(paramSelectList instanceof java.lang.String){
                              selectArrayList.add(paramSelectList.toString())
                            }else{
                              paramSelectList.each {s->
                                 selectArrayList.add(s)
                              }
                            }

                          }
                      %>
                      <g:each in="${SelectList}" status="j" var="selectObj">

                       <%
                         if(selectArrayList.contains(selectObj?.object)){
                            checked = "true" 
                         }else{
                            checked = "false"
                         }
                       %>
                         <g:if test="${(j%5) == 0}">
                          <g:if test="${j == 0}">
                            <tr>
                          </g:if>
                          <g:elseif test="${j!= 0}">
                            </tr>
                            <tr>
                          </g:elseif>
                          <td>
                              <g:checkBox name="Select" value="${selectObj?.object}" checked="${checked}"  /> ${selectObj?.title} &nbsp;
                          </td>
                        </g:if>
                        <g:elseif test="${(j%5) != 0}">
                          <td>
                              <g:checkBox name="Select" value="${selectObj?.object}" checked="${checked}"  /> ${selectObj?.title} &nbsp;
                          </td>
                        </g:elseif>
                      </g:each>
                      </tr>
                   </table>
                 </div>
              </td>
            </tr>

            %{--End: select block--}%

            <tr>
              <td height="5px"></td>
            </tr>

            %{--Start: where block--}%
            <tr>
              <td>

                <div class='info_header' style="vertical-align:bottom">
                    <span class="info_title">&nbsp;Prepare your own Criteria</span>
                </div>
                <div class="info">

                  <table id='tabWhereMain' border='0' width="60%" align='center' cellpadding="0" cellspacing="0" class="table_noborder">

                      <tr align='right' valign='bottom' height="20px" valign='middle'>
                          <td width="10%" colspan="5" align="right" valign='middle'>
                              <input type="button" class="EditableField" name="AddCriteria" value="Add Criteria" onclick="addRowToTable();" />
                              <input type="button" class="EditableField" name="RemoveCriteria" value="Remove Criteria" onclick="removeRowFromTable();" disabled/>
                          </td>

                      </tr>
                      <tr align='left' valign='bottom'>
                        <td align="center" >

                          <table border="0" id="tabWhere" width="100%" align='center' cellpadding="2" cellspacing="0" >

                             <tr align='left' valign='bottom' class="TableHeaderBG">
                                <th width="5%"> S.No. </th>
                                <th width="5%"> Options </th>
                                <th width="30%"> Criteria </th>
                                <th width="20%"> Operation </th>
                                <th width="40%"> Search Content </th>
                            </tr>

                            <tr>
                                <td><center>1</center></td>
                                <td > &nbsp; </td>
                                <td ><center> <select name='cmbCriteria1' style="width:200px" > ${SearchCriteriaString} </select> </center></td>
                                <td ><center> <select name='cmbOperation1' > <option value='-1'> --Select-- </option> <option value='='> Equals </option> <option value='<>'> Not Equals </option> <option value='like'> like </option> </select> </center> </td>
                                <td ><center> <input name="txtSearchData1" value="" class="Label3" style="width:400px"/> </center> </td>
                            </tr>

                            <%
                            System.out.println countWhereClause
                            int licountWhereClause = 1
                            int licountOrderByClause = 1
                            if( countWhereClause != null ){

                                def cmbOption = params.cmbOption1
                                def cmbCriteria = params.cmbCriteria1
                                def cmbOperation = params.cmbOperation1
                                def txtSearchData = params.txtSearchData1
                            %>
                              <script>
                                  setWhereBlock('1','${cmbOption}','${cmbCriteria}','${cmbOperation}','${txtSearchData}');
                              </script>
                            <%
                               licountWhereClause = Integer.parseInt("0"+countWhereClause?.toString())
                               for(int i=1; i<licountWhereClause ; i++){
                                  cmbOption = params."cmbOption${i+1}"
                                  cmbCriteria = params."cmbCriteria${i+1}"
                                  cmbOperation = params."cmbOperation${i+1}"
                                  txtSearchData = params."txtSearchData${i+1}"
                            %>
                              <tr>
                                <td><center>${i+1}</center></td>
                                <td > <center><select name='cmbOption${i+1}' > <option value='AND'> AND </option> <option value='OR'> OR </option>  </select></center> </td>
                                <td ><center> <select name='cmbCriteria${i+1}' style="width:200px" > ${SearchCriteriaString} </select> </center></td>
                                <td ><center> <select name='cmbOperation${i+1}' > <option value='-1'> --Select-- </option> <option value='='> Equals </option> <option value='<>'> Not Equals </option> <option value='like'> like </option> </select> </center> </td>
                                <td ><center> <input name="txtSearchData${i+1}" value="" class="Label3" style="width:400px"/> </center> </td>
                              </tr>
                              <script>
                                  setWhereBlock('${i+1}','${cmbOption}','${cmbCriteria}','${cmbOperation}','${txtSearchData}');
                              </script>
                            <% }
                            } %>
                          </table>
                        </td>
                      </tr>

                  </table>
                </div>
              </td>

              %{--End: where block--}%

              <td>&nbsp;</td>

              %{--Start: orderby block--}%
              <td>

                <div class='info_header' style="vertical-align:bottom">
                    <span class="info_title">&nbsp;Order by</span>
                </div>
                <div class="info">
                  <table id='tabOrderByMain' border='0' width="60%" align='center' cellpadding="0" cellspacing="0" class="table_noborder">

                    <tr align='right' valign='bottom' height="20px" valign='middle'>
                      <td width="10%" colspan="5" align="right" valign='middle'>
                          <input type="button" class="EditableField" name="AddOrder" value="Add Order" onclick="addRowToOrderByTable();" />
                          <input type="button" class="EditableField" name="RemoveOrder" value="Remove Order" onclick="removeRowFromOrderByTable();" disabled />
                      </td>
                    </tr>
                    <tr align='left' valign='bottom'>
                      <td align="center" >

                        <table border="0" id="tabOrderBy" width="100%" align='center' cellpadding="2" cellspacing="0" >
                          <tr align='left' valign='bottom' class="TableHeaderBG">
                              <th width="5%"> S.No. </th>
                              <th width="5%"> Options </th>
                              <th width="30%"> Order Type </th>
                          </tr>

                          <tr>
                              <td><center>1</center></td>
                              <td ><center> <select name='cmbOrderByOption1' style="width:200px" > ${OrderMapString}  </select> </center></td>
                              <td ><center> <select name='cmbType1' > <option value="asc"> Ascending </option> <option value="desc"> Descending </option> </select></center>  </td>
                          </tr>

                          <%
                            if( countOrderByClause != null ){

                                def cmbOrderByOption = params.cmbOrderByOption1
                                def cmbType = params.cmbType1
                          %>
                          
                              <script>
                                  setOrderByBlock('1','${cmbOrderByOption}','${cmbType}');
                              </script>
                            <%
                               licountOrderByClause = Integer.parseInt("0"+countOrderByClause?.toString())
                               for(int i=1; i<licountOrderByClause ; i++){
                                  cmbOrderByOption = params."cmbOrderByOption${i+1}"
                                  cmbType = params."cmbType${i+1}"                                  
                            %>
                              <tr>
                                <td><center>${i+1}</center></td>
                                <td ><center> <select name='cmbOrderByOption${i+1}' style="width:200px" > ${OrderMapString}  </select> </center></td>
                                <td ><center> <select name='cmbType${i+1}' > <option value="asc"> Ascending </option> <option value="desc"> Descending </option> </select></center>  </td>                                                          
                              </tr>
                              <script>
                                  setOrderByBlock('${i+1}','${cmbOrderByOption}','${cmbType}');
                              </script>
                            <% }
                            } %>
                        </table>
                      </td>
                    </tr>

                  </table>
                </div>
              </td>
             %{--End: orderby block--}%
            </tr>
          </table>
        <br>

         <input type="hidden" name="countWhereClause" value="${licountWhereClause}">
         <input type="hidden" name="countOrderByClause" value="${licountOrderByClause}">


        <p align="center">
          <g:submitButton name="submit" class="buttonSubmit" value="Submit"  />
        </p>
      </g:form>

      </div>
      <!-- End div groupbox -->

      <div class="groupbox">
         <g:render template="../templates/blank"/>
      </div>
     </div>
    </div>
    <!-- End div main -->

    <div id="footer">
         <g:render template="../templates/footer"/>
    </div>
</div>

</body>
</html>