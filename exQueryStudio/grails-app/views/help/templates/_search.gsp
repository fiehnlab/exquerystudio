<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: 2/26/11
  Time: 12:01 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
%{--About Application : Start--}%
<div>
    <h1>Application Info</h1>
</div>

<table>
    <tr>
        <td class="info_title">
            Application Name
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           Search
        </td>
    </tr>
    <tr>
        <td class="info_title">
            Location
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           Home Page
        </td>
    </tr>
    <tr>
        <td class="info_title">
            Version
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           1.0
        </td>
    </tr>
    <tr>
        <td class="info_title">
            Description
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           To search setupx objects( Experiments, classes, samples, organs and species) by there id and text
        </td>
    </tr>
</table>
%{--About Application : End --}%

%{--Application Form(s) : Start--}%
<div>
    <h1>Application Form(s)</h1>
</div>

<div class="help_screen_div">
   <img class="help_image" src="${resource(dir:'/help/images/search', file:'search_form.png')}"/>
</div>


%{--Application Form(s) : End--}%

%{--Fields Description : Start--}%
<div>
    <h1>Fields Description</h1>
</div>
<table>
    <tr>
        <th>
           Sr.No.
        </th>
        <th>
           Action
        </th>
        <th>
           Description/Validation
        </th>
        <th>
           Remarks/Examples
        </th>
    </tr>
    <tr>
      <td>
         1.
      </td>
      <td>
         Select One option from "Search by".
      </td>
      <td>
         It is a mandatory field.
      </td>
      <td>

      </td>
    </tr>
    <tr>
      <td>
         2.
      </td>
      <td>
         Enter text in "value" field.
      </td>
      <td>
         It is a mandatory field. It contains either id or text.
      </td>
      <td>
         19778 &nbsp;&nbsp;or<br>
         sx1533 &nbsp;&nbsp; or<br>
         *Potato*
      </td>
    </tr>
    <tr>
      <td>
         3.
      </td>
      <td>
         Press submit button.
      </td>
      <td>
         to submit your request.
      </td>
      <td>

      </td>
    </tr>
    <tr>
      <td>
         4.
      </td>
      <td>
         Generic Search button.
      </td>
      <td>
         Link to access generic search application.
      </td>
      <td>
      </td>
    </tr>
</table>

%{--Fields Description : End --}%

%{--Screen shots(s) : Start--}%
<div>
    <h1>Screen shots(s)</h1>
</div>

<h2>Screen-1(Example of search application.)</h2>
<img class="help_image" src="${resource(dir:'/help/images/search', file:'search_input.png')}"/>


<h2>Screen-2(Search Result screen)</h2>
<div class="screen_desc">
   (1.)&nbsp;Result page has pagination
</div>
<div class="screen_desc">
   (2.)&nbsp;Also provide download feature. Download available in most common formats like CSV, Excel, PDF and RTF.
</div>
<img class="help_image" src="${resource(dir:'/help/images/search', file:'search_result.png')}"/>

<h2>Screen-3(Experiment Info.)</h2>
<div class="screen_desc">
   (1.)&nbsp;Provide whole information about experiment.
</div>
<img class="help_image" src="${resource(dir:'/help/images/search', file:'experiment_info.png')}"/>

%{--Screen shots(s) : End--}%
<br>