<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: 2/28/11
  Time: 3:47 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
%{--About Application : Start--}%
<div>
    <h1>Application Info</h1>
</div>

<table>
    <tr>
        <td class="info_title">
            Application Name
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           Generic Search
        </td>
    </tr>
    <tr>
        <td class="info_title">
            Location
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           Home Page -> Search Form
        </td>
    </tr>
    <tr>
        <td class="info_title">
            Version
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           1.0
        </td>
    </tr>
    <tr>
        <td class="info_title">
            Description
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           To design more flexible search criteria
        </td>
    </tr>
</table>
%{--About Application : End --}%

%{--Application Form(s) : Start--}%
<div>
    <h1>Application Form(s)</h1>
</div>

<div class="help_screen_div">
   <img class="help_image" src="${resource(dir:'/help/images/genericsearch', file:'genericsearch_form.png')}"/>
</div>


%{--Application Form(s) : End--}%

%{--Fields Description : Start--}%
<div>
    <h1>Fields Description</h1>
</div>
<table>
    <tr>
        <th>
           Sr.No.
        </th>
        <th>
           Action
        </th>
        <th>
           Description/Validation
        </th>
        <th>
           Remarks/Examples
        </th>
    </tr>
    <tr>
      <td>
         1.
      </td>
      <td>
         Select properties to view into the result.
      </td>
      <td>
         It is a mandatory field.
      </td>
      <td>

      </td>
    </tr>
    <tr>
      <td>
         2.
      </td>
      <td>
         where block -> Add criteria
      </td>
      <td>
         To add more criteria for the search
      </td>
      <td>
         Criteria must add at the last position of the where block.
      </td>
    </tr>
    <tr>
      <td>
         3.
      </td>
      <td>
         where block -> Remove criteria
      </td>
      <td>
         To remove criteria from the search
      </td>
      <td>
         Last Criteria would be remove from where block.
      </td>
    </tr>
    <tr>
      <td>
         4.
      </td>
      <td>
         where block -> Select an Option
      </td>
      <td>
         It is a mandatory field. It used to make relationship between two adjacent criteria.
      </td>
      <td>
          Add Option default selected.
      </td>
    </tr>
    <tr>
      <td>
         5.
      </td>
      <td>
         where block -> Select a Criteria
      </td>
      <td>
         It is a mandatory field. This is a field on which searching will perform.
      </td>
      <td>
         It contains common attributes of setupx objects like Experiment Title, Abstract, Description, sample title etc.
      </td>
    </tr>
    <tr>
      <td>
         6.
      </td>
      <td>
         where block -> Select an Operation
      </td>
      <td>
         It is a mandatory field. This is an operation which is performed on Criteria.
      </td>
      <td>
         like Title Equals
      </td>
    </tr>
    <tr>
      <td>
         7.
      </td>
      <td>
         where block -> Search content
      </td>
      <td>
         It is a mandatory field. Enter searching text.
      </td>
      <td>
         *Citrus Healthy* <br> or <br> *FSA <br> or  <br> Extraction Optimization
      </td>
    </tr>
    <tr>
      <td>
         8.
      </td>
      <td>
         order by block -> Add Order
      </td>
      <td>
         To add more ordering on search result.
      </td>
      <td>

      </td>
    </tr>
    <tr>
      <td>
         9.
      </td>
      <td>
         order by block -> Remove order
      </td>
      <td>
         To remove ordering from the search result.
      </td>
      <td>

      </td>
    </tr>
    <tr>
      <td>
         10.
      </td>
      <td>
         order by block -> Options
      </td>
      <td>
         It is a mandatory field. This is an ordering field.
      </td>
      <td>

      </td>
    </tr>
    <tr>
      <td>
         11.
      </td>
      <td>
         order by block -> Order type
      </td>
      <td>
         It is a mandatory field. Which kind of ordering you want to perform on search result.
      </td>
      <td>
         Ascending Type is default selected.
      </td>
    </tr>
    <tr>
      <td>
         12.
      </td>
      <td>
         Press submit button.
      </td>
      <td>
         to submit your request.
      </td>
      <td>

      </td>
    </tr>
</table>

%{--Fields Description : End --}%

%{--Screen shots(s) : Start--}%
<div>
    <h1>Screen shots(s)</h1>
</div>

<h2>Screen-1(Example of Generic Search application.)</h2>
<img class="help_image" src="${resource(dir:'/help/images/genericsearch', file:'genericsearch_input.png')}"/>


<h2>Screen-2(Search Result screen)</h2>
<div class="screen_desc">
   (1.)&nbsp;provide download feature. Download available in most common formats like CSV, Excel, PDF and RTF.
</div>
<img class="help_image" src="${resource(dir:'/help/images/genericsearch', file:'genericsearch_result.png')}"/>

%{--Screen shots(s) : End--}%
<br>