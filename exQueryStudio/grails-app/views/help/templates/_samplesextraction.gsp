<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: 2/25/11
  Time: 11:08 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
%{--About Application : Start--}%
<div>
    <h1>Application Info</h1>
</div>

<table>
    <tr>
        <td class="info_title">
            Application Name
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           Samples Extraction
        </td>
    </tr>
    <tr>
        <td class="info_title">
            Location
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           Home Page
        </td>
    </tr>
    <tr>
        <td class="info_title">
            Version
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           1.0
        </td>
    </tr>
    <tr>
        <td class="info_title">
            Description
        </td>
        <td class="info_gap">:</td>
        <td class="info_value">
           To extract samples by bunch of setupx object Ids.
        </td>
    </tr>
</table>
%{--About Application : End --}%

%{--Application Form(s) : Start--}%
<div>
    <h1>Application Form(s)</h1>
</div>

<div class="help_screen_div">
   <img class="help_image" src="${resource(dir:'/help/images/samplesextraction', file:'samplesextraction_form.png')}"/>
</div>


%{--Application Form(s) : End--}%

%{--Fields Description : Start--}%
<div>
    <h1>Fields Description</h1>
</div>
<table>
    <tr>
        <th>
           Sr.No.
        </th>
        <th>
           Action
        </th>
        <th>
           Description/Validation
        </th>
        <th>
           Remarks/Examples
        </th>
    </tr>
    <tr>
      <td>
         1.
      </td>
      <td>
         Enter list of Ids.
      </td>
      <td>
         It is a mandatory field. Ids should be comma, tab, new line or single space separated.
      </td>
      <td>
         20321 20204 12345 38293&nbsp;&nbsp;or<br>
         20321,20204,12345,38293 &nbsp;&nbsp; or<br>
         20321<br>20204<br>12345<br>38293
      </td>
    </tr>
    <tr>
      <td>
         2.
      </td>
      <td>
         Select type of ids.
      </td>
      <td>
         It is a mandatory field. Extraction will perform basis of selected type.
      </td>
      <td>
         check "Samples"
      </td>
    </tr>
    <tr>
      <td>
         3.
      </td>
      <td>
         Press submit button.
      </td>
      <td>
         to submit your request.
      </td>
      <td>

      </td>
    </tr>
</table>

%{--Fields Description : End --}%

%{--Screen shots(s) : Start--}%
<div>
    <h1>Screen shots(s)</h1>
</div>

<h2>Screen-1(Example of Samples Extraction application.)</h2>
<img class="help_image" src="${resource(dir:'/help/images/samplesextraction', file:'samplesextraction_input.png')}"/>


<h2>Screen-2(Result screen)</h2>
<div class="screen_desc">
   (1.)&nbsp;Colored indentation. <span class="searchdatafound">Matched</span> and <span class="searchdatanotfound">UnMatched</span>
</div>
<div class="screen_desc">
   (2.)&nbsp;Also provide download feature. Download available in most common formats like CSV, Excel, PDF and RTF.
</div>
<img class="help_image" src="${resource(dir:'/help/images/samplesextraction', file:'samplesextraction_result.png')}"/>

<h2>Screen-3(Samples Extraction by chromatof)</h2>
<div class="screen_desc">
   (1.)&nbsp;Highlight matched chromatofs.
</div>
<img class="help_image" src="${resource(dir:'/help/images/samplesextraction', file:'samplesextraction_ chromatof _result.png')}"/>

%{--Screen shots(s) : End--}%
<br>