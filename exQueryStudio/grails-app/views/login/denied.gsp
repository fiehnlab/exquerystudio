<head>
<meta name='layout' content='main' />
<title>${message(code: 'ProjectName', default: 'exQueryStudio')}: Denied</title>
</head>

<body>
<div class='body'>
	<div class='errors'>Sorry, you're not authorized to view this page.</div>
</div>
</body>
