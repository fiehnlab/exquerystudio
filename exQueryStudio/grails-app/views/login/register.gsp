<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Jul 22, 2010
  Time: 2:52:47 PM
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>${message(code: 'ProjectName', default: 'exQueryStudio')}: Register</title>
</head>

<body>

    <div id="container">
  <div id="header">
        <g:render template="../templates/header"/>
  </div>

  <!-- Start div main -->
  <div id="main">
    <div class="box">
      <form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>

       <div class="groupbox">

             <div id="login">
            <table align="center" border="0">
              <tr>
                <td colspan="2">
                  &nbsp;
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <div class="appsHeader"> Registration </div>
                </td>
              </tr>

               <tr class="prop" >
                  <td valign="top" class="name" style="word-wrap:break-word" colspan="2">
                     <g:if test='${flash.message}'>
                      <div class='errors'>${flash.message}</div>
                     </g:if>
                  </td>
               </tr>


              <tr>
                <td>
                   <img src="${resource(dir:'images/forms',file:'user128.png')}" style="padding: 2px; width:100px" align="left">
                </td>

                 <td>

              <table>
                        <tbody>

                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="username"><g:message code="user.username.label" default="Username" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userInstance, field: 'username', 'errors')}">
                                    <g:textField name="username" value="${userInstance?.username}" />
                                </td>
                            </tr>

                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="password"><g:message code="user.password.label" default="Password" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userInstance, field: 'password', 'errors')}">
                                    <g:textField name="password" value="${userInstance?.password}" />
                                </td>
                            </tr>

                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="password">Confirm Password</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userInstance, field: 'password', 'errors')}">
                                    <g:textField name="password" value="${userInstance?.password}" />
                                </td>
                            </tr>

                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="email"><g:message code="user.email.label" default="Email" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userInstance, field: 'email', 'errors')}">
                                    <g:textField name="email" value="${userInstance?.email}" />
                                </td>
                            </tr>

                            %{--<tr class="prop">--}%
                                %{--<td valign="top" class="name">--}%
                                    %{--<label for="enabled"><g:message code="user.enabled.label" default="Enabled" /></label>--}%
                                %{--</td>--}%
                                %{--<td valign="top" class="value ${hasErrors(bean: userInstance, field: 'enabled', 'errors')}">--}%
                                    %{--<g:checkBox name="enabled" value="${userInstance?.enabled}" />--}%
                                %{--</td>--}%
                            %{--</tr>--}%

                            %{--<tr class="prop">--}%
                                %{--<td valign="top" class="name">--}%
                                    %{--<label for="passwordExpired"><g:message code="user.passwordExpired.label" default="Password Expired" /></label>--}%
                                %{--</td>--}%
                                %{--<td valign="top" class="value ${hasErrors(bean: userInstance, field: 'passwordExpired', 'errors')}">--}%
                                    %{--<g:checkBox name="passwordExpired" value="${userInstance?.passwordExpired}" />--}%
                                %{--</td>--}%
                            %{--</tr>--}%

                            %{--<tr class="prop">--}%
                                %{--<td valign="top" class="name">--}%
                                    %{--<label for="accountExpired"><g:message code="user.accountExpired.label" default="Account Expired" /></label>--}%
                                %{--</td>--}%
                                %{--<td valign="top" class="value ${hasErrors(bean: userInstance, field: 'accountExpired', 'errors')}">--}%
                                    %{--<g:checkBox name="accountExpired" value="${userInstance?.accountExpired}" />--}%
                                %{--</td>--}%
                            %{--</tr>--}%

                            %{--<tr class="prop">--}%
                                %{--<td valign="top" class="name">--}%
                                    %{--<label for="accountLocked"><g:message code="user.accountLocked.label" default="Account Locked" /></label>--}%
                                %{--</td>--}%
                                %{--<td valign="top" class="value ${hasErrors(bean: userInstance, field: 'accountLocked', 'errors')}">--}%
                                    %{--<g:checkBox name="accountLocked" value="${userInstance?.accountLocked}" />--}%
                                %{--</td>--}%
                            %{--</tr>--}%

                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="authorities">Authority</label>
                                </td>

                                <td valign="top" class="value">
                                  Guest
                                    <g:hiddenField name="authoritie" value="ROLE_USER" />

                                </td>
                            </tr>
                            <tr class="prop">
                                <td valign="top"  colspan="2">
                                    <input type='submit' value='Submit' class="buttonSubmit" disabled="disabled" align="middle"/>
                                    <font color='red'> (Under Construction) </font>
                                </td>
                            </tr>
                        </tbody>
                    </table>
              </td>
            </tr>
           </table>
          </div>
              <!-- End div groupbox -->


         </div>

          </form>
        <div class="groupbox">
          <g:render template="../templates/blank"/>
        </div>
    </div>

    </div>
      <!-- End div main -->

  <div id="footer">
            <g:render template="../templates/footer"/>
  </div>
</div>
<script type='text/javascript'>
<!--
(function(){
	document.forms['loginForm'].elements['j_username'].focus();
})();
// -->
</script>

</body>
</html>