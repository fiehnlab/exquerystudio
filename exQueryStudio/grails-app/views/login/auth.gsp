<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title> ${message(code: 'ProjectName', default: 'exQueryStudio')}: Login</title>
</head>

<body>

    <div id="container">
  <div id="header">
        <g:render template="../templates/header"/>
  </div>

  <!-- Start div main -->
  <div id="main">
    <div class="groupbox">
      <form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>

       <div class="groupbox">

             <div id="login">
            <table align="center" border="0">
              <tr>
                <td colspan="2">
                  &nbsp;
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <div class="appsHeader"> Login.. </div>
                </td>
              </tr>

               <tr class="prop" >
                  <td valign="top" class="name" style="word-wrap:break-word" colspan="2">
                     <g:if test='${flash.message}'>
                      <div class='errors'>${flash.message}</div>
                     </g:if>
                  </td>
               </tr>


              <tr>
                <td>
                   <img src="${resource(dir:'images/forms',file:'login128.png')}" style="padding: 2px; width:100px" align="left">
                </td>

                 <td>

              <table>


                <tbody>

                    <tr class="prop">
                        <td valign="top" class="name">
                            <label for='username'>Login ID</label>
                        </td>
                        <td valign="top">
                            <input type='text' class='text_' name='j_username' id='username' />
                        </td>
                    </tr>

                    <tr class="prop">
                        <td valign="top" class="name">
                            <label for='password'>Password</label>
                        </td>
                        <td valign="top">
                            <input type='password' class='text_' name='j_password' id='password' />
                        </td>
                    </tr>

                    <tr class="prop">
                        <td valign="top" class="name">
                            <label for='remember_me'>Remember me</label>
                        </td>
                        <td valign="top">
                            <input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me'
                            <g:if test='${hasCookie}'>checked='checked'</g:if> />
                        </td>
                    </tr>

                    <tr class="prop">
                        <td valign="top"  colspan="2">
                            <input type='submit' value='Submit' class="buttonSubmit"/>
                        </td>
                    </tr>
                </tbody>
           </table>
              </td>
            </tr>
           </table>
          </div>
              <!-- End div groupbox -->


         </div>

          </form>
        <div class="groupbox">
          <g:render template="../templates/blank"/>
        </div>
    </div>

    </div>
      <!-- End div main -->

  <div id="footer">
            <g:render template="../templates/footer"/>
  </div>
 </div>
<script type='text/javascript'>
<!--
(function(){
	document.forms['loginForm'].elements['j_username'].focus();
})();
// -->
</script>

</body>
