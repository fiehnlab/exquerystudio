<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: 3/2/11
  Time: 2:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="main"/>
</head>

<body>
<div id="container">
  <div id="header">
        <g:render template="../templates/header"/>
  </div>

  <div id="main">

      <div class="box">
         <div class="nav">
            <span class="menuButton"><g:link controller="homePage" action="index" class="home">Home</g:link></span>
         </div>

         <div class="appsHeader"> Web Services </div>

         <div class="info">
              <table>
                  <tr>
                      <th>Service Name</th>
                      <th width="300px">Service Method(s)</th>
                      <th width="300px">
                          Input Arguments(s)
                          <br> Argument_Name(Data_Type): Description
                      </th>
                      <th>Result</th>
                      <th>Remark</th>
                  </tr>

                  <tr class="even">
                      <td rowspan="3">
                          1.<a href="${resource(dir:'',file:'')}/services/experimentWeb?wsdl" target="_blank" >Experiment Web Service </a>
                          <div class="description">To extract experiment metadata by providing one of experimentId, classId or sampleId.</div>
                      </td>
                      <td>1.1 &nbsp; experimentMetaDataXMLByExperimentId </td>
                      <td>
                          <table class="table_noborder">
                              <tr>
                                  <td>experimentId(long): Id of the experiment <br>Ex. 101879</td>
                              </tr>
                          </table>
                      </td>
                      <td> XML with experiment metadata</td>
                      <td></td>
                  </tr>

                  <tr class="odd">
                      <td>1.2 &nbsp; experimentMetaDataXMLByClassId </td>
                      <td>
                          <table class="table_noborder">
                              <tr>
                                  <td>classId(long): Id of the class <br>Ex. 102139</td>
                              </tr>
                          </table>
                      </td>
                      <td> XML with experiment metadata</td>
                      <td></td>
                  </tr>

                  <tr class="even">
                      <td>1.3 &nbsp; experimentMetaDataXMLBySampleId </td>
                      <td>
                          <table class="table_noborder">
                              <tr>
                                  <td>sampleId(long): Id of the class <br>Ex. 102204</td>
                              </tr>
                          </table>
                      </td>
                      <td> XML with experiment metadata</td>
                      <td></td>
                  </tr>


                  <tr class="odd">
                      <td rowspan="1">
                          2.<a href="${resource(dir:'',file:'')}/services/sampleWeb?wsdl" target="_blank">Sample Web Service </a>
                          <div class="description">To extract sample metadata.</div>

                      </td>
                      <td>2.1 &nbsp; getSampleIdBySampleLabel </td>
                      <td>
                      <table class="table_noborder">
                          <tr>
                              <td>sampleLabel(String): Id of the sample <br>Ex. sa39</td>
                          </tr>
                      </table>
                      </td>
                      <td> Id of the sample in string type.</td>
                      <td></td>
                  </tr>

              </table>
         </div>

         <div class="notes">
            <div class="notes_header">Note(s): </div>
            <ul>
                 <li class="notes_note"> Click on <b>service name</b> for WSDL file. </li>
            </ul>
         </div>

      </div>
      <div class="groupbox">
       <g:render template="../templates/blank"/>
     </div>
  </div>

<div id="footer">
            <g:render template="../templates/footer"/>
  </div>
</div>


</body>
</html>