
<%@ page import="auth.util.RoleUtil; auth.entity.Requestmap" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'requestmap.label', default: 'Requestmap')}" />
        <title>${message(code: 'ProjectName', default: 'exQueryStudio')}: <g:message code="default.list.label" args="[entityName]" /></title>
    </head>

<body>
<div id="container">
      <div id="header">
            <g:render template="../templates/header"/>
      </div>

      <!-- Start div main -->
      <div id="main">
        <div class="box">

        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
          
        <div class="groupbox">
            %{--<h1><g:message code="default.list.label" args="[entityName]" /></h1>--}%
            <div class="appsHeader"> <g:message code="default.list.label" args="[entityName]" /> </div>
          
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
          <div class="info">
            <div class="list">
                <table>
                    <thead>
                        <tr>

                            <g:sortableColumn property="id" title="${message(code: 'requestmap.id.label', default: 'Id')}" />

                            <g:sortableColumn property="url" title="${message(code: 'requestmap.url.label', default: 'Url')}" />

                            <g:sortableColumn property="configAttribute" title="${message(code: 'requestmap.configAttribute.label', default: 'Config Attribute')}" />

                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${requestmapInstanceList}" status="i" var="requestmapInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                            <td><g:link action="show" id="${requestmapInstance.id}">${fieldValue(bean: requestmapInstance, field: "id")}</g:link></td>

                            <td>${fieldValue(bean: requestmapInstance, field: "url")}</td>

                            <td>${ RoleUtil.getInstance().getConfigAttributeDescription(requestmapInstance?.configAttribute) }</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            %{--<div class="paginateButtons">--}%
                %{--<g:paginate total="${requestmapInstanceTotal}" />--}%
            %{--</div>--}%

      <div class="pagination_bottom">
        <g:customPaginate total="${requestmapInstanceTotal}" prev="&lt; previous" next="next &gt;"/>
      </div>
       </div>

        </div>
        <!-- End div groupbox -->
      </div>
        <!-- End div box -->

        <div class="groupbox">
          <g:render template="../templates/blank"/>
        </div>
      </div>
      <!-- End div main -->

      <div id="footer">
         <g:render template="../templates/footer"/>
      </div>
</div>    


</body>
</html>
