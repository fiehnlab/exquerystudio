
<%@ page import="auth.util.RoleUtil; auth.entity.Requestmap" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'requestmap.label', default: 'Requestmap')}" />
        <title>${message(code: 'ProjectName', default: 'exQueryStudio')}: <g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

<div id="container">
  <div id="header">
        <g:render template="../templates/header"/>
  </div>

  <!-- Start div main -->
  <div id="main">
    <div class="box">
    
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="groupbox">
            %{--<h1><g:message code="default.show.label" args="[entityName]" /></h1>--}%
            <div class="appsHeader"> <g:message code="default.show.label" args="[entityName]" /> </div>
          
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="requestmap.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: requestmapInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="requestmap.url.label" default="Url" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: requestmapInstance, field: "url")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="requestmap.configAttribute.label" default="Config Attribute" /></td>
                            
                            <td valign="top" class="value">${ RoleUtil.getInstance().getConfigAttributeDescription(requestmapInstance?.configAttribute) }</td>

                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${requestmapInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
      </div>
        <!-- End div groupbox -->

        <div class="groupbox">
          <g:render template="../templates/blank"/>
        </div>
    </div>
    <!-- End div main -->

  <div id="footer">
            <g:render template="../templates/footer"/>
  </div>
    </div>



    </body>
</html>
