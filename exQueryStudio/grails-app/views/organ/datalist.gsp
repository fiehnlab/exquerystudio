<%@ page import="exquery.util.CommonUtil" %>
<html>
<head>
  <meta name="layout" content="main"/>
  <export:resource />
</head>

<body>
<div id="container">
       <div id="header">
            <g:render template="../templates/header"/>
       </div>

      <div id="main">
        <div class="box">
            <div class="nav">
            <span class="menuButton"><g:link controller="homePage" action="index" class="home">Home</g:link></span>
            <span class="menuButton"><a href="javascript://nop" class="back" onclick="history.back(1)">Back to result</a></span>
         </div>

         <div class="appsHeader"> ${title} </div>
         <div class='info_header' style="vertical-align:bottom">
            <span class="info_title">&nbsp;Organ</span>
         </div>
         <div class="info">
              <table width="100%" cellpadding="0" cellspacing="0" class="table_noborder">
                  <tr>
                    <td width="10%" class="forminputlabel"> <g:message code="SXorgan.organName"/> </td>
                    <td width="1%"> : </td>
                    <td width="30%">${organName}</td>
                    <td width="1%">&nbsp;</td>
                    <td width="10%" class="forminputlabel"> <g:message code="SXorgan.species"/></td>
                    <td width="1%"> : </td>
                    <td width="30%">${CommonUtil.getInstance().convertNameListIntoString(organInfo?.species)}</td>
                  </tr>

                  <g:if test='${lookupBy?.toString().trim().equalsIgnoreCase("experiments")}'>
                  <tr>
                    <td width="10%" class="forminputlabel"> <g:message code="SXorgan.classesCount"/></td>
                    <td width="1%"> : </td>
                    <td width="30%"><g:link controller="organ" action="datalist" params='[lookupBy:"classes",organName:"${organName}"]'>${organInfo?.classesCount}</g:link></td>
                    <td width="1%">&nbsp;</td>
                    <td width="10%" class="forminputlabel"> <g:message code="SXorgan.samplesCount"/></td>
                    <td width="1%"> : </td>
                    <td width="30%"><g:link controller="organ" action="datalist" params='[lookupBy:"samples",organName:"${organName}"]'>${organInfo?.samplesCount}</g:link></td>
                  </tr>
                   </g:if>
                  <g:if test='${lookupBy?.toString().trim().equalsIgnoreCase("classes")}'>

                  <tr>
                    <td width="10%" class="forminputlabel"> <g:message code="SXorgan.experimentCount"/></td>
                    <td width="1%"> : </td>
                    <td width="30%"><g:link controller="organ" action="datalist" params='[lookupBy:"experiments",organName:"${organName}"]'>${organInfo?.experimentCount}</g:link></td>
                    <td width="1%">&nbsp;</td>
                    <td width="10%" class="forminputlabel"> <g:message code="SXorgan.samplesCount"/></td>
                    <td width="1%"> : </td>
                    <td width="30%"><g:link controller="organ" action="datalist" params='[lookupBy:"samples",organName:"${organName}"]'>${organInfo?.samplesCount}</g:link></td>
                  </tr>
                  </g:if>

                  <g:if test='${lookupBy?.toString().trim().equalsIgnoreCase("samples")}'>
                  <tr>
                    <td width="10%" class="forminputlabel"> <g:message code="SXorgan.experimentCount"/></td>
                    <td width="1%"> : </td>
                    <td width="30%"><g:link controller="organ" action="datalist" params='[lookupBy:"experiments",organName:"${organName}"]'>${organInfo?.experimentCount}</g:link></td>
                    <td width="1%">&nbsp;</td>
                    <td width="10%" class="forminputlabel"> <g:message code="SXorgan.classesCount"/></td>
                    <td width="1%"> : </td>
                    <td width="30%"><g:link controller="organ" action="datalist" params='[lookupBy:"classes",organName:"${organName}"]'>${organInfo?.classesCount}</g:link></td>
                  </tr>
                  </g:if>
              </table>
          </div>

          <div class='info_header' style="vertical-align:bottom">
            <span class="info_title">&nbsp;Result List</span>
          </div>
          <div class="info">
              <table class="result">

                <%
                  if (result.fieldList != null && result.fieldList.size() > 0) {
                %>
               <tr>
                <g:each var="headerField" in="${result.fieldList}">
                <%
                      def cssClass = result.cssClassMap.get(headerField)
                      def header = result.labelMap.get(headerField)
                %>
                  <th class="${cssClass}">${header}</th>

                  %{--<g:sortableColumn property="${headerField}" title="${header}" params="[cmbSearchBy: params.cmbSearchBy,searchText:params.searchText]" />--}%

                </g:each>
                </tr>

                <%
                  }  %>

                <%
                  if (result.resultList != null && result.resultList.size() > 0) {
                    long rowNo=1;
                %>
                   <g:each var="row" in="${result.resultList}" status="index">
                   <%
                      String lStrClass = "";

                      if (index % 2 == 0) {
                        lStrClass = "even"
                      } else {
                        lStrClass = "odd"
                      }
                  %>

                  <tr class='<%=lStrClass%>' onMouseOver="this.className='highlight'" onMouseOut="this.className='<%=lStrClass%>'" >

                    <g:each var="columnfield" in="${result.fieldList}">
                    <%
                          cssClass = result.cssClassMap.get(columnfield)
                          def data = row.get(columnfield)
                    %>
                    <td class="${cssClass}">${data}</td>
                    </g:each>
                  </tr>
                </g:each>

                <%
                  } else {
                %>
                <tr>
                  <td colspan="${result.fieldList?.size}" class='even'>
                    <div class="errors">

                      ${result?.ErrorMsg}

                    </div>
                  </td>

                </tr>
                <% }
                %>
              </table>

              <g:if test="${result?.TotalRecordCount != null}">
                  <div class="pagination_top">
                   <g:customPaginate isPaginate="false" total="${result?.TotalRecordCount}" />
                  </div>

                  <g:if test="${result?.isExport != null && result?.isExport}" >
                      <export:formats formats="['csv', 'excel', 'pdf', 'rtf']" params="[lookupBy: lookupBy,organName:organName]" />
                  </g:if>
              </g:if>

          </div>

     </div>
     <!-- End div box -->
     <div class="groupbox">
       <g:render template="../templates/blank"/>
     </div>


     </div>
     <!-- End div main -->
     <div id="footer">
       <g:render template="../templates/footer"/>
     </div>
</div>
</body>

</html>