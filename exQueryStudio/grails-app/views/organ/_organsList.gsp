<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: 5/6/11
  Time: 12:38 PM
  To change this template use File | Settings | File Templates.
--%>

<div>
    <g:if test="${sxOrgansList!=null && sxOrgansList?.size()>0}">
        <%
            int maxCount=10
            def organ
            for(int index=0; index < sxOrgansList.size() && index < maxCount; index++){
                organ = sxOrgansList.get(index)

        %>
        %{--<g:each in="${sxOrgansList}" var="organ" status="index">--}%
            <g:if test="${index==0}">
                <span style="cursor:pointer" title="ExperimentCount:${organ.experimentCount}, ClassesCount:${organ.classesCount} and SamplesCount:${organ.samplesCount}">${organ.organName}[${organ.samplesCount}]</span>
            </g:if>
            <g:else>
                ;&nbsp;&nbsp;<span style="cursor:pointer" title="ExperimentCount:${organ.experimentCount}, ClassesCount:${organ.classesCount} and SamplesCount:${organ.samplesCount}">${organ.organName}[${organ.samplesCount}]</span>
            </g:else>
        %{--</g:each>--}%
        <%
            }
        %>
        <g:if test="${sxOrgansList.size()>maxCount}">
             <span style="cursor:pointer" > <g:link controller="organ" action="list" title="click to get the list of organs">more...</g:link> </span>
        </g:if>

    </g:if>
    <g:else>
       <div class="errors">
        No Organ
       </div>
    </g:else>
</div>