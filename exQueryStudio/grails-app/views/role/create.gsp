
<%@ page import="auth.entity.SecRole" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}" />
        <title>${message(code: 'ProjectName', default: 'exQueryStudio')}: <g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>

<div id="container">
  <div id="header">
        <g:render template="../templates/header"/>
  </div>

  <!-- Start div main -->
  <div id="main">
    <div class="box">    
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="groupbox">
            <div class="appsHeader"><g:message code="default.create.label" args="[entityName]" /></div>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${roleInstance}">
            <div class="errors">
                <g:renderErrors bean="${roleInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="authority"><g:message code="role.authority.label" default="Authority" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roleInstance, field: 'authority', 'errors')}">
                                    <g:textField name="authority" value="${roleInstance?.authority}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="description"><g:message code="role.description.label" default="Description" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roleInstance, field: 'description', 'errors')}">
                                    <g:textField name="description" value="${roleInstance?.description}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="remarks"><g:message code="role.remarks.label" default="Remarks" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roleInstance, field: 'remarks', 'errors')}">
                                    <g:textField name="remarks" value="${roleInstance?.remarks}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>

    </div>
          </div>
            <!-- End div groupbox -->

            <div class="groupbox">
              <g:render template="../templates/blank"/>
            </div>
        </div>
        <!-- End div main -->

      <div id="footer">
                <g:render template="../templates/footer"/>
      </div>
        </div>
    </body>
</html>
