
<%@ page import="auth.entity.SecRole" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}" />
        <title>${message(code: 'ProjectName', default: 'exQueryStudio')}: <g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
    <div id="container">
  <div id="header">
        <g:render template="../templates/header"/>
  </div>

  <!-- Start div main -->
  <div id="main">
    <div class="box">
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="groupbox">
            <div class="appsHeader"><g:message code="default.list.label" args="[entityName]" /></div>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
          <div class="info">
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'role.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="authority" title="${message(code: 'role.authority.label', default: 'Authority')}" />
                        
                            <g:sortableColumn property="description" title="${message(code: 'role.description.label', default: 'Description')}" />
                        
                            <g:sortableColumn property="remarks" title="${message(code: 'role.remarks.label', default: 'Remarks')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${roleInstanceList}" status="i" var="roleInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${roleInstance.id}">${fieldValue(bean: roleInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: roleInstance, field: "authority")}</td>
                        
                            <td>${fieldValue(bean: roleInstance, field: "description")}</td>
                        
                            <td>${fieldValue(bean: roleInstance, field: "remarks")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            %{--<div class="paginateButtons">--}%
                %{--<g:paginate total="${roleInstanceTotal}" />--}%
            %{--</div>--}%
                    <div class="pagination_bottom">
                  <g:customPaginate total="${roleInstanceTotal}" prev="&lt; previous" next="next &gt;"/>
                </div>
                 </div>
                  </div>

              </div>
                    </div>
                      <!-- End div groupbox -->

                      <div class="groupbox">
                        <g:render template="../templates/blank"/>
                      </div>
                  </div>
                  <!-- End div main -->

                <div id="footer">
                          <g:render template="../templates/footer"/>
                </div>
                  </div>


    </body>
</html>
