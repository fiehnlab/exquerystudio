<%@ page contentType="text/html;charset=UTF-8" %>


<html>
<head>
  <meta name="layout" content="main"/>
</head>

<body>
<div id="container">
      <div id="header">
            <g:render template="../templates/header"/>
      </div>

      <div id="main">
        <div class="groupbox">
        <!-- Start Div left -->
        <div id="left">

         <!-- Add Top 10 Species template : Start-->
         <div class='subgroup' title="List of species with samples count">
            <div class='info_header'>
              <span class="info_image" title="click to get the list of species">
                  <g:link controller="species" action="list">
                    <img src="${resource(dir:'images/skin',file:'list.png')}" width="20px" style="padding: 2px" class="noborder">
                  </g:link>
              </span>
              <span class="info_title">Species</span>
            </div>
            <div class="info">
              <g:render template="../species/speciesList" model="[sxSpeciesList:sxSpeciesList]"/>
            </div>
          </div>
          <!-- Top 10 Species template : End-->

          <!-- Add Top 10 Organs template : Start-->
          <div class='subgroup' title="List of organs with samples count">
            <div class='info_header'>
              <span class="info_image" title="click to get the list of organs">
                <g:link controller="organ" action="list">
                    <img src="${resource(dir:'images/skin',file:'list.png')}" width="20px" style="padding: 2px" class="noborder">
                </g:link>
              </span>
              <span class="info_title">Organs</span>
            </div>
            <div class="info">
              <g:render template="../organ/organsList" model="[sxOrgansList:sxOrgansList]"/>
            </div>
          </div>
          <!-- Top 10 Organs template : Start-->
          <!-- Services template : Start-->
          <div class='subgroup'>
            <div class='info_header'>
              <span class="info_image">
                <img src="${resource(dir:'images/forms',file:'moreservices.png')}" width="20px" style="padding: 2px">
              </span>
              <span class="info_title">Services</span>
            </div>
            <div class="info">
              <g:render template="../templates/moreservices"/>
            </div>
          </div>
          <!-- Services template : End-->

          <!-- Direct Links template : Start-->
          <div class='subgroup'>
            <div class='info_header'>
              <span class="info_image">
                <img src="${resource(dir:'images/forms',file:'favorites.png')}" width="20px" style="padding: 2px">
              </span>
              <span class="info_title">Direct Links</span>
            </div>
            <div class="info">
              <g:render template="../templates/directLinks"/>
            </div>
          </div>
          <!-- Direct Links template : End-->

        <sec:ifAnyGranted roles="ROLE_SUPER_ADMIN">
          <div class='subgroup'>
            <div class='info_header'>
              <span class="info_image">
                <img src="${resource(dir:'images/forms',file:'adminapps24.png')}" width="20px" style="padding: 2px">
              </span>
              <span class="info_title">Admin Applications</span>
            </div>
            <div class="info">
              <g:render template="../templates/adminApps"/>
            </div>
          </div>
        </sec:ifAnyGranted>
      </div>
     <!-- End Div left -->

     <!-- Start div center -->
     <div id="center">

          <!-- Add Welcome Block : Start-->
          <div class='subgroup'>
          <div class='info_header'>
            <span class="info_image">
              <img src="${resource(dir:'images/forms',file:'home.png')}" width="20px" style="padding: 2px">
            </span>
            <span class="info_title">
              Welcome
            </span>
          </div>
          <div class="info">

          <p>
            &nbsp;&nbsp;&nbsp;&nbsp;${message(code: 'ProjectName', default: 'SetupX-Query')} attempts to provides an easy and flexible way to query experiment and studies of SetupX project.
            <br>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;The system also provides you following features:
            <ul>
              <li>Search on setupX objects.</li>
              <li>Samples extraction (Process batch of ids at the same time.)</li>
              <li>Design your own query with multiple search criteria.</li>
            </ul>
            </p>
          </p>
          </div>
        </div>
          <!-- Welcome Block : End-->

          <!-- Add Chart template : Start-->
          <div class='subgroup'>
            <div class='info_header'>
              <span class="info_image">
                  <img src="${resource(dir:'images/forms',file:'piechart.png')}" width="20px" style="padding: 2px" >
              </span>
              <span class="info_title">Chart</span>
              %{--<span class="info_alert" onclick="window.open('${resource(dir:'',file:'')}/help.gsp?application=search', 'HelpDocs', 'menubar=0,resizable=0,location=1,status=0,scrollbars=1,  width=800,height=600');" target="_blank" title="info">&nbsp;&nbsp;&nbsp;</span>--}%
            </div>
            <div class="info">
              <g:render template="../templates/speciesChart"/>
            </div>
          </div>
          <!-- Chart template : Start-->

     </div>
     <!-- End Div center -->

     <div  id="Right">

          <!-- Add Public Data : Start-->
          <div class='subgroup'>
            <div class='info_header'>
                <span class="info_image">
                  <img src="${resource(dir:'images/forms',file:'earth24.png')}" width="20px" style="padding: 2px">
                </span>
                <span class="info_title">
                  Public Data
                </span>
            </div>
            <div class="info">
              <p>
                &nbsp;&nbsp;&nbsp;&nbsp;
                A set of ${PrototypeInfo?.classesSize} classes out of the ${PrototypeInfo?.samplesSize} samples related to ${PrototypeInfo?.experimentSize} experiments is publicly available
                - including the whole experimental design and the annotated GC-TOF result data.
                Use ${message(code: 'ProjectName', default: 'SetupX-Query')} to query the whole system for the samples that you are interested in.
                <br>
              <p>
            </div>
          </div>
          <!-- Public Data : End-->

          <!-- Add Search template : Start-->
          <div class='subgroup'>
            <div class='info_header'>
              <span class="info_image">
                  <img src="${resource(dir:'images/forms',file:'search24.png')}" width="20px" style="padding: 2px" >
              </span>
              <span class="info_title">Search</span>
              <span class="info_alert" onclick="window.open('${resource(dir:'',file:'')}/help.gsp?application=search', 'HelpDocs', 'menubar=0,resizable=0,location=1,status=0,scrollbars=1,  width=800,height=600');" target="_blank" title="info">&nbsp;&nbsp;&nbsp;</span>
            </div>
            <div class="info">
              <g:render template="../search/searchForm" model="[getSXSearchObjectType:getSXSearchObjectType]"/>
            </div>
          </div>
          <!-- End Search template : Start-->

          <!-- SamplesExtraction template : Start-->
          <div class='subgroup'>
            <div class='info_header'>
              <span class="info_image">
                  <img src="${resource(dir:'images/forms',file:'search24.png')}" width="20px" style="padding: 2px" >
              </span>
              <span class="info_title">Samples Extraction</span>
              <span class="info_alert" onclick="window.open('${resource(dir:'',file:'')}/help.gsp?application=samplesextraction', 'HelpDocs', 'menubar=0,resizable=0,location=1,status=0,scrollbars=1,  width=800,height=600');" target="_blank" title="info">&nbsp;&nbsp;&nbsp;</span>
            </div>
            <div class="info">
              <g:render template="../query/samplesExtraction" model="[sxObjectType:sxObjectType]"/>
            </div>
          </div>
          <!-- SamplesExtraction template : End-->

        </div>

     </div>
      <!-- End div groupbox -->

     <div class="groupbox">
       <g:render template="../templates/blank"/>
     </div>
    </div>
    <!-- End div main -->

  <div id="footer">
            <g:render template="../templates/footer"/>
  </div>
</div>


</body>
</html>