package exquery.util

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class CommonUtilTests extends GrailsUnitTestCase{
    Logger gLogger = Logger.getLogger(CommonUtilTests.class)
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testPrepareSearchableIdList() {
      def ids = "1234 1239 8978 98989 909089"
      def idList = CommonUtil.getInstance().prepareSearchableIdList(ids)
      gLogger.info idList
      assertTrue  idList != null
      assertTrue  idList.size() == 5
    }

    void testToUpperList(){
       def taxonomyList = ["potato", "Solanum tuberosum L.", "potatoes", "Solanum tuberosum subsp. tuberosum", "Solanum tuberosum"]
       def taxonomyUpperCaseList = CommonUtil.getInstance().toUpper(taxonomyList)
       println taxonomyUpperCaseList

    }

    void testToUpperArray(){
       String[] taxonomyList = new String[5];
       taxonomyList[0] = "potato"
       taxonomyList[1] = "Solanum tuberosum L."
       taxonomyList[2] = "potatoes"
       taxonomyList[3] = "Solanum tuberosum subsp. tuberosum"
       taxonomyList[4] = "Solanum tuberosum"
       def taxonomyUpperCaseList = CommonUtil.getInstance().toUpper(taxonomyList)
       println taxonomyUpperCaseList

    }

    void testToUpper(){
        def data="potato"
        def taxonomyUpperCase = CommonUtil.getInstance().toUpper(data)
        println taxonomyUpperCase

    }
}
