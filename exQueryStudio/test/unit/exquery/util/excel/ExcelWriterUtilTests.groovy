package exquery.util.excel

import org.apache.log4j.Logger
import grails.test.GrailsUnitTestCase
import exquery.util.excel.valuebeans.ExcelVO
import exquery.util.excel.valuebeans.SheetVO
import exquery.util.excel.valuebeans.RowVO
import exquery.util.excel.valuebeans.CellVO

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/18/11
 * Time: 12:19 PM
 * To change this template use File | Settings | File Templates.
 */
class ExcelWriterUtilTests extends GrailsUnitTestCase{
    Logger gLogger = Logger.getLogger(ExcelWriterUtilTests.class)
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testGenerateExcel() {

      String outputFile = "./test/exceltest/Test.xls";

      ExcelVO excelVO = new ExcelVO()
      excelVO.setExcelName("Test")

      SheetVO sheetVO = new SheetVO()
      sheetVO.setSheetName("sh1")

      RowVO headerData = new RowVO()
      headerData.setIndex(2)
      def cells = []
      CellVO cell = new CellVO()
      //cell.setIndex(3)
      cell.setValue("UserName")
      cells.add(cell)
      cell = new CellVO()
      //cell.setIndex(2)
      cell.setValue("Passwords")
      cells.add(cell)

      headerData.setCells cells

      sheetVO.setHeaderData headerData

      excelVO.setSheets([sheetVO])

      def res = ExcelWriterUtil.getInstance().generateExcel(excelVO)

      println res

      // The Output file is where the xls will be created
      FileOutputStream fOut = new FileOutputStream(outputFile);
      // Write the XL sheet
      res.write(fOut);
      fOut.flush();
      // Done Deal..
      fOut.close();

      gLogger.info res
    }
}
