package exquery.util

import grails.test.GrailsUnitTestCase
import java.util.regex.Matcher
import java.util.regex.Pattern
import org.hibernate.validator.AssertTrue

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/19/11
 * Time: 1:51 PM
 * Test cases for HTML decoder
 */
class HTMLDecoderTests extends GrailsUnitTestCase{

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testDecode() {

      String regex = "&#[0-9]{1,4}[;]{0}"
      Pattern p = Pattern.compile(regex);

      String text = "82 &#181mol/m&#178&#183s"

      String methodName = HTMLDecoder.getInstance().decode(text);
      println("methodName :: ${methodName}")
      assertTrue methodName.equals("82 µmol/m²·s")

      text = "82 &#181;mol/m&#178;&#183;s"
      methodName = HTMLDecoder.getInstance().decode(text);
      println("methodName :: ${methodName}")
      assertTrue methodName.equals("82 µmol/m²·s")

    }
}
