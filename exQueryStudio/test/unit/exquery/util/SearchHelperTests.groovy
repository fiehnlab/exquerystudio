package exquery.util

import grails.test.GrailsUnitTestCase

class SearchHelperTests extends GrailsUnitTestCase{

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testGetSearchMethodName() {

      String methodAnnotation = "Experiments"
      String methodName = SearchHelper.getInstance().getSearchMethodName(methodAnnotation);

      assertTrue  methodName == 'searchExperiments'
    }
}
