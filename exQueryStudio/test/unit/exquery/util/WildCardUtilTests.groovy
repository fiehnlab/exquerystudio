package exquery.util

import grails.test.GrailsUnitTestCase

class WildCardUtilTests extends GrailsUnitTestCase{

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConvertToLikeWildCard() {

      String text = "?etabolomic*";

      String res = WildCardUtil.getInstance().convertToLikeWildCard(text);

      assertTrue  res == '_etabolomic%'

    }


    void testHasWildCards() {

      String text = "Metabolomic*";

      boolean res = WildCardUtil.getInstance().convertToLikeWildCard(text);

      assertTrue  res

    }
}
