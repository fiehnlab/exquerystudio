package exquery.util

import grails.test.GrailsUnitTestCase

class ValidationUtilTests extends GrailsUnitTestCase{

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testIsLong() {

      String value = "1234567";

      boolean res = ValidationUtil.getInstance().isLong(value);

      assertTrue  res

      value = "1234567 ";

      res = ValidationUtil.getInstance().isLong(value);

      assertTrue  !res
    }

}
