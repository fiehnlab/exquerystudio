package auth.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class SecUserTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(SecUserTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
      SecUser rights = new SecUser(id:1101);

      gLogger.info rights.id == 1101

      assertEquals 1101, rights.id
    }
}
