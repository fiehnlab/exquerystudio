package auth.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class SecRoleTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(SecRoleTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
      SecRole role = new SecRole(id:1101);

      gLogger.info role.id == 1101

      assertEquals 1101, role.id
    }
}
