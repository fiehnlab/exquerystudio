package setupx.entity

import grails.test.*
import org.apache.log4j.Logger

class ClassMetadataTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(ClassMetadataTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
      ClassMetadata metadata = new ClassMetadata(id:1101);

      gLogger.info metadata.id == 1101

      assertEquals 1101, metadata.id
    }
}
