package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class ExperimentTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(ExperimentTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
         Experiment exp = new Experiment(id:151);

         gLogger.info exp.id == 151

         assertEquals 151, exp.id
    }
}
