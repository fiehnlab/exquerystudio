package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class ClassOrganTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(ClassOrganTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
      ClassOrgan organ = new ClassOrgan(id:1101);

      gLogger.info organ.id == 1101

      assertEquals 1101, organ.id
    }
}
