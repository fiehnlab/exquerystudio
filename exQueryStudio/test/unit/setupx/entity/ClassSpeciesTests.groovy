package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class ClassSpeciesTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(ClassSpeciesTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
        ClassSpecies species = new ClassSpecies(id:1102);

        gLogger.info species.id == 1102

        assertEquals 1102, species.id
    }
}
