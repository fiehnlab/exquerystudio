package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class SearchConfigTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(SearchConfigTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
        SearchConfig config = new SearchConfig(id:1101);

        gLogger.info config.id == 1101

        assertEquals 1101, config.id
    }
}
