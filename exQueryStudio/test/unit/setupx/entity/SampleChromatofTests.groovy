package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class SampleChromatofTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(SampleChromatofTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
      SampleChromatof chromatof = new SampleChromatof(id:1101);

      gLogger.info chromatof.id == 1101

      assertEquals 1101, chromatof.id
    }
}
