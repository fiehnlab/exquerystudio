package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class ExperimentPublicationTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(ExperimentPublicationTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
      ExperimentPublication publication = new ExperimentPublication(id:1101);

      gLogger.info publication.id == 1101

      assertEquals 1101, publication.id
    }
}
