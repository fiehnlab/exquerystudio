package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class AccessRightsTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(AccessRightsTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
      AccessRights rights = new AccessRights(id:1101);

      gLogger.info rights.id == 1101

      assertEquals 1101, rights.id
    }
}
