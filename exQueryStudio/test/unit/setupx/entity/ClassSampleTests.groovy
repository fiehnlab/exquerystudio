package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class ClassSampleTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(ClassSampleTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
         ClassSample sample = new ClassSample(id:118);

         gLogger.info sample.id == 118

         assertEquals 118, sample.id
    }
}
