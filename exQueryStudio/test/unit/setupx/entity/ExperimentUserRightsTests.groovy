package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class ExperimentUserRightsTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(ExperimentUserRightsTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
      ExperimentUserRights userRights = new ExperimentUserRights(id:1101);

      gLogger.info userRights.id == 1101

      assertEquals 1101, userRights.id
    }
}
