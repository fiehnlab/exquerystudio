package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class SampleMetadataTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(SampleMetadataTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
      SampleMetadata metadata = new SampleMetadata(id:1101);

      gLogger.info metadata.id == 1101

      assertEquals 1101, metadata.id
    }
}
