package setupx.entity

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class ExperimentClassTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(ExperimentClassTests.class)

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
         ExperimentClass experimentclass = new ExperimentClass(id:124);

         gLogger.info experimentclass.id = 124

         assertEquals 124 , experimentclass.id
    }
}
