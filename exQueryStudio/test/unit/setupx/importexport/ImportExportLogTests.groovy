package setupx.importexport

import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger

class ImportExportLogTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(ImportExportLogTests.class)
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {
      ImportExportLog log = new ImportExportLog(id:1101);

      gLogger.info log.id == 1101

      assertEquals 1101, log.id
    }
}
