package setupx.entity

import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Oct 8, 2010
 * Time: 4:51:31 PM
 * test cases for experiment access rights
 */
class ExperimentUserRightsIntegrationTests extends GroovyTestCase{

    Logger gLogger = Logger.getLogger(ExperimentUserRightsIntegrationTests.class)

    void testQuery(){

      gLogger.info('=========== Output ===========')
      def rights = ExperimentUserRights.get((long) 490);
      gLogger.info(rights.getRemarks())
      gLogger.info(rights.experiment)
      gLogger.info(rights.accessRights)
      gLogger.info(rights.secUser)
      gLogger.info('=========== Output ===========')

      assertTrue rights.getRemarks() == '0% of access to the experiment'
      assertTrue rights.experiment.id == 29518
      assertTrue rights.accessRights.id == 1001
      assertTrue rights.secUser.username == 'fsa'
      assertTrue rights.secUser.id == 46
    }
}
