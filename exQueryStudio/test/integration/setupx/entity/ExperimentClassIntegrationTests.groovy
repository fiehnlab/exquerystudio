package setupx.entity

import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Jun 30, 2010
 * Time: 11:28:33 AM
 * Integration test cases for setupx.entity.experimentclass table
 */
class ExperimentClassIntegrationTests extends GroovyTestCase {

    Logger gLogger = Logger.getLogger(ExperimentClassIntegrationTests.class)

    void testQuery(){
      def experimentClass = ExperimentClass.get((long) 28033);

      gLogger.info('=========== Output ===========')
      gLogger.info(experimentClass.getGenotype())
      gLogger.info(experimentClass)
      gLogger.info(experimentClass.experiment)
      gLogger.info(experimentClass.sampleSet.size())
      gLogger.info(experimentClass.organSet)
      gLogger.info(experimentClass.speciesSet)
      gLogger.info('=========== Output ===========')

      assertTrue experimentClass!=null
      assertTrue experimentClass.id == (long) 28033
      assertTrue experimentClass.getGenotype() == 'Agria'
      assertTrue experimentClass.experiment.id == (long) 29518
      assertTrue experimentClass.sampleSet.size() == 30
    }
}
