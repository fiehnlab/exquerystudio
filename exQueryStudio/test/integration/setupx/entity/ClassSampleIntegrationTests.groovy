package setupx.entity

import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Dec 22, 2010
 * Time: 11:29:19 AM
 * Integration test cases for setupx.entity.classsample table
 */
class ClassSampleIntegrationTests extends GroovyTestCase {

    Logger gLogger = Logger.getLogger(ClassSpeciesIntegrationTests.class)

    void testQuery(){
      def sample = ClassSample.get((long) 20153);

      gLogger.info('=========== Output ===========')
      gLogger.info(sample.getLabel())
      gLogger.info(sample)
      gLogger.info(sample.experimentclass)
      gLogger.info(sample.sampleChromatofSet.size())
      gLogger.info('=========== Output ===========')

      assertTrue sample!=null
      assertTrue sample.id == (long) 20153
      assertTrue sample.getLabel() == 'sx976'
      assertTrue sample.experimentclass.id == (long) 20179
    }

}
