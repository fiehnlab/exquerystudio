package setupx.entity

import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Jun 30, 2010
 * Time: 11:28:58 AM
 * Integration test cases for setupx.entity.experiment table
 */
class ExperimentIntegrationTests extends GroovyTestCase{

    Logger gLogger = Logger.getLogger(ExperimentIntegrationTests.class)

    void testQuery(){

      gLogger.info('=========== Output ===========')
      def experiment = Experiment.get((long) 29518);
      gLogger.info(experiment.getTitle())
      gLogger.info(experiment)
      gLogger.info(experiment.experimentclassSet.size())
      gLogger.info(experiment.experimentUserRights)
      gLogger.info('=========== Output ===========')

      assertTrue experiment!=null
      assertTrue experiment.id == (long)29518
      assertTrue experiment.getTitle() == 'FSA Potato 2003'
      assertTrue experiment.experimentclassSet.size() == 48
      assertTrue experiment.experimentUserRights.size() == 7
    }
}
