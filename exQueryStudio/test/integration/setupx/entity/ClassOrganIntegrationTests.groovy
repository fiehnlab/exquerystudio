package setupx.entity

import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Dec 22, 2010
 * Time: 11:27:20 AM
 * Integration test cases for setupx.entity.classorgan table
 */
class ClassOrganIntegrationTests extends GroovyTestCase {

    Logger gLogger = Logger.getLogger(ClassOrganIntegrationTests.class)

    void testQuery(){

      gLogger.info('=========== testQuery ===========')

      def organ = ClassOrgan.get((long) 1952020179);

      gLogger.info('=========== Output ===========')
      gLogger.info(organ.getOrgan_name())
      gLogger.info(organ.getOrgan_id())
      gLogger.info(organ)
      gLogger.info(organ.experimentclass)
      gLogger.info('=========== Output ===========')

      assertTrue organ!=null
      assertTrue organ.id == (long) 1952020179
      assertTrue organ.getOrgan_id() == (long) 19520
      assertTrue organ.getOrgan_name() == 'leaf'
      assertTrue organ.experimentclass.id == (long) 20179
    }
}
