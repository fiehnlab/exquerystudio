package setupx.entity

import org.apache.log4j.Logger

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 3/30/11
 * Time: 2:38 PM
 * To change this template use File | Settings | File Templates.
 */
class ClassMetadataIntegrationTests extends GroovyTestCase {

    Logger gLogger = Logger.getLogger(ClassMetadataIntegrationTests.class)

    void testClassMetadataCount(){
        def experimentClass = ExperimentClass.findById(115837L)

        gLogger.info "experimentClass :: ${experimentClass}"

        assertTrue(experimentClass!=null)
        assertTrue(experimentClass.classMetadataSet!=null)
        assertTrue(experimentClass.classMetadataSet.size() != 8)
    }

    void testExperimentClassByMetaData(){
        def classMetadata = ClassMetadata.findById(1959520179L)

        assertTrue(classMetadata!=null)
        assertTrue(classMetadata.experimentclass!=null)
        assertTrue(classMetadata.experimentclass.getGenotype() == 'Taipei 309')
    }

}
