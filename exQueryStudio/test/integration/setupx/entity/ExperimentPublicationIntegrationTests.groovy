package setupx.entity

import auth.util.UserUtil
import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Dec 22, 2010
 * Time: 6:50:40 PM
 * Integration test cases for setupx.entity.experimentpublication table
 */
class ExperimentPublicationIntegrationTests extends GroovyTestCase{

    Logger gLogger = Logger.getLogger(ExperimentPublicationIntegrationTests.class)

    void testQuery(){

      gLogger.info('=========== Output ===========')
      def experimentPublication = ExperimentPublication.get((long) 4);
      def user = UserUtil.getInstance().getUserNameByUserId( experimentPublication.getPublishedBy())
      gLogger.info(experimentPublication.getLabel())
      gLogger.info(experimentPublication.getPublishedDate())
      gLogger.info(experimentPublication.experiment)      
      gLogger.info( user )


      gLogger.info('=========== Output ===========')

      assertTrue experimentPublication!=null
      assertTrue experimentPublication.id == (long)4
      assertTrue experimentPublication.getLabel() == 'fsa upload dataset'
      assertTrue experimentPublication.experiment.id == 29518
      assertTrue user == 'scholz'

    }
}
