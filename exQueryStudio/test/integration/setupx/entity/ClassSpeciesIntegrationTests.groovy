package setupx.entity

import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Dec 22, 2010
 * Time: 11:28:09 AM
 * Integration test cases for setupx.entity.classspecies table
 */
class ClassSpeciesIntegrationTests extends GroovyTestCase {

    Logger gLogger = Logger.getLogger(ClassSpeciesIntegrationTests.class)

    void testQuery(){
      def species = ClassSpecies.get((long) 1956120179);

      gLogger.info('=========== Output ===========')
      gLogger.info(species.getSpecies_name())
      gLogger.info(species)
      gLogger.info(species.experimentclass)
      gLogger.info('=========== Output ===========')

      assertTrue species!=null
      assertTrue species.id == (long) 1956120179
      assertTrue species.getSpecies_name() == 'Bos taurus'
      assertTrue species.experimentclass.id == (long) 20179
    }

}
