package setupx.entity

import exquery.GenericSearchService
import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: dec 22, 2010
 * Time: 6:28:01 PM
 * Integration test cases for setupx.entity.searchconfig table
 */
class SearchConfigIntegrationTests extends GroovyTestCase {

    Logger gLogger = Logger.getLogger(SearchConfigIntegrationTests.class)
    GenericSearchService genericSearchService = new GenericSearchService()

    void testQuery(){
      def res = genericSearchService.getAllSelectOptions()
      gLogger.info "res :: ${res.size()}"

      assertTrue res!=null
      assertTrue res.size() == 14
    }

}
