package taxonomy.domain

import org.apache.log4j.Logger
import taxonomy.TaxonomyNames

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/8/11
 * Time: 1:47 PM
 * To change this template use File | Settings | File Templates.
 */
class TaxonomyNamesIntegrationTests extends GroovyTestCase {

    Logger gLogger = Logger.getLogger(TaxonomyNamesIntegrationTests.class)

    void testSearchTaxonomyName(){
      String nameTxt = "potato"
      def res = TaxonomyNames.executeQuery("select tn from taxonomy.TaxonomyNames tn  where upper(tn.nameTxt) = :nameTxt",["nameTxt":nameTxt.toUpperCase()])
      gLogger.info "res :: ${res}"
      println "res :: ${res}"
      println "res :: ${res?.tax}"
      assertTrue res!=null
    }
}



