package taxonomy.domain

import org.apache.log4j.Logger
import taxonomy.TaxonomyNodes

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/9/11
 * Time: 11:15 AM
 * To change this template use File | Settings | File Templates.
 */
class TaxonomyNodesIntegrationTests extends GroovyTestCase{
    Logger gLogger = Logger.getLogger(TaxonomyNodesIntegrationTests.class)

    void testSearchNode(){

       def res = TaxonomyNodes.findById(9606L)
       println "res :: ${res}"
       println "res :: ${res.division}"
       println "res :: ${res.taxonomyNamesSet}"

       assertTrue res!=null
       assertTrue res.division.id == 5L
       assertTrue res.taxonomyNamesSet.size() == 4

   }
}
