package taxonomy.services

import grails.test.*
import org.apache.log4j.Logger
import taxonomy.NCBITaxonomyService
import setupx.entity.ClassSpecies
import exquery.util.CommonUtil

class NCBITaxonomyServiceTests extends GrailsUnitTestCase {
    Logger gLogger = Logger.getLogger(NCBITaxonomyServiceTests.class)

    NCBITaxonomyService nCBITaxonomyService
    protected void setUp(){
        super.setUp()
        nCBITaxonomyService = new NCBITaxonomyService()
    }

    protected void tearDown(){
        super.tearDown()
    }

    void testSearchTaxonomyByTaxonomyName() {
        def taxonomyName = "solanum tuberosum"
        gLogger.info "nCBITaxonomyService :: ${nCBITaxonomyService}"
        println "nCBITaxonomyService :: ${nCBITaxonomyService}"

        def taxonomyNodesObj = nCBITaxonomyService.searchTaxonomyByTaxonomyName(taxonomyName)
        println taxonomyNodesObj

        assertTrue taxonomyNodesObj != null
        assertTrue taxonomyNodesObj.size == 1L
    }

    void testSearchTaxonomyByTaxonomyNameWildCard() {
        def taxonomyName = "Homo*"
        gLogger.info "nCBITaxonomyService :: ${nCBITaxonomyService}"
        println "nCBITaxonomyService :: ${nCBITaxonomyService}"

        def taxonomyNodeObj = nCBITaxonomyService.searchTaxonomyByTaxonomyNameWithWildCards(taxonomyName)
        println taxonomyNodeObj
        println taxonomyNodeObj.size()

        assertTrue taxonomyNodeObj != null
    }

    void testSearchTaxonomyIdByTaxonomyName() {
        def taxonomyName = "solanum tuberosum"
        def taxonomyIds = nCBITaxonomyService.searchTaxonomyIdByTaxonomyName(taxonomyName)
        println taxonomyIds

        assertTrue taxonomyIds != null
        assertTrue taxonomyIds.size == 1L
    }

    void testGetAllNamesByTaxonomyIds(){
        Long taxonomyId = 4113L
        def taxonomyNames = nCBITaxonomyService.getAllNamesByTaxonomyIds(taxonomyId)
        println taxonomyNames

        assertTrue taxonomyNames != null
        assertTrue taxonomyNames.size() == 5
    }

    void testGetAllNamesByTaxonomyName(){
        def taxonomyName = "solanum tuberosum"
        def taxonomyNames = nCBITaxonomyService.getAllNamesByTaxonomyName(taxonomyName)
        println taxonomyNames
        assertTrue taxonomyNames != null
        assertTrue taxonomyNames.size() == 5
        assertTrue taxonomyNames.contains("potato")
    }

    void testSearchSpecies(){
        def taxonomyName = "potato"
        def taxonomyNames = nCBITaxonomyService.getAllNamesByTaxonomyName(taxonomyName)
        println taxonomyNames

        def classSpeciesSet = ClassSpecies.executeQuery("Select cs from setupx.entity.ClassSpecies cs where upper(cs.species_name) in (:taxonomyNames) order by cs.id", ["taxonomyNames":CommonUtil.getInstance().toUpper(taxonomyNames)])

        println classSpeciesSet
        assertTrue classSpeciesSet != null
        assertTrue classSpeciesSet.size() == 48
    }

    void testUniqueListOfSxSpecies(){
        def uniqueList = ClassSpecies.executeQuery("Select distinct cs.species_name from setupx.entity.ClassSpecies cs")
        println uniqueList
        assertTrue uniqueList!=null
        assertTrue uniqueList.size() == 7
    }

    void testConvertNameListIntoString(){
        def names = ["Homo sapiens Linnaeus, 1758","human","man"]
        def result = CommonUtil.getInstance().convertNameListIntoString(names)
        println result

        assertTrue result.toString().equals("Homo sapiens Linnaeus, 1758; human; man")
    }

    void testGetTaxonomySynonyms(){
        Long taxId = 9606L
        def result = nCBITaxonomyService.getTaxonomySynonyms(taxId)
        println result

        assertTrue result!=null
        assertTrue result.size() == 3
    }

    void testGetUniqueDivisionsByNCBIId(){
       def taxIds = [9606L]

       def result = nCBITaxonomyService.getUniqueDivisionsByNCBIIds(taxIds)
       println result

       assertTrue result!=null
       assertTrue result.size() == 1L
    }

    void testGetUniqueDivisionsByOneNCBIId(){
       def taxIds = 9606L

       def result = nCBITaxonomyService.getUniqueDivisionsByNCBIIds(taxIds)
       assertTrue result!=null
       assertTrue result.size() == 1L
    }

    void testGetUniqueDivisionsByNCBIIds(){
       def taxIds = [9606L,9347L,7776L,9605L]

       def result = nCBITaxonomyService.getUniqueDivisionsByNCBIIds(taxIds)
       assertTrue result!=null
       assertTrue result.size() == 3L
    }

    void testGetTaxonomyScientificName(){
       def taxId = 9606L
       def result = nCBITaxonomyService.getTaxonomyScientificName(taxId)
       println result

       assertTrue result!=null
       assertTrue result.toString().equals("Homo sapiens")
    }


}
