package dal

import java.sql.Connection
import java.sql.Statement
import org.apache.log4j.Logger

class DBConnectionServiceIntegrationTests extends GroovyTestCase {

    Logger logger = Logger.getLogger("DBConnectionServiceIntegrationTests")
    DBConnectionService ConnectionService = new DBConnectionService()

    void testSomething() {
      Connection con = ConnectionService.getConnection()
      logger.info con

      def query = "select * from public.experiment "
      Statement st = con.createStatement()
      def rows = st.execute(query)
      logger.info rows

      assertTrue rows == true

    }
}
