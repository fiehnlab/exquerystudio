package auth.util

import org.apache.log4j.Logger

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 6/1/11
 * Time: 1:43 PM
 * To change this template use File | Settings | File Templates.
 */
class UserUtilIntegrationTests extends GroovyTestCase {
    Logger gLogger = Logger.getLogger(UserUtilIntegrationTests.class)

    void testGetUserDetailsByUserName() {

        Long userId = 6L

        def userVO = UserUtil.getInstance().getUserDetails(userId)

        println userVO
        assertTrue userVO!=null
        assertTrue userVO.displayName == 'Oliver Fiehn'
        assertTrue userVO.experimentCount == 10L
        assertTrue userVO.classesCount == 146L
        assertTrue userVO.samplesCount == 2040L
    }

    void testGetUserDetailsByWrongUserName() {
        String userName = "xyz"

        def userVO = UserUtil.getInstance().getUserDetails(userName)

        println userVO
        assertTrue userVO==null
    }
}
