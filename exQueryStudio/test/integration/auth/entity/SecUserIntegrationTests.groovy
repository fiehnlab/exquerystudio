package auth.entity

import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Dec 22, 2010
 * Time: 10:54:23 AM
 * Integration test cases for auth.entity.secuser table
 */
class SecUserIntegrationTests extends GroovyTestCase {

    Logger gLogger = Logger.getLogger(SecUserIntegrationTests.class)

    void testQuery(){

        gLogger.info('=========== testQuery ===========')

        def user = SecUser.get((long) 6);

        gLogger.info('=========== Output ===========')
        gLogger.info(user.id)
        gLogger.info(user.getUsername())
        gLogger.info(user.experimentUserRights)
        gLogger.info(user.experimentUserRights.size())
        gLogger.info('=========== Output ===========')

        assertTrue user!=null
        assertTrue user.id == (long) 6
        assertTrue user.getUsername() == 'fiehn'

        // here user belongs to 10 experiments
        assertTrue user.experimentUserRights.size() == 10
    }
}
