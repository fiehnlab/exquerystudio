package exquery.util.common

import org.apache.log4j.Logger
import exquery.common.OrganUtil

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 5/10/11
 * Time: 1:54 PM
 * To change this template use File | Settings | File Templates.
 */
class OrganUtilTests extends GroovyTestCase {
    Logger gLogger = Logger.getLogger(OrganUtilTests.class)

    void testGetOrganDataByOrganName() {
        String organName = "rosette leaf"

        def result = OrganUtil.getInstance().getOrganDataByOrganName(organName)

        //println result
        assertTrue result!=null
        assertTrue result?.experimentCount == 2L
        assertTrue result?.classesCount == 21L
        assertTrue result?.samplesCount == 126L
    }

    void testGetUniqueSxOrgans(){
        def result = OrganUtil.getInstance().getUniqueSxOrgans()

        assertTrue result!=null
        assertTrue result.size()==7L
    }
}
