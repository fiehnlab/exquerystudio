package exquery.util.common

import org.apache.log4j.Logger
import exquery.common.SpeciesUtil
import exquery.valuebeans.export.SpeciesVO

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 5/6/11
 * Time: 2:55 PM
 * To change this template use File | Settings | File Templates.
 */
class SpeciesUtilTests extends GroovyTestCase {
    Logger gLogger = Logger.getLogger(SpeciesUtilTests.class)

    void testSXSpeciesBySpeciesName() {
      String speciesName="Vitis vinifera"
      SpeciesVO speciesVO = SpeciesUtil.getInstance().getSXSpeciesBySpeciesName(speciesName)
      //println speciesVO
      //println speciesVO.classesCount

      assertTrue speciesVO!=null
      assertTrue speciesVO?.experimentCount == 1L
      assertTrue speciesVO?.classesCount == 17L
      assertTrue speciesVO?.samplesCount == 102L
    }

    void testGetUniqueSxSpecies(){
        def result = SpeciesUtil.getInstance().getUniqueSxSpecies()

        assertTrue result!=null
        assertTrue result.size()==7L
    }
}
