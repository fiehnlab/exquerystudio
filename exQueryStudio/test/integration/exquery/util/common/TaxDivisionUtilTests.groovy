package exquery.util.common

import org.apache.log4j.Logger
import exquery.common.TaxDivisionUtil

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 5/10/11
 * Time: 2:06 PM
 * To change this template use File | Settings | File Templates.
 */
class TaxDivisionUtilTests extends GroovyTestCase {
    Logger gLogger = Logger.getLogger(TaxDivisionUtilTests.class)

    void testGetUniqueDivisions(){
        def result = TaxDivisionUtil.getInstance().getUniqueDivisions()

        assertTrue result!=null
        assertTrue result.size()==4L
    }
}
