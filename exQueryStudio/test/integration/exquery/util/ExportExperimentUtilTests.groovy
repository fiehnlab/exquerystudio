package exquery.util

import org.apache.log4j.Logger
import setupx.entity.Experiment

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/18/11
 * Time: 3:18 PM
 * To change this template use File | Settings | File Templates.
 */
class ExportExperimentUtilTests extends GroovyTestCase {
    Logger gLogger = Logger.getLogger(ExportExperimentUtilTests.class)

    void testExportExperiment() {
      Long experimentId = 115958L
      String outputFile = "./test/exceltest/${experimentId}.xls";
      def result = Experiment.findAllById(experimentId)
      def res = ExportExperimentUtil.getInstance().exportExperiment(result)
      println res

      // The Output file is where the xls will be created
      FileOutputStream fOut = new FileOutputStream(outputFile);
      // Write the XL sheet
      res.write(fOut);
      fOut.flush();
      // Done Deal..
      fOut.close();
    }

    void testExportExperimentByIds() {
      String outputFile = "./test/exceltest/All.xls";
      def result = Experiment.findAllByTitleLike("%Samples%")
      def res = ExportExperimentUtil.getInstance().exportExperiment(result)
      println res

      // The Output file is where the xls will be created
      FileOutputStream fOut = new FileOutputStream(outputFile);
      // Write the XL sheet
      res.write(fOut);
      fOut.flush();
      // Done Deal..
      fOut.close();
    }

    void testExportExperimentByWrongIds() {
      String outputFile = "./test/exceltest/NoData.xls";
      def result = Experiment.findAllByTitleLike("*Samples*")
      def res = ExportExperimentUtil.getInstance().exportExperiment(result)
      println res

      // The Output file is where the xls will be created
      FileOutputStream fOut = new FileOutputStream(outputFile);
      // Write the XL sheet
      res.write(fOut);
      fOut.flush();
      // Done Deal..
      fOut.close();
    }

}
