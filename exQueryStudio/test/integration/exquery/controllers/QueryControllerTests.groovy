package exquery.controllers

import exquery.QueryController
import exquery.LookupService

class QueryControllerTests extends GroovyTestCase {

  QueryController controller

  protected void setUp(){
      super.setUp()

      controller = new QueryController()
      controller.lookupService = new LookupService()

  }

  protected void tearDown() {
    super.tearDown()
  }

  void testSamplesExtractionByExperimentIds() {
      String ids = "331566,421340,123456";
      String idType = "experiments";

      def params = [txtIds: ids, rdType: idType]

      QueryController.metaClass.getParams{-> params}

      def results = controller.samples_result()

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate != true)
      assertTrue(results?.result?.TotalRecordCount == 242) // 241- Samples in 2 experiments and 1- experiment not matched
  }

  void testSamplesExtractionByClassIds() {
      String ids = "20521,20204,28064,328204,343726";
      String idType = "classes";

      def params = [txtIds: ids, rdType: idType]

      QueryController.metaClass.getParams{-> params}

      def results = controller.samples_result()

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate != true)
      assertTrue(results?.result?.TotalRecordCount == 47) // 46- Samples in 4 classes and 1- class not matched
  }

  void testSamplesExtractionBySampleIds() {
      String ids = "20321,20204,12345,38293";
      String idType = "samples";

      def params = [txtIds: ids, rdType: idType]

      QueryController.metaClass.getParams{-> params}

      def results = controller.samples_result()

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate != true)
      assertTrue(results?.result?.TotalRecordCount == 4)
  }

  void testSamplesExtractionByChromatof() {
      String ids = "070503byusa54_1,070503byusa67_1,060610bfisa32_1,060610bfisa32_2,4159eg21_1,4159eg21_2";
      String idType = "chromatofs";

      def params = [txtIds: ids, rdType: idType]

      QueryController.metaClass.getParams{-> params}

      def results = controller.samples_result()

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate != true)
      assertTrue(results?.result?.TotalRecordCount == 6) // 4- matched and 2- unmatched
  }

}
