package exquery.controllers

import exquery.GenericSearchController
import exquery.GenericSearchService

class GenericSearchControllerTests extends GroovyTestCase {

  GenericSearchController controller

  protected void setUp(){
      super.setUp()

      controller = new GenericSearchController()
      controller.genericSearchService = new GenericSearchService()
  }

  protected void tearDown(){
      super.tearDown()
  }

  void testGenericSearch() {

      def params = ["cmbType1":"asc", "countOrderByClause":"2", "cmbOrderByOption2":"E.experiment_abstract",
              "txtSearchData2":"*Clorox*", "countWhereClause":"2", "cmbCriteria1":"E.title", "cmbOrderByOption1":"E.title",
              "txtSearchData1":"*Citrus Healthy*", "cmbCriteria2":"E.experiment_abstract", "cmbOption2":"OR",
              "Select":["E.title", "E.experiment_abstract", "E.description"], "cmbOperation1":"like", "cmbOperation2":"like", "cmbType2":"desc"]

      GenericSearchController.metaClass.getParams{-> params}

      def response = controller.process()

      println "response :: ${response}"
      println "resultList = ${response?.result}"
      println "size :: ${response?.result?.resultList?.size()}"

      assertTrue response != null
      assertTrue response?.result != null
      assertTrue response?.result?.resultList?.size() == 3
    
  }

}
