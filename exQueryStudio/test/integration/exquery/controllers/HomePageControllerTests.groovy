package exquery.controllers

import exquery.HomePageController
import exquery.IELogService

class HomePageControllerTests extends GroovyTestCase {

  HomePageController controller

  protected void setUp(){
      super.setUp()
      controller = new HomePageController()
  }

  protected void tearDown(){
      super.tearDown()
  }

  void testIndex(){

      def response = controller.index()
//      println response.PrototypeInfo.experimentSize
//      println response.PrototypeInfo.classesSize
//      println response.PrototypeInfo.samplesSize

//      println response.sxObjectType.size()
//      println response.getSXSearchObjectType.size()

      assertTrue response.PrototypeInfo.experimentSize == 10
      assertTrue response.PrototypeInfo.classesSize == 146
      assertTrue response.PrototypeInfo.samplesSize == 2040

      assertTrue response.sxObjectType.size() == 4
      assertTrue response.getSXSearchObjectType.size() == 7
  }

}
