package exquery.controllers

import exquery.SpeciesController
import exquery.SpeciesService
import exquery.ExperimentService
import exquery.ExperimentClassService
import exquery.ClassSampleService
import de.andreasschmitt.export.ExportService
/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/27/11
 * Time: 10:30 AM
 * Test cases for SpeciesController
 */

class SpeciesControllerTests extends GroovyTestCase {

  SpeciesController controller

  protected void setUp() {
    super.setUp()

    controller = new SpeciesController()
    controller.speciesService = new SpeciesService()
    controller.experimentService = new ExperimentService()

    controller.experimentClassService = new ExperimentClassService()
    controller.classSampleService = new ClassSampleService()
    controller.exportService = new ExportService()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testDatalist() {

      def params = [lookupBy:"classes", speciesName:"Homo sapiens"]

      SpeciesController.metaClass.getParams{-> params}
      def results = controller.datalist()

      //println results

      assertTrue results!=null
      assertTrue results.speciesName == 'Homo sapiens'
      assertTrue results.title == "Class List By Species 'Homo sapiens'"
      assertTrue results.result.TotalRecordCount == 2L
  }

}
