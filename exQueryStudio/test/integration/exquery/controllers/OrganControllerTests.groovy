package exquery.controllers

import exquery.OrganController
import exquery.SpeciesService
import exquery.OrgansService
import de.andreasschmitt.export.ExportService
import exquery.ClassSampleService
import exquery.ExperimentClassService
import exquery.ExperimentService

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/27/11
 * Time: 10:29 AM
 * Test cases for OrganController
 */
class OrganControllerTests extends GroovyTestCase {

  OrganController controller

  protected void setUp() {
    super.setUp()

    controller = new OrganController()
    controller.organsService = new OrgansService()
    controller.experimentService = new ExperimentService()

    controller.experimentClassService = new ExperimentClassService()
    controller.classSampleService = new ClassSampleService()
    controller.exportService = new ExportService()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testDatalist() {

      def params = [lookupBy:"experiments", organName:"rosette leaf"]

      OrganController.metaClass.getParams{-> params}
      def results = controller.datalist()

      //println results

      assertTrue results!=null
      assertTrue results.organName == 'rosette leaf'
      assertTrue results.title == "Experiment List By Organ 'rosette leaf'"
      assertTrue results.result.TotalRecordCount == 2L
  }
}
