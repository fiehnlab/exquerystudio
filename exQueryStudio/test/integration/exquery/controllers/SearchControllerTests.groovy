package exquery.controllers

import exquery.SearchController
import exquery.SearchService
import exquery.LookupService
import exquery.SpeciesService
import taxonomy.NCBITaxonomyService
import exquery.ExperimentService
import exquery.ClassSampleService
import exquery.OrgansService
import exquery.valuebeans.export.OrganVO

class SearchControllerTests extends GroovyTestCase {

  SearchController controller

  protected void setUp() {
    super.setUp()

    controller = new SearchController()
    controller.searchService = new SearchService()
    controller.searchService.lookupService = new LookupService()

    controller.searchService.lookupService.speciesService = new SpeciesService()
    controller.searchService.lookupService.speciesService.taxonomyService = new NCBITaxonomyService()
    controller.searchService.lookupService.NCBITaxonomyService = new NCBITaxonomyService()
    controller.searchService.lookupService.organsService = new OrgansService()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testSearchByExperimentId() {
      def params = [cmbSearchBy:"Experiments",searchText:"19778"]

      SearchController.metaClass.getParams{-> params}
      def results = controller.search()

//      println results?.result
//      println results?.result?.isPaginate
//      println results?.result?.TotalRecordCount

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate != true)
      assertTrue(results?.result?.TotalRecordCount == 1)

  }

  void testSearchByClassId() {
      def params = [cmbSearchBy:"Classes",searchText:"328054"]

      SearchController.metaClass.getParams{-> params}
      def results = controller.search()

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate != true)
      assertTrue(results?.result?.TotalRecordCount == 1)

  }

  void testSearchByClassText() {
      def params = [cmbSearchBy:"Classes",searchText:"Taipei*"]

      SearchController.metaClass.getParams{-> params}
      def results = controller.search()

      //println results?.result
      //println results?.result?.isPaginate
      //println results?.result?.TotalRecordCount

      assertTrue(results?.result != null)
      assertTrue(results?.result?.isPaginate == true)
      assertTrue(results?.result?.TotalRecordCount == 7)

  }

  void testSearchBySamplesId() {
      def params = [cmbSearchBy:"Samples",searchText:"327966"]

      SearchController.metaClass.getParams{-> params}
      def results = controller.search()

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate != true)
      assertTrue(results?.result?.TotalRecordCount == 1)

  }

  void testSearchBySamplesText() {
      def params = [cmbSearchBy:"Samples",searchText:"sx_5*"]

      SearchController.metaClass.getParams{-> params}
      def results = controller.search()

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate == true)
      assertTrue(results?.result?.TotalRecordCount == 234)

  }

  void testSearchBySpeciesId() {
      def params = [cmbSearchBy:"Species",searchText:"9606"]

      SearchController.metaClass.getParams{-> params}
      def results = controller.search()

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate != true)
      assertTrue(results?.result?.TotalRecordCount == 1)

  }

  void testSearchBySpeciesText() {
      def params = [cmbSearchBy:"Species",searchText:"Mus sp."]

      SearchController.metaClass.getParams{-> params}
      def results = controller.search()

      println results?.result
      println results?.result?.isPaginate
      println results?.result?.TotalRecordCount

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.TotalRecordCount == 1)

  }

  void testSearchByOrgansId() {
      def params = [cmbSearchBy:"Organs",searchText:"115704"]

      SearchController.metaClass.getParams{-> params}
      def results = controller.search()

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate != true)
      assertTrue(results?.result?.isExport == true)
      assertTrue(results?.result?.TotalRecordCount == 1)

  }

  void testSearchByOrgansText() {
      def params = [cmbSearchBy:"Organs",searchText:"*leaf"]

      SearchController.metaClass.getParams{-> params}
      def results = controller.search()

      println results?.result
      println results?.result?.TotalRecordCount

      assertTrue(results?.result !=null)
      assertTrue(results?.result?.isPaginate != true)
      assertTrue(results?.result?.isExport == true)
      assertTrue(results?.result?.TotalRecordCount == 2L)

  }


}
