package exquery.services

import exquery.SearchService
import org.apache.log4j.Logger
import exquery.SpeciesService
import exquery.LookupService
import taxonomy.NCBITaxonomyService
import exquery.OrgansService
import exquery.UserService

/**
 * User: pradeep
 * Date: Dec 22, 2010
 * Time: 11:15:16 AM
 * Test cases for SearchService class
 */
class SearchServiceTests extends GroovyTestCase {

  Logger gLogger = Logger.getLogger(SearchServiceTests.class)

  SearchService searchService

  protected void setUp() {
    super.setUp()
    searchService = new SearchService()
    searchService.lookupService = new LookupService()
    searchService.lookupService.NCBITaxonomyService = new NCBITaxonomyService()
    searchService.lookupService.speciesService = new SpeciesService()
    searchService.lookupService.speciesService.taxonomyService = new NCBITaxonomyService()
    searchService.lookupService.organsService = new OrgansService()
    searchService.lookupService.userService = new UserService()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testSearchExperiments() {
      def searchBy = "Experiments" 
      def searchText = "*rice*"
      gLogger.info "searchService :: ${searchService}"

      def e = searchService.search(searchBy,searchText)

      gLogger.info e
      gLogger.info e.result
      gLogger.info e.TotalRecordCount  
      
      assertTrue e!=null
      assertTrue e.TotalRecordCount == 1
 }


  void testSearchClasses() {
      def searchBy = "Classes" 
      def searchText = "Agria*"

      def e = searchService.search(searchBy,searchText)
      gLogger.info e.TotalRecordCount

      assertTrue e!=null
      assertTrue e.TotalRecordCount == 4
  }

  void testSearchSamples() {
      def searchBy = "Samples"
      def searchText = "*sx22*"

      def e = searchService.search(searchBy,searchText)
      println e.TotalRecordCount

      assertTrue e!=null
      assertTrue e.TotalRecordCount == 93
  }

  void testSearchSpecies() {
      def searchBy = "Species"
      def searchText = "solanum tuberosum"

      def e = searchService.search(searchBy,searchText)
      gLogger.info e.TotalRecordCount

      assertTrue e!=null
      assertTrue e.TotalRecordCount == 1
  }

  void testSearchOrgans() {
      def searchBy = "Organs"
      def searchText = "*leaf*"

      def e = searchService.search(searchBy,searchText)
      gLogger.info e.TotalRecordCount

      assertTrue e!=null
      assertTrue e.TotalRecordCount == 2L
  }

  void testSearchAll() {
      def searchBy = "SearchAll"
      def searchText = "*leaf*"

      def e = searchService.search(searchBy,searchText)
      println e.TotalRecordCount

      assertTrue e!=null
      assertTrue e.TotalRecordCount == 3L
  }

}
