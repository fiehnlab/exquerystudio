package exquery.services.webservices

import webservices.exquery.ExperimentWebService
import org.apache.log4j.Logger

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 6/6/11
 * Time: 10:40 AM
 * To change this template use File | Settings | File Templates.
 */
class ExperimentWebServiceTests extends GroovyTestCase {
    Logger gLogger = Logger.getLogger(ExperimentWebServiceTests.class)
    ExperimentWebService experimentWebService

    protected void setUp() {
      super.setUp()
      experimentWebService = new ExperimentWebService()
    }

    protected void tearDown(){
        super.tearDown()
    }


    void testExperimentMetaDataXMLByExperimentId(){
        def experimentId = 115958L
        def result = experimentWebService.experimentMetaDataXMLByExperimentId(experimentId)

        println result

        assertTrue result!=null
        assertTrue result.toString().contains("Fatb Induction Experiment (FatBIE)")
    }

    void testExperimentMetaDataXMLByExperimentId_WrongId(){
        def experimentId = 108L
        def result = experimentWebService.experimentMetaDataXMLByExperimentId(experimentId)

        println result

        assertTrue result!=null
    }

    void testExperimentMetaDataXMLByClassId(){
        def classId = 115837L
        def result = experimentWebService.experimentMetaDataXMLByClassId(classId)

        println result

        assertTrue result!=null
        assertTrue result.toString().contains("Fatb Induction Experiment (FatBIE)")
    }

    void testExperimentMetaDataXMLBySampleId(){
        def sampleId = 115811L
        def result = experimentWebService.experimentMetaDataXMLBySampleId(sampleId)

        println result

        assertTrue result!=null
        assertTrue result.toString().contains("Fatb Induction Experiment (FatBIE)")
    }

}
