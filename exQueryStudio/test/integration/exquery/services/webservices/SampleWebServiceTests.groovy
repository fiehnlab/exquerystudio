package exquery.services.webservices

import webservices.exquery.SampleWebService
import org.apache.log4j.Logger
import exquery.ClassSampleService

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 6/6/11
 * Time: 12:11 PM
 * To change this template use File | Settings | File Templates.
 */
class SampleWebServiceTests extends GroovyTestCase {
    Logger gLogger = Logger.getLogger(ExperimentWebServiceTests.class)
    SampleWebService service

    protected void setUp() {
      super.setUp()
      service = new SampleWebService()
      service.classSampleService = new ClassSampleService()
    }

    protected void tearDown(){
        super.tearDown()
    }


    void testGetSampleIdBySampleLabel(){
        def sampleLabel = "sx985"
        def result = service.getSampleIdBySampleLabel(sampleLabel)

        println result

        assertTrue result!=null
        assertTrue result.toString().equals("20199")
    }

    void testGetSampleIdByWrongSampleLabel(){
        def sampleLabel = "xyz985"
        def result = service.getSampleIdBySampleLabel(sampleLabel)

        println result
        assertTrue result==null
    }

}
