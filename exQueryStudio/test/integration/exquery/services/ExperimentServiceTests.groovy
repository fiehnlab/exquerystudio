package exquery.services

import org.apache.log4j.Logger
import exquery.ExperimentService

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/26/11
 * Time: 3:23 PM
 * To change this template use File | Settings | File Templates.
 */
class ExperimentServiceTests extends GroovyTestCase {
    Logger gLogger = Logger.getLogger(ExperimentServiceTests.class)
    def experimentService

    protected void setUp() {
      super.setUp()
      experimentService = new ExperimentService()
    }

    protected void tearDown() {
      super.tearDown()
    }

    void testGetExperimentsByExperimentIds(){
        def ids = [19778L,115958L,343581L,101L]
        def results = experimentService.getExperimentsByExperimentIds(ids)
        println results

        assertTrue results!=null
        assertTrue results?.size == 3L
    }

    void testGetExperimentsByExperimentId(){
        def id = 19778L
        def results = experimentService.getExperimentsByExperimentIds(id)
        println results

        assertTrue results!=null
        assertTrue results?.size == 1L
    }

    void testGetAllExperimentsByUserId(){
        def userId = 6L
        def results = experimentService.getAllExperimentsByUserId(userId)
        println results
        assertTrue results!=null
        assertTrue results?.size == 10L
    }

    void testGetExperimentIdsByClassIds(){
        def ids = [28343L,103905L,28467L]
        def results = experimentService.getExperimentIdsByClassIds(ids)
        println results
        assertTrue results!=null
        assertTrue results?.size == 2L
    }

    void testGetExperimentIdsByClassId(){
        def id = 28343L
        def results = experimentService.getExperimentIdsByClassIds(id)
        println results
        assertTrue results!=null
        assertTrue results?.size == 1L
    }

    void testGetExperimentsByClassIds(){
        def id = [28343L,103905L,28467L]
        def results = experimentService.getExperimentsByClassIds(id)
        println results
        assertTrue results!=null
        assertTrue results?.size == 2L
    }

    void testGetExperimentsByClassId(){
        def id = 28343L
        def results = experimentService.getExperimentsByClassIds(id)
        println results
        assertTrue results!=null
        assertTrue results?.size == 1L
    }
}
