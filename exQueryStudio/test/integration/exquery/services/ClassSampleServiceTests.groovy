package exquery.services

import exquery.ClassSampleService
import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Dec 20, 2010
 * Time: 2:44:41 PM
 * Test cases for classSample Service class
 */
class ClassSampleServiceTests extends GroovyTestCase {
    Logger gLogger = Logger.getLogger(ClassSampleServiceTests.class)
    ClassSampleService classSampleService

    protected void setUp() {
      super.setUp()
      classSampleService = new ClassSampleService()
    }

    protected void tearDown(){
        super.tearDown()
    }

    void testGetClassSampleIdBySampleLabel() {
        String sampleLabel = "sx2042"

        String sampleId = classSampleService.getClassSampleIdBySampleLabel(sampleLabel)

        assertTrue sampleId == "37408"
    }

    void testGetAllSampleIdsByClassId(){
        def classId = 20210L

        def result = classSampleService.getAllSampleIdsByClassIds(classId)
        //println result

        assertTrue result!=null
        assertTrue result?.size() == 9L
    }

    void testGetAllSampleIdsByClassIds(){
        def classIds = [20210L,328104L]

        def result = classSampleService.getAllSampleIdsByClassIds(classIds)
        //println result

        assertTrue result!=null
        assertTrue result?.size() == 15L
    }

    void testGetSampleIdsByClassId(){
        def classId = 20210L

        def result = classSampleService.getSampleIdsByClassIds(classId)
        //println result

        assertTrue result!=null
        assertTrue result?.size() == 9L
    }

    void testGetSampleIdsByClassIds(){
        def classIds = [20210L,328104L]

        def result = classSampleService.getSampleIdsByClassIds(classIds)
        //println result

        assertTrue result!=null
        assertTrue result?.size() == 15L
    }

    void testGetSamplesByClassId(){
        def classId = 20210L

        def result = classSampleService.getSamplesByClassIds(classId)
        //println result

        assertTrue result!=null
        assertTrue result?.size() == 9L
    }

    void testGetSamplesByClassIds(){
        def classIds = [20210L,328104L]

        def result = classSampleService.getSamplesByClassIds(classIds)
        //println result

        assertTrue result!=null
        assertTrue result?.size() == 15L
    }

    void testSamplesCountByClassId(){
        def classId = 20210L

        def result = classSampleService.getSamplesCountByClassIds(classId)
        //println result

        assertTrue result!=null
        assertTrue result == 9L
    }

    void testSamplesCountByClassIds(){
        def classIds = [20210L,328104L]

        def result = classSampleService.getSamplesCountByClassIds(classIds)
        //println result

        assertTrue result!=null
        assertTrue result == 15L
    }

    void testSamplesCountByExperimentId(){
        def experimentId = 327934L

        def result = classSampleService.getSamplesCountByExperimentIds(experimentId)
        //println result

        assertTrue result!=null
        assertTrue result == 102L
    }

    void testSamplesCountByExperimentIds(){
        def experimentIds = [327934L,343581L]

        def result = classSampleService.getSamplesCountByExperimentIds(experimentIds)
        //println result

        assertTrue result!=null
        assertTrue result == 192L
    }

    void testGetSamplesBySamplesId(){
        def sampleId = 20153L

        def result = classSampleService.getSamplesBySamplesIds(sampleId)
        println result

        assertTrue result!=null
        assertTrue result?.size() == 1L

    }

    void testGetSamplesBySamplesIds(){
        def sampleIds = [20153L,20199L,20561L,35588L,35763L,12345L]

        def result = classSampleService.getSamplesBySamplesIds(sampleIds)
        println result

        assertTrue result!=null
        assertTrue result?.size() == 5L

    }


}
