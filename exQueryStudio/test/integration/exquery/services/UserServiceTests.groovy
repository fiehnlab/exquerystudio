package exquery.services

import grails.test.*
import exquery.valuebeans.export.UserVO
import exquery.UserService
import org.apache.log4j.Logger

class UserServiceTests extends GrailsUnitTestCase {
    def userService
    Logger gLogger = Logger.getLogger(UserServiceTests.class)

    protected void setUp() {
      super.setUp()
      userService = new UserService()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testGetUserById() {
        Long userId = 6L

        UserVO userVO = userService.getUserById(userId)

        println(userVO)
        assertTrue userVO!=null
        assertTrue userVO.displayName == 'Oliver Fiehn'
        assertTrue userVO.experimentCount == 10L
        assertTrue userVO.classesCount == 146L
        assertTrue userVO.samplesCount == 2040L

    }

    void testGetAllUsers(){

        def result = userService.getAllUsers()

        println(result)
        println(result.size())

        assertTrue result!=null
        assertTrue result.size() == 21L
    }

    void testSearchUser(){

        String userName="Gert Wohlgemuth"
        def result = userService.searchUser(userName)

        println(result)
        println(result.size())

        assertTrue result!=null
        assertTrue result.size() == 1L

    }
}
