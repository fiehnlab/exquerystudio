package exquery.services

import grails.test.*
import exquery.SpeciesService
import taxonomy.NCBITaxonomyService

class SpeciesServiceTests extends GrailsUnitTestCase {

    def speciesService

    protected void setUp() {
        super.setUp()
        speciesService = new SpeciesService()
        speciesService.taxonomyService = new NCBITaxonomyService()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testGetClassIdsBySpeciesName() {
        String speciesName = "Homo sapiens"

        def result = speciesService.getClassIdsBySpeciesName(speciesName)
        println result

        assertTrue result.size == 2
    }

    void testGetExperimentIdsBySpeciesName() {
        String speciesName = "Homo sapiens"

        def result = speciesService.getExperimentIdsBySpeciesName(speciesName)
        println result

        assertTrue result != null
        assertTrue result.size == 1
    }

    void testGetSampleIdsBySpeciesName() {
        String speciesName = "Homo sapiens"

        def result = speciesService.getSampleIdsBySpeciesName(speciesName)
        println result

        assertTrue result != null
        assertTrue result.size == 12
    }

    void testGetUniqueSxSpecies(){
        def result = speciesService.getUniqueSxSpecies()
        println result

        assertTrue result != null
        assertTrue result.size == 7
    }

    void testGetUniqueSxSpeciesNCBIIds(){
        def result = speciesService.getUniqueSxSpeciesNCBIIds()
        println result

        assertTrue result != null
        assertTrue result.size == 7
    }

    void testGetOrgansBySpeciesName(){
        String speciesName = "mice"

        def result = speciesService.getOrgansBySpeciesName(speciesName)
        println result
        assertTrue result != null
        assertTrue result.size == 1
    }

    void testGetSxTaxonomyByDivisionId(){
        def divisionId = 5L
        def result = speciesService.getSxTaxonomyByDivisionIds(divisionId)
        println result

        assertTrue result!=null
        assertTrue result.size()==1L
    }

    void testGetSxTaxonomyByDivisionIds(){
        def divisionIds = [5L,4L,7L]
        def result = speciesService.getSxTaxonomyByDivisionIds(divisionIds)
        println result

        assertTrue result!=null
        assertTrue result.size()==5L
    }

    void testGetUniqueDivisions(){
        def result = speciesService.getUniqueDivisions()

        println result
        assertTrue result!=null
        assertTrue result.size()==4L
    }
}
