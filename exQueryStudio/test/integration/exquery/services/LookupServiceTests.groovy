package exquery.services

import exquery.LookupService
import org.apache.log4j.Logger
import setupx.entity.*

import taxonomy.NCBITaxonomyService
import exquery.SpeciesService

import exquery.valuebeans.export.SpeciesVO
import exquery.valuebeans.export.OrganVO
import exquery.OrgansService
import exquery.UserService
import exquery.valuebeans.export.UserVO

/**
 * User: pradeep
 * Date: Dec 22, 2010
 * Time: 11:44:19 AM
 * Test cases for LookupService class
 */
class LookupServiceTests extends GroovyTestCase {

    Logger gLogger = Logger.getLogger(LookupServiceTests.class)
    LookupService lookupService

    protected void setUp() {
    super.setUp()
    lookupService = new LookupService()
    lookupService.NCBITaxonomyService = new NCBITaxonomyService()
    lookupService.speciesService = new SpeciesService()
    lookupService.speciesService.taxonomyService = new NCBITaxonomyService()
    lookupService.organsService = new OrgansService()
    lookupService.userService = new UserService()
  }

  protected void tearDown() {
    super.tearDown()
  }


   /**
    * Experiment text cases : Start
    */
    void testSearchExperimentById() {
      Experiment e = lookupService.searchExperimentById(29518,null)

      gLogger.info e

      assertTrue e!=null
      assertTrue e.id == (long)29518
      assertTrue e.getTitle() == 'FSA Potato 2003'
    }

   void testGetAllExperiments() {
      def e = lookupService.getAllExperiments()

      assertTrue e!=null
      assertTrue e.size() == 10
    }

   void testSearchExperiments() {
      def e = lookupService.searchExperiments('*Extraction*')

      gLogger.info   e.size()
      assertTrue e!=null
      assertTrue e.size() == 1
    }
  /**
    * Experiment text cases : End
    */


  /**
    * ExperimentClass text cases : Start
    */
    void testSearchExperimentClassById() {
      ExperimentClass ec = lookupService.searchExperimentClassById(28033,null)

      assertTrue ec!=null
      assertTrue ec.id == (long)28033
      assertTrue ec.experiment.id == (long) 29518
      assertTrue ec.getGenotype() == 'Agria'
    }

   void testGetAllExperimentClasses() {
      def e = lookupService.getAllExperimentClasses()
      gLogger.info  "testGetAllExperimentClasses: ${e.size()}"
      assertTrue e!=null
      assertTrue e.size() == 146
    }

   void testSearchExperimentClasses() {
      def e = lookupService.searchExperimentClasses('Granola*')

      gLogger.info  "testSearchExperimentClasses: ${e.size()}"
      assertTrue e!=null
      assertTrue e.size() == 4
    }
  /**
    * ExperimentClass text cases : End
    */

  /**
    * ClassSample text cases : Start
    */
    void testSearchSampleById() {
      ClassSample s = lookupService.searchSampleById(20153,null)

      gLogger.info s?.getSampleMetaData()

      assertTrue s!=null
      assertTrue s.id == (long)20153
      assertTrue s.getSampleMetaData() == null
      assertTrue s.experimentclass.id == (long)20179
    }

   void testGetAllSamples() {
      def e = lookupService.getAllSamples()
      gLogger.info  "testGetAllSamples: ${e.size()}"
      assertTrue e!=null
      assertTrue e.size() == 2040
    }

   void testSearchSamples() {
      def e = lookupService.searchSamples('sx227*')
      gLogger.info  "testSearchSamples: ${e.size()}"
      assertTrue e!=null
      assertTrue e.size() == 9
    }
  /**
    * ClassSample text cases : End
    */


  /**
    * ClassSpecies text cases : Start
    */
    void testSpeciesById() {
      SpeciesVO speciesVO = lookupService.searchSpeciesById(9606,null)

      assertTrue speciesVO!=null
      assertTrue speciesVO.ncbiId == (long)9606
      assertTrue speciesVO.classesCount == 2L
      assertTrue speciesVO.experimentCount == 1L
      assertTrue speciesVO.samplesCount == 12L
      assertTrue speciesVO.scientificName == 'Homo sapiens'
    }

   void testGetAllSpecies() {
      def e = lookupService.getAllSpecies()
      println "e.size() :: ${e.size()}"
      assertTrue e!=null
      assertTrue e.size() == 7L
    }

   void testSearchSpecies() {
      def e = lookupService.searchSpecies('Arabidopsis*')
      gLogger.info  "testSearchSpecies: ${e.size()}"

      assertTrue e!=null
      assertTrue e.size()==1
      assertTrue e.get(0)!=null

      SpeciesVO speciesVO = e.get(0)

      assertTrue speciesVO.ncbiId == 3702L
      assertTrue speciesVO.experimentCount == 3L
      assertTrue speciesVO.classesCount == 36L
      assertTrue speciesVO.samplesCount == 216L
      assertTrue speciesVO.scientificName == 'Arabidopsis thaliana'
    }
  /**
    * ClassSpecies text cases : End
    */

  /**
    * ClassOrgan text cases : Start
    */
    void testOrganById() {
      OrganVO organVO = lookupService.searchOrganById(115704,null)
      println organVO
      println organVO.getExperimentCount()
      println organVO.getSpecies()
      println organVO.getClassesCount()
      println organVO.getSamplesCount()

      assertTrue organVO!=null
      assertTrue organVO.experimentCount == 2L
      assertTrue organVO.classesCount == 21L
      assertTrue organVO.samplesCount == 126L
    }

   void testGetAllOrgans() {
      def co = lookupService.getAllOrgans()

      assertTrue co!=null
      assertTrue co.size() == 7L
    }

   void testSearchOrgans() {
      def co = lookupService.searchOrgans('*leaf')
      gLogger.info  "testSearchOrgans: ${co.size()}"
      assertTrue co!=null
      assertTrue co.size() == 2L
    }
  /**
    * ClassOrgan text cases : End
    */

   void testGetSamplesById(){
     def ids = [(long)20153,(long)36263,(long)36408]

     def sampleList = lookupService.getSamplesById(ids)
     gLogger.info  "sampleList: ${sampleList.size()}"
     assertTrue sampleList!=null
     assertTrue sampleList.size() == 3
   }

  void testGetclasssById(){
     def ids = [(long)20179,(long)20210,(long)20337]

     def classList = lookupService.getClassesById(ids)
     gLogger.info  "classList: ${classList.size()}"
     assertTrue classList!=null
     assertTrue classList.size() == 2
   }

  void testGetExperimentsById(){
     def ids = [(long)19778,(long)29518,(long)104274]

     def experimentList = lookupService.getExperimentsById(ids)
     gLogger.info  "experimentList: ${experimentList.size()}"
     assertTrue experimentList!=null
     assertTrue experimentList.size() == 3
  }

  void testGetUserById() {
        Long userId = 6L

        UserVO userVO = lookupService.searchUserById(userId)

        println(userVO)

        assertTrue userVO!=null
        assertTrue userVO.displayName == 'Oliver Fiehn'
        assertTrue userVO.experimentCount == 10L
        assertTrue userVO.classesCount == 146L
        assertTrue userVO.samplesCount == 2040L

    }

    void testGetAllUsers(){

        def result = lookupService.getAllUsers()

        println(result)
        println(result.size())

        assertTrue result!=null
        assertTrue result.size() == 21L
    }

    void testSearchUser(){

        String userName="*gert*"
        def result = lookupService.searchUser(userName)

        println(result)
        println(result.size())

        assertTrue result!=null
        assertTrue result.size() == 1L

    }




}
