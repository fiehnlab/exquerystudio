package exquery.services

import exquery.ExperimentInfoService
import org.apache.log4j.Logger

/**
 * User: pradeep
 * Date: Dec 20, 2010
 * Time: 12:10:13 PM
 * Test cases for ExperimentInfoService class
 */
class ExperimentInfoServiceTests extends GroovyTestCase{

  Logger gLogger = Logger.getLogger(SearchServiceTests.class)

  ExperimentInfoService experimentInfoService

  void testGetExperimentInfoByExperimentId() {
      def infoBy = "ExperimentId"
      def id = "29518"
      gLogger.info "experimentInfoService :: ${experimentInfoService}"

      def e = experimentInfoService.getExperimentInfo(infoBy,id)

      gLogger.info e
      gLogger.info e?.Experiment

      assertTrue e!=null
      assertTrue e?.Experiment != null
      assertTrue e?.AppsHeader == "Experiment Info by Experiment Id 29518"
  }

  void testGetExperimentInfoByExperimentClassId() {
      def infoBy = "ExperimentClassId"
      def id = "28033"
      gLogger.info "experimentInfoService :: ${experimentInfoService}"

      def e = experimentInfoService.getExperimentInfo(infoBy,id)

      gLogger.info e
      gLogger.info e?.Experiment
      gLogger.info e?.ExperimentClass

      assertTrue e!=null
      assertTrue e?.Experiment != null
      assertTrue e?.ExperimentClass != null
      assertTrue e?.AppsHeader == "Experiment Info by Class Id 28033"
  }

  void testGetExperimentInfoByClassSampleId() {
      def infoBy = "ClassSampleId"
      def id = "20153"
      gLogger.info "experimentInfoService :: ${experimentInfoService}"

      def e = experimentInfoService.getExperimentInfo(infoBy,id)

      gLogger.info e
      gLogger.info e?.Experiment
      gLogger.info e?.ExperimentClass
      gLogger.info e?.ClassSample
      
      assertTrue e!=null
      assertTrue e?.Experiment != null
      assertTrue e?.ExperimentClass != null
      assertTrue e?.ClassSample !=null
      assertTrue e?.AppsHeader == "Experiment Info by Sample Id 20153"
  }

}
