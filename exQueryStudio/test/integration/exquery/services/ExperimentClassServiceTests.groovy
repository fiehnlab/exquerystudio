package exquery.services

import exquery.ExperimentClassService
import org.apache.log4j.Logger

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: 4/26/11
 * Time: 4:04 PM
 * To change this template use File | Settings | File Templates.
 */
class ExperimentClassServiceTests extends GroovyTestCase {
    Logger gLogger = Logger.getLogger(ExperimentServiceTests.class)
    def experimentClassService

    protected void setUp() {
      super.setUp()
      experimentClassService = new ExperimentClassService()
    }

    protected void tearDown(){
        super.tearDown()
    }

    void testGetClassesByClassIds(){
        def ids = [28343L,103905L,28467L]
        def results = experimentClassService.getClassesByClassIds(ids)

//        println results
//        println results?.size()

        assertTrue results!=null
        assertTrue results.size() == 3L
    }

    void testGetClassIdsByExperimentIds(){
        def ids = [19778L,115958L,343581L,101L]
        def results = experimentClassService.getClassIdsByExperimentIds(ids)

        println results
        println results?.size()

        assertTrue results!=null
        assertTrue results.size() == 26L
    }

    void testGetClassesCountByExperimentIds(){
        def ids = [19778L,115958L,343581L,101L]
        def results = experimentClassService.getClassesCountByExperimentIds(ids)

//        println results

        assertTrue results!=null
        assertTrue results == 26L
    }

    void testGetClassesByClassId(){
        def id = 28343L
        def results = experimentClassService.getClassesByClassIds(id)

//        println results
//        println results?.size()

        assertTrue results!=null
        assertTrue results.size() == 1L
    }

    void testGetClassIdsByExperimentId(){
        def id = 19778L
        def results = experimentClassService.getClassIdsByExperimentIds(id)

        println results
        println results?.size()

        assertTrue results!=null
        assertTrue results.size() == 7L
    }

    void testGetClassesCountByExperimentId(){
        def id = 19778L
        def results = experimentClassService.getClassesCountByExperimentIds(id)

        //println results

        assertTrue results!=null
        assertTrue results == 7L
    }
}
