package exquery.services

import grails.test.*
import exquery.OrgansService

class OrgansServiceTests extends GrailsUnitTestCase {

    def organsService
    protected void setUp() {
        super.setUp()
        organsService = new OrgansService()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testGetOrgansByClassId() {
       def ClassId = 115837L
       def organs = organsService.getOrgansByClassIds(ClassId)
       println organs

       assertTrue organs!=null
       assertTrue organs.size() == 1L
    }

    void testGetOrgansByClassIds() {
       def ClassIds = [115837L,115868L,115899L,331722L,331747L,331772L,331797L]
       def organs = organsService.getOrgansByClassIds(ClassIds)
       println organs

       assertTrue organs!=null
       assertTrue organs.size() == 2L
    }

    void testGetUniqueSxOrgans(){
        def uniqueList = organsService.getUniqueSxOrgans()
        println uniqueList

        assertTrue uniqueList!=null
        assertTrue uniqueList.size() == 7L
    }

    void testGetOrganDataByOrganName(){
        String organName = "rosette leaf"
        def organ = organsService.getOrganDataByOrganName(organName)

        assertTrue organ!=null
        assertTrue organ.getExperimentCount() == 2L
    }

    void testGetOrganDataByOrganNameWithWildCards(){
        String organName = "*leaf*"
        def result = organsService.getOrganDataByOrganNameWithWildCards(organName)
        println result

        assertTrue result!=null
        assertTrue result.size() == 2L
    }

    void testGetClassIdsByOrganName(){
        String organName = "rosette leaf"
        def classIds = organsService.getClassIdsByOrganName(organName)

        assertTrue classIds!=null
        assertTrue classIds.size() == 21L
    }

    void testGetSampleIdsByOrganName(){
        String organName="rosette leaf"
        def result = organsService.getSampleIdsByOrganName(organName)
        println result

        assertTrue result!=null
        assertTrue result?.size() == 126L

    }
}
