package exquery



class SearchWebTests extends grails.util.WebTest {

    // Unlike unit tests, functional tests are sometimes sequence dependent.
    // Methods starting with 'test' will be run automatically in alphabetical order.
    // If you require a specific sequence, prefix the method name (following 'test') with a sequence
    // e.g. test001XclassNameXListNewDelete

    void testSearchAtHomePage() {
        invoke "http://localhost:8080/exQueryStudio/homePage"

        setSelectField(name: "cmbSearchBy", text: "Experiments")
        setSelectField(htmlId: "cmbSearchBy", text: "Experiments")
        setInputField(name: "searchText", value: "29518")
        setInputField(htmlId: "searchText", value: "29518")
        clickButton "Submit"

    }

    void testSearch() {
        invoke "http://localhost:8080/exQueryStudio/search/index"

        setSelectField(name: "cmbSearchBy", text: "Experiments")
        setSelectField(htmlId: "cmbSearchBy", text: "Experiments")
        setInputField(name: "searchText", value: "29518")
        setInputField(htmlId: "searchText", value: "29518")
        clickButton "Submit"
    }

    void testSearchNORecordFound() {
        invoke "http://localhost:8080/exQueryStudio/search/index"

        setSelectField(name: "cmbSearchBy", text: "Experiments")
        setSelectField(htmlId: "cmbSearchBy", text: "Experiments")
        setInputField(name: "searchText", value: "007")
        setInputField(htmlId: "searchText", value: "007")
        clickButton "Submit"

        verifyText(text: 'No Data Found...')
    }
}